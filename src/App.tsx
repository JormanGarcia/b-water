import { IonApp, IonRouterOutlet, IonSplitPane } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import { Redirect, Route } from "react-router-dom";

import PopupNotification from "./components/PopupNotification";

// Importando páginas
import Menu from "./components/Menu";
import Dashboard from "./pages/Dashboard";
import Login from "./pages/Login";
import Billing from "./pages/Billing";
import OrdersPage from "./pages/OrdersPage";
import ProductsPage from "./pages/ProductsPage";
import MapView from "./pages/MapView";
import InventoryPage from "./pages/InventoryPage";
import MatricesPage from "./pages/MatricesPage";
import CustomersPage from "./pages/CustomersPage";
import HistoryInventory from "./pages/HistoryInventory";
import VendorsPage from "./pages/VendorsPage";
import UsersPage from "./pages/UsersPage";
import ProductsRegistrationPage from "./pages/ProductsRegistrationPage";
import Personal from "./pages/Personal";
import Comisiones from "./pages/Comisiones";
import Notifications from "./pages/Notifications";
import Egresos from "./pages/Egresos";
import VehiclePages from "./pages/VehiclePage";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import ExtraHours from "./components/ExtraHours";
import AssignmentHistory from "./pages/AssignmentHistory";
import Garrafones from "./pages/Garrafones";
import UseAuth from "./hooks/useAuth";

import AuthRedirection from "./components/AuthRedirection";
import LoadingSpinner from "./components/LoadingSpinner";

const App = () => {
  const { isAuthenticated } = UseAuth();

  // return (
  //   <IonApp>
  //     <IonReactRouter>
  //       <IonSplitPane contentId="main" class="split-plane">
  //         {authenticated ? <Menu /> : null}
  //         <IonRouterOutlet id="main">
  //           <Route path="/" exact={true}>
  //             {authenticated ? (
  //               <Redirect to="/page/dashboard" />
  //             ) : (
  //               <Redirect to="/login" />
  //             )}
  //           </Route>

  //           <Route path="/login" exact={true}>
  //             <Login />
  //           </Route>

  //           <Route path="/page/dashboard" exact={true}>
  //             <Dashboard />
  //           </Route>

  //           <Route path="/page/notifications" exact={true}>
  //             <Notifications />
  //           </Route>

  //           <Route path="/page/billing" exact={true}>
  //             <Billing />
  //           </Route>

  //           <Route path="/page/orders" exact={true}>
  //             <OrdersPage />
  //           </Route>

  //           <Route path="/page/products" exact={true}>
  //             <ProductsPage />
  //           </Route>

  //           <Route path="/page/map" exact={true}>
  //             <MapView />
  //           </Route>

  //           <Route path="/page/inventory" exact={true}>
  //             <InventoryPage />
  //           </Route>

  //           <Route path="/page/matrices" exact={true}>
  //             <MatricesPage />
  //           </Route>

  //           <Route path="/page/egresos" exact={true}>
  //             <Egresos />
  //           </Route>

  //           <Route path="/page/customers" exact={true}>
  //             <CustomersPage />
  //           </Route>

  //           <Route path="/page/history-inventory" exact={true}>
  //             <HistoryInventory />
  //           </Route>

  //           <Route path="/page/vendors" exact={true}>
  //             <VendorsPage />
  //           </Route>

  //           <Route path="/page/users" exact={true}>
  //             <UsersPage />
  //           </Route>

  //           <Route path="/page/personal" exact={true}>
  //             <Personal />
  //           </Route>

  //           <Route path="/page/extra-hours" exact={true}>
  //             <ExtraHours />
  //           </Route>

  //           <Route path="/page/comisiones" exact={true}>
  //             <Comisiones />
  //           </Route>

  //           <Route path="/page/vehicle-page" exact={true}>
  //             <VehiclePages />
  //           </Route>

  //           <Route path="/page/assignment-history" exact={true}>
  //             <AssignmentHistory />
  //           </Route>

  //           <Route path="/page/garrafones" exact={true}>
  //             <Garrafones />
  //           </Route>

  //           <Route path="/page/product-registration" exact={true}>
  //             <ProductsRegistrationPage />
  //           </Route>
  //           {/* PARA MOSTRAR TEMPORALMENTE LA VISTA DEL LOGIN */}
  //           <Route path="/page/login" exact={true}>
  //             <Login />
  //           </Route>
  //           {/*PARA MOSTRAR TEMPORALMENTE LA VISTA DEL LOGIN  */}
  //         </IonRouterOutlet>
  //       </IonSplitPane>
  //     </IonReactRouter>
  //   </IonApp>
  // );

  return (
    <IonApp>
      <IonReactRouter>
        <LoadingSpinner />
        <PopupNotification />
        <IonSplitPane contentId="main" class="split-plane">
          {isAuthenticated && <Menu />}

          <IonRouterOutlet id="main">
            <Route path="/login" exact={true}>
              <Login />
            </Route>

            <AuthRedirection>
              <Route path="/page/dashboard" exact={true}>
                <Dashboard />
              </Route>

              <Route path="/page/notifications" exact={true}>
                <Notifications />
              </Route>

              <Route path="/page/billing" exact={true}>
                <Billing />
              </Route>

              <Route path="/page/orders" exact={true}>
                <OrdersPage />
              </Route>

              <Route path="/page/products" exact={true}>
                <ProductsPage />
              </Route>

              <Route path="/page/map" exact={true}>
                <MapView />
              </Route>

              <Route path="/page/inventory" exact={true}>
                <InventoryPage />
              </Route>

              <Route path="/page/matrices" exact={true}>
                <MatricesPage />
              </Route>

              <Route path="/page/egresos" exact={true}>
                <Egresos />
              </Route>

              <Route path="/page/customers" exact={true}>
                <CustomersPage />
              </Route>

              <Route path="/page/history-inventory" exact={true}>
                <HistoryInventory />
              </Route>

              <Route path="/page/vendors" exact={true}>
                <VendorsPage />
              </Route>

              <Route path="/page/users" exact={true}>
                <UsersPage />
              </Route>

              <Route path="/page/personal" exact={true}>
                <Personal />
              </Route>

              <Route path="/page/extra-hours" exact={true}>
                <ExtraHours />
              </Route>

              <Route path="/page/comisiones" exact={true}>
                <Comisiones />
              </Route>

              <Route path="/page/vehicle-page" exact={true}>
                <VehiclePages />
              </Route>

              <Route path="/page/assignment-history" exact={true}>
                <AssignmentHistory />
              </Route>

              <Route path="/page/garrafones" exact={true}>
                <Garrafones />
              </Route>

              <Route path="/page/product-registration" exact={true}>
                <ProductsRegistrationPage />
              </Route>
            </AuthRedirection>
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
