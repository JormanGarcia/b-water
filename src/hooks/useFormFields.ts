import { ChangeEvent, useState } from "react";

const useFormFields = <t>(initialValue: t) => {
  const [formField, setFormFields] = useState(initialValue);

  const onChangeField =
    (key: keyof t) =>
    (e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLSelectElement>) => {
      const value = e.target.value;
      setFormFields((prev: t) => ({ ...prev, [key]: value }));
    };

  const areAllFieldsFill = () => {
    const areFilled = Object.values(formField).filter(
      (field) => field === "" || field === 0
    );

    return areFilled.length === 0;
  };

  const getLabelProps = (key: keyof t) => {
    return {
      value: formField[key],
      onChange: onChangeField(key),
    };
  };

  return {
    formField,
    onChangeField,
    getLabelProps,
    setFormFields,
    areAllFieldsFill,
  };
};

export default useFormFields;
