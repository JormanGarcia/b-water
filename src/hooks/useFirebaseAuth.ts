import { useFirebaseApp, useUser } from "reactfire";
import { useUserApi } from "../api/useUserApi";
import { UseTokenStorage } from "../utils/token.storage";
import UseAuth from "./useAuth";
import { useNavContext } from "./useNavContext";
import UseSystemState from "./useSystemState";

interface IniciarSesionProps {
  password: string;
  email: string;
}
const useFirebaseAuth = () => {
  const { loginUser, logoutUser } = UseAuth();
  const { navigate } = useNavContext();
  const firebase = useFirebaseApp();
  const TokenStorage = UseTokenStorage();
  const { pushNotification, setLoading } = UseSystemState();

  const usersApi = useUserApi();

  const firebaseUser = useUser();

  const IniciarSesion = async ({ password, email }: IniciarSesionProps) => {
    if (email === "" || password === "") {
      pushNotification("Todos los campos deben de estar llenos");
      return;
    }
    setLoading(true);
    const firebaseResponse = await firebase
      .auth()
      .signInWithEmailAndPassword(email.toLowerCase(), password.toLowerCase());

    const currentUserToken = await firebase.auth().currentUser?.getIdToken();

    TokenStorage.saveToken(currentUserToken!);

    const userToken = await firebaseResponse.user?.getIdToken();

    setLoading(false);
  };

  const logout = () => {
    firebase
      .auth()
      .signOut()
      .then((res) => {
        logoutUser();
      });
  };

  const createUser = async (email: string, password: string) => {
    const firebaseResponse = await firebase
      .auth()
      .createUserWithEmailAndPassword(email, password);
    return firebaseResponse;
  };

  const onTokenChange = () => {
    // ALGORITMO PARA COMPROBAR QUE EL TOKEN DE LA SESION EXISTE AUN O NO

    firebase.auth().onIdTokenChanged(async (user) => {
      if (!user) return;
      const idToken = await firebase.auth().currentUser?.getIdToken(true);

      const saveTokenResponse = await TokenStorage.saveToken(idToken!);
      if (idToken === null) {
        await firebase.auth().signOut();
        console.log(saveTokenResponse, "Cerrando Sesion");
        return;
      }

      console.log(firebaseUser.data, "Conectado...");
    });
  };

  return {
    IniciarSesion,
    logout,
    createUser,
    onTokenChange,
  };
};

export default useFirebaseAuth;
