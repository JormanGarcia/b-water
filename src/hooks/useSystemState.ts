import { useSelector } from "react-redux";

import { StateType, useAppDispatch } from "../store";
import {
  setLoading as SetLoading,
  pushNotification as PushNotification,
  unsetNotification,
} from "../store/systemStateSlice";

const UseSystemState = () => {
  const systemState = useSelector((state: StateType) => state.systemState);
  const dispatch = useAppDispatch();

  const setLoading = (loadingIs: boolean) => dispatch(SetLoading(loadingIs));

  const pushNotification = (payload: string) => {
    const newNotification = {
      key: Math.random(),
      msg: payload,
    };
    dispatch(PushNotification(newNotification));

    setTimeout(() => dispatch(unsetNotification(newNotification.key)), 4000);
  };

  return {
    loading: systemState.loading,
    notifications: systemState.notifications,
    setLoading,
    pushNotification,
  };
};

export default UseSystemState;
