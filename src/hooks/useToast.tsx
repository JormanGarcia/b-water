import { useIonToast } from "@ionic/react";

export const useToast = () => {
  const [presenter] = useIonToast();

  return {
    presenter: (msg: string, duration: number = 2000) =>
      presenter({
        message: msg,
        duration,
        position: "top",
        translucent: true,
      }),
  };
};
