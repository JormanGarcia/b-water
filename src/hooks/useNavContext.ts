import { NavContext } from "@ionic/react";
import { useContext } from "react";

export const useNavContext = () => {
  const navContext = useContext(NavContext);
  return navContext;
};
