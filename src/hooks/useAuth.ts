import { useSelector } from "react-redux";
import authApi from "../api/auth";
import { StateType, useAppDispatch } from "../store";
import {
  logout,
  login as loginAction,
  setToken,
} from "../store/authenticationSlice";
import { useIonToast } from "@ionic/react";
import { useToast } from "./useToast";

const UseAuth = () => {
  const authState = useSelector((state: StateType) => state.auth);

  const { presenter } = useToast();
  const dispatch = useAppDispatch();

  const logoutUser = async () => {
    // const response = await authApi.logoutUser(authState.authToken!);
    dispatch(logout());
  };

  const loginUser = async (user: { email: string; password: string }) => {
    function isNumeric(str: string) {
      if (typeof str != "string") return false; // we only process strings!

      return (
        !isNaN(+str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
        !isNaN(parseFloat(str))
      ); // ...and ensure strings of whitespace fail
    }

    let response;

    if (isNumeric(user.email)) {
      response = await authApi.loginUserByPhone(user);
    } else {
      response = await authApi.loginUser(user);
    }

    const { data, status } = response!;

    console.log(status);

    if (status !== 200) {
      presenter("Intente de nuevo");
    }

    dispatch(
      loginAction({
        id: data.id,
        nombre: data.nombre,
        apellido: data.apellido,
        rol: data.rol,
        sucursal_id: data.sucursal_id,
      })
    );
    dispatch(setToken(`${data.token_type} ${data.access_token}`));
    return response;
  };

  const SetAuthToken = (Token: string) => dispatch(setToken(Token));

  return {
    isAuthenticated: authState.user !== null,
    user: authState.user,
    token: authState.authToken,
    logoutUser,
    loginUser,
    SetAuthToken,
  };
};

export default UseAuth;
