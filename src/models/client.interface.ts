export interface IClient {
  apellido_cliente: string;
  avenida: string;
  calle: string;
  casa: string;
  ciudad: string;
  codigo_postal: string;
  coto: string;
  created_at?: string;
  estado: string;
  estatus_cliente: string;
  fraccionamiento: string;
  id?: number;
  nombre_cliente: string;
  rfc: string;
  telefono_cliente: string;
  tipo_cliente: string;
  updated_at?: string;
  tel_fam_1: string;
  tel_fam_2: string;
  nom_fam_1: string;
  nom_fam_2: string;
  correo_cliente: string;
  cliente_nuevo?: any;
}
