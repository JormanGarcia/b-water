export interface sucursalesType {
  id?: number;
  nombre_sucursal: string;
  direccion: string;
  latitud: string;
  longitud: string;
  telefono_sucursal: string;
  nombre_gerente: string;
  telefono_gerente: string;
}
