export interface userByIdType {
  id?: number;
  nombre: string;
  apellido: string;
  sucursal_id: number;
  rol: number;
  status: number;
  email: string;
  uid?: string;
  telefono: string;
  password?: string;
}
