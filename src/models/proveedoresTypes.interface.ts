export interface proveedoresType {
  razon_social: string;
  representante: string;
  direccion_proveedor: string;
  telefono_proveedor: string;
  rfc: number;
  id?: number;
}
