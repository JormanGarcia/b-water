export interface userType {
  id: number;
  nombre: string;
  apellido: string;
  nombre_sucursal: string;
  rol: number;
  rolConverted?: string;
  status: number;
  statusConverted?: string;
  email: string;
  telefono: string;
}
