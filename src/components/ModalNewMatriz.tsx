import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonModal,
  IonHeader,
  IonFooter,
} from "@ionic/react";

import { ChangeEvent, useEffect, useState } from "react";
import { useSucursalesApi } from "../api/useSucursalesApi";
import { useUserApi } from "../api/useUserApi";
import { userType } from "../models/userType.interface";
import UseSystemState from "../hooks/useSystemState";
import useFormFields from "../hooks/useFormFields";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => any;
  fetchSucursales: () => any;
}

const ModalNewMatriz = ({
  showModal,
  setShowModal,
  fetchSucursales,
}: WithModalProps) => {
  const [gerentes, setGerentes] = useState<userType[]>([]);

  const { formField, onChangeField, setFormFields, areAllFieldsFill } =
    useFormFields({
      gerente: {
        id: "",
        telefono: "",
        nombre: "",
      },
      direccion: "",
      telefonoSucursal: "",
      nombre: "",
      latitud: "",
      longitud: "",
    });

  const { setLoading, pushNotification } = UseSystemState();

  const usersApi = useUserApi();
  const sucursalesApi = useSucursalesApi();

  const onChangeGerenteSelect = (e: ChangeEvent<HTMLSelectElement>) => {
    const gerenteSelected = gerentes.filter((x) => x.id === +e.target.value)[0];

    setFormFields({
      ...formField,
      gerente: {
        nombre: `${gerenteSelected.nombre} ${gerenteSelected.apellido}`,
        telefono: gerenteSelected.telefono,
        id: e.target.value,
      },
    });
  };

  const fetchData = async () => {
    setLoading(true);
    const usersResponse = await usersApi.getUsersByRole(2);
    setGerentes(usersResponse);
    setLoading(false);
  };

  const addSucursales = async (newSucursal: typeof formField) => {
    const { direccion, gerente, latitud, longitud, nombre, telefonoSucursal } =
      newSucursal;
    if (!areAllFieldsFill) {
      pushNotification("Todos los campos deben de estar llenos");
      return;
    }

    setLoading(true);
    await sucursalesApi.addSucursales({
      nombre_sucursal: nombre,
      direccion: direccion,
      latitud,
      longitud,
      telefono_sucursal: telefonoSucursal,
      nombre_gerente: gerente.nombre,
      telefono_gerente: gerente.telefono,
    });
    setShowModal(false);
    await fetchSucursales();
    setLoading(false);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Nueva matriz</IonHeader>
      <IonContent className="ion-padding-horizontal ion-padding-vertical">
        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Nombre</label>
              <input
                type="text"
                className="form-control"
                value={formField.nombre}
                onChange={onChangeField("nombre")}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Dirección</label>
              <input
                type="text"
                className="form-control"
                value={formField.direccion}
                onChange={onChangeField("direccion")}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Teléfono de sucursal</label>
              <input
                type="text"
                className="form-control"
                value={formField.telefonoSucursal}
                onChange={onChangeField("telefonoSucursal")}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Latitud</label>
              <input
                type="text"
                className="form-control"
                value={formField.latitud}
                onChange={onChangeField("latitud")}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Longitud</label>
              <input
                type="text"
                className="form-control"
                value={formField.longitud}
                onChange={onChangeField("longitud")}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Gerente</label>
              <select
                value={formField.gerente.id}
                className="form-control"
                onChange={onChangeGerenteSelect}
              >
                <option value="" disabled>
                  Seleccione gerente
                </option>
                {gerentes.map(({ id, nombre, apellido }) => (
                  <option key={id} value={id}>
                    {nombre} {apellido}
                  </option>
                ))}
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="">Tlf Gerente</label>
              <input
                type="number"
                className="form-control"
                value={formField.gerente.telefono}
                disabled
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton color="bwater" onClick={() => addSucursales(formField)}>
          Guardar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default ModalNewMatriz;
