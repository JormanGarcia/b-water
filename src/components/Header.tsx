import {
  IonHeader,
  IonToolbar,
  IonTitle,
  IonButtons,
  IonMenuButton,
} from "@ionic/react";
import { useLocation } from "react-router";
import getHeaderTitle from "../utils/headerTitles";

import UserProfile from "./UserProfile";

const Header = ({ title }: { title: string }) => {
  const location = useLocation();

  return (
    <IonHeader>
      <IonToolbar className="bg-light-bwater">
        <IonButtons slot="start">
          <IonMenuButton />
        </IonButtons>
        <IonTitle className="page-title">{title}</IonTitle>
        <div slot="end" className="end-header">
          <UserProfile />
        </div>
      </IonToolbar>
    </IonHeader>
  );
};

export default Header;
