import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonModal,
  IonHeader,
  IonFooter,
} from "@ionic/react";
import { GetTodayDate } from "../utils";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

const VehicleAssignment = ({ showModal, setShowModal }: WithModalProps) => {
  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Asignación de Vehiculo</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Sucursal</label>
              <input
                type="text"
                className="form-control"
                placeholder="Sucursal"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de Asignacion</label>
              <input
                type="date"
                className="form-control"
                placeholder="Generado automaticamente"
                value={GetTodayDate()}
                disabled
              />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol size="6">
              <label htmlFor="">Asignado Por</label>
              <input
                type="text"
                className="form-control"
                placeholder="Usuario Logeado"
              />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol size="4">
              <label htmlFor="">Vehiculos de sucursal</label>
            </IonCol>
            <IonCol size="8">
              <label htmlFor="">Chofer</label>
            </IonCol>
          </IonRow>
          {[1, 2, 3, 4].map((x) => (
            <>
              <IonRow>
                <IonCol size="4">
                  <input
                    className="form-control"
                    placeholder={`Vehiculo ${x}`}
                  />
                </IonCol>
                <IonCol size="8">
                  <select className="form-control" defaultValue="def">
                    <option value="def">Seleccione Chofer</option>
                  </select>
                </IonCol>
              </IonRow>
            </>
          ))}
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton color="bwater">Guardar</IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default VehicleAssignment;
