import { IonCard, IonCardContent, IonCardHeader, IonIcon } from "@ionic/react";
import { searchOutline } from "ionicons/icons";
import { useState } from "react";
import { useTable, useGlobalFilter, useAsyncDebounce } from "react-table";

interface TableProps {
  data: any[];
  columns: any;
  onRowClick: (a: any) => void;
  title: string;
}

function GlobalFilter({ globalFilter, setGlobalFilter }) {
  const [value, setValue] = useState(globalFilter);
  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined);
  }, 200);

  return (
    <input
      value={value || ""}
      onChange={(e) => {
        setValue(e.target.value);
        onChange(e.target.value);
      }}
      className="form-control"
    />
  );
}

const DataTable = ({ columns, data, onRowClick, title }: TableProps) => {
  const instance = useTable({ columns, data }, useGlobalFilter);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    setGlobalFilter,
  } = instance;

  const Header = () =>
    headerGroups.map((headerGroup) => (
      <tr {...headerGroup.getHeaderGroupProps()}>
        {headerGroup.headers.map((column) => (
          <th {...column.getHeaderProps}>{column.render("Header")}</th>
        ))}
      </tr>
    ));

  const Body = () =>
    rows.map((row) => {
      prepareRow(row);
      return (
        <tr {...row.getRowProps()} onClick={() => onRowClick(row.original)}>
          {row.cells.map((cell) => (
            <td {...cell.getCellProps}>{cell.render("Cell")}</td>
          ))}
        </tr>
      );
    });

  return (
    <IonCard className="m-0">
      <IonCardHeader className="table-header">
        <h2 className="table-title">{title}</h2>
        <div style={{ display: "flex" }}>
          <div className="search" style={{ marginRight: ".5rem" }}>
            <button>
              <IonIcon md={searchOutline} />
            </button>
            <GlobalFilter
              globalFilter={state.globalFilter}
              setGlobalFilter={setGlobalFilter}
            />
          </div>
        </div>
      </IonCardHeader>
      <IonCardContent>
        <table className="table" {...getTableProps}>
          <thead>{Header()}</thead>
          <tbody {...getTableBodyProps()}>{Body()}</tbody>
        </table>
      </IonCardContent>
    </IonCard>
  );
};

export default DataTable;
