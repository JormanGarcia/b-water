import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
} from "@ionic/react";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

const ExtraHoursConfig = ({ showModal, setShowModal }: WithModalProps) => {
  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">
        Configuración de Horas Extras
      </IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <label htmlFor="">Valor de Horas Extras</label>
              <input
                placeholder="Valor de Horas Extras"
                className="form-control"
                type="number"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Cantidad Maxima de Horas Extras Semanal</label>
              <input
                type="number"
                className="form-control"
                placeholder="Cantidad Maxima de Horas Extras Semanal"
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton color="bwater" style={{ marginRight: "1rem" }}>
          Guardar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default ExtraHoursConfig;
