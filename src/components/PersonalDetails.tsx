import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
} from "@ionic/react";
import { useState } from "react";

interface Personal {
  ID: number;
  fechaIngreso: string;
  fechaRegistro: string;
  nombre: string;
  apellido: string;
  cargo: string;
  sucursal: string;
  salario: number;
  telefono: number;
  correo: string;
  direccion: string;
}

interface WithModalProps {
  personal: Personal | null;
  setPersonal: (a: Personal | null) => void;
}

const PersonalDetails = ({ personal, setPersonal }: WithModalProps) => {
  const [edit, setEdit] = useState<boolean>(false);
  return (
    <IonModal
      isOpen={personal !== null}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setPersonal(null)}
    >
      <IonHeader className="modal__header">Detalles de personal</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <label htmlFor="">ID</label>
              <input
                type="text"
                className="form-control"
                placeholder="ID"
                value={personal?.ID}
                disabled={!edit}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de ingreso</label>
              <input
                type="date"
                className="form-control"
                placeholder="Fecha de ingreso"
                value={personal?.fechaIngreso}
                disabled={!edit}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de Registro</label>
              <input
                type="date"
                className="form-control"
                placeholder="Fecha de Registro"
                value={personal?.fechaRegistro}
                disabled={!edit}
              />
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label>Nombres</label>
              <input
                type="text"
                className="form-control"
                placeholder="Nombres"
                value={personal?.nombre}
                disabled={!edit}
              />
            </IonCol>
            <IonCol>
              <label>Apellidos</label>
              <input
                type="text"
                className="form-control"
                placeholder="Apellidos"
                value={personal?.apellido}
                disabled={!edit}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label>Cargo</label>
              <select className="form-control" disabled={!edit}>
                <option>Seleccione Cargo</option>
              </select>
            </IonCol>
            <IonCol>
              <label>Sucursal Asignada</label>
              <select className="form-control" disabled={!edit}>
                <option>Sucursal Asignada</option>
              </select>
            </IonCol>
            <IonCol>
              <label>Salario</label>
              <input
                className="form-control"
                placeholder="Salario"
                disabled={!edit}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label>Telefono</label>
              <input
                type="text"
                className="form-control"
                placeholder="Telefono"
                disabled={!edit}
                value={personal?.telefono}
              />
            </IonCol>
            <IonCol>
              <label>Correo Electrónico</label>
              <input
                type="text"
                className="form-control"
                placeholder="Correo Electronico"
                disabled={!edit}
                value={personal?.correo}
              />
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol size="6">
              <label>Direccion</label>
              <input
                className="form-control"
                placeholder="Direccion"
                disabled={!edit}
                value={personal?.direccion}
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setPersonal(null);
            setEdit(false);
          }}
        >
          Cancelar
        </IonButton>
        {!edit ? (
          <IonButton
            color="bwater"
            style={{ marginRight: "1rem" }}
            onClick={() => setEdit(true)}
          >
            Editar
          </IonButton>
        ) : (
          <IonButton color="bwater" style={{ marginRight: "1rem" }}>
            Guardar
          </IonButton>
        )}
      </IonFooter>
    </IonModal>
  );
};

export default PersonalDetails;
