import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonBadge,
  IonIcon,
} from "@ionic/react";

import { getBadgeColor } from "../helpers/index";

import { searchOutline } from "ionicons/icons";

import ModalOrder from "../components/ModalOrder";

import { useState, useEffect } from "react";

const OrdersTable = () => {
  // Obtenemos el estado de los pedidos
  const [pedidos, setPedidos] = useState([]);
  const [showModal, setShowModal] = useState(false);

  // Esto se ejecuta cuando el componente se renderiza (cambio de estado, props, o primera vez que se carga)
  useEffect(() => {
    fetch("http://localhost:8100/data-pedidos.json")
      .then((res) => res.json())
      .then((res) => {
        setPedidos(res[0].pedidos);
      });
  }, []); // El ultimo argumento es un array de propiedades que activan el useEffect

  return (
    <IonCard className="m-0">
      <ModalOrder showModal={showModal} setShowModal={setShowModal} />
      <IonCardHeader className="table-header">
        <h2 className="table-title">Listado de pedidos activos</h2>
        <div className="search">
          <button>
            <IonIcon md={searchOutline} />
          </button>
          <input type="text" className="search__input" placeholder="Buscar" />
          <select className="search__input">
            <option>Estatus Envio</option>
          </select>
          <select className="search__input">
            <option>Estatus Pago</option>
          </select>
          <select className="search__input">
            <option>Conductor</option>
          </select>
        </div>
      </IonCardHeader>
      <IonCardContent>
        <table className="table">
          <thead>
            <tr>
              <th>ID Pedido</th>
              <th>Fecha</th>
              <th>ID Cliente</th>
              <th>Tipo Cliente</th>
              <th>Tipo Venta</th>
              <th>Sucursal</th>
              <th>Conductor</th>
              <th>Monto</th>
              <th>Estatus de envío</th>
              <th>Estatus de pago</th>
            </tr>
          </thead>
          <tbody>
            {pedidos.map((pedido: any, index: number) => (
              <tr onClick={() => setShowModal(true)} key={index}>
                <td>{pedido.idPedido}</td>
                <td>{pedido.fecha}</td>
                <td>{pedido.idCliente}</td>
                <td>{pedido.tipoCliente || ""}</td>
                <td>{pedido.tipoVenta || ""}</td>
                <td>{pedido.sucursal || ""}</td>
                <td>{pedido.conductor}</td>
                <td>{pedido.monto || ""}</td>
                <td>
                  <IonBadge color={getBadgeColor(pedido.estatusEnvio)}>
                    {pedido.estatusEnvio}
                  </IonBadge>
                </td>
                <td>
                  <IonBadge color={getBadgeColor(pedido.estatusFacturacion)}>
                    {pedido.estatusFacturacion}
                  </IonBadge>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </IonCardContent>
    </IonCard>
  );
};

export default OrdersTable;
