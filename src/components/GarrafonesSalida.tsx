import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
} from "@ionic/react";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

const GarrafonesSalida = ({ showModal, setShowModal }: WithModalProps) => {
  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Garrafones de Salida</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol size="6">
              <label htmlFor="">Numero de Garrafones</label>
              <input
                placeholder="Numero de Garrafones"
                className="form-control"
                type="number"
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton color="bwater" style={{ marginRight: "1rem" }}>
          Guardar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default GarrafonesSalida;
