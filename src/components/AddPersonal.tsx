import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
} from "@ionic/react";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

const AddPersonal = ({ showModal, setShowModal }: WithModalProps) => {
  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Agregar Personal</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <label htmlFor="">ID</label>
              <input type="text" className="form-control" placeholder="ID" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de ingreso</label>
              <input
                type="date"
                className="form-control"
                placeholder="Fecha de ingreso"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de Registro</label>
              <input
                type="text"
                className="form-control"
                placeholder="Fecha de Registro"
              />
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label>Nombres</label>
              <input
                type="text"
                className="form-control"
                placeholder="Nombres"
              />
            </IonCol>
            <IonCol>
              <label>Apellidos</label>
              <input
                type="text"
                className="form-control"
                placeholder="Apellidos"
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label>Cargo</label>
              <select className="form-control">
                <option>Seleccione Cargo</option>
              </select>
            </IonCol>
            <IonCol>
              <label>Sucursal Asignada</label>
              <select className="form-control">
                <option>Sucursal Asignada</option>
              </select>
            </IonCol>
            <IonCol>
              <label>Salario</label>
              <input className="form-control" placeholder="Salario" />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label>Telefono</label>
              <input
                type="text"
                className="form-control"
                placeholder="Telefono"
              />
            </IonCol>
            <IonCol>
              <label>Correo Electronico</label>
              <input
                type="text"
                className="form-control"
                placeholder="Correo Electronico"
              />
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol size="6">
              <label>Direccion</label>
              <input className="form-control" placeholder="Direccion" />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton color="bwater" style={{ marginRight: "1rem" }}>
          Agregar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default AddPersonal;
