import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
} from "@ionic/react";
import { useState } from "react";

interface WithModalProps {
  item: object | null;
  setItem: (a: object | null) => void;
}

const ExtraHoursRecord = ({ item, setItem }: WithModalProps) => {
  const [edit, setEdit] = useState<boolean>(false);

  const CloseModal = () => {
    setItem(null);
    setEdit(false);
  };

  return (
    <IonModal
      isOpen={item !== null}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => CloseModal()}
    >
      <IonHeader className="modal__header">Detalle de Horas Extras</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <label htmlFor="">Trabajador</label>
              <select disabled={!edit} className="form-control">
                <option>Seleccione Trabajador</option>
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha Laborada</label>
              <input
                disabled={!edit}
                type="date"
                className="form-control"
                placeholder="Fecha de ingreso"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de Registro</label>
              <input
                disabled={!edit}
                type="date"
                className="form-control"
                placeholder="Fecha de Registro"
              />
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label>Nombres</label>
              <input
                disabled={!edit}
                type="text"
                className="form-control"
                placeholder="Nombres"
              />
            </IonCol>
            <IonCol>
              <label>Apellidos</label>
              <input
                disabled={!edit}
                type="text"
                className="form-control"
                placeholder="Apellidos"
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label>Cargo</label>
              <input
                disabled={!edit}
                type="text"
                className="form-control"
                placeholder="Cargo"
              />
            </IonCol>
            <IonCol>
              <label>Sucursal</label>
              <select disabled={!edit} className="form-control">
                <option>Sucursal</option>
              </select>
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label>Numero de Horas Extras</label>
              <input
                disabled={!edit}
                type="number"
                className="form-control"
                placeholder="Horas Extras"
              />
            </IonCol>
            <IonCol>
              <label>Valor de Hora Extra</label>
              <input
                disabled={!edit}
                type="number"
                className="form-control"
                placeholder="Pago Extra"
              />
            </IonCol>
            <IonCol>
              <label>Total de Pago Extra</label>
              <input disabled={!edit} type="text" className="form-control" />
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol size="6">
              <label>Observacion</label>
              <textarea
                disabled={!edit}
                className="form-control"
                placeholder="Observacion"
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => CloseModal()}
        >
          Cancelar
        </IonButton>
        {!edit ? (
          <IonButton
            color="bwater"
            style={{ marginRight: "1rem" }}
            onClick={() => setEdit(true)}
          >
            Editar
          </IonButton>
        ) : (
          <IonButton color="bwater" style={{ marginRight: "1rem" }}>
            Guardar
          </IonButton>
        )}
      </IonFooter>
    </IonModal>
  );
};

export default ExtraHoursRecord;
