import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonModal,
  IonHeader,
  IonFooter,
} from "@ionic/react";

import { useEffect, useState } from "react";

import "firebase/auth";

import { useUserApi } from "../api/useUserApi";
import { useSucursalesApi } from "../api/useSucursalesApi";

import { sucursalesType } from "../models/sucuarsalesType.interface";
import useFirebaseAuth from "../hooks/useFirebaseAuth";
import UseSystemState from "../hooks/useSystemState";
import useFormFields from "../hooks/useFormFields";
import { useToast } from "../hooks/useToast";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
  fetchData: () => void;
}

const ModalAddUser = ({
  showModal,
  setShowModal,
  fetchData,
}: WithModalProps) => {
  const { formField, onChangeField, areAllFieldsFill, getLabelProps } =
    useFormFields({
      nombre: "",
      apellido: "",
      rol: 0,
      sucursal: 0,
      password1: "",
      password2: "",
      email: "",
      telefono: "",
    });

  const sucursalesApi = useSucursalesApi();
  const usersApi = useUserApi();
  const { presenter } = useToast();

  const [sucur, setSucur] = useState<sucursalesType[]>([]);

  const { pushNotification, setLoading } = UseSystemState();

  const fetchSucursales = async () => {
    setLoading(true);

    const sucursalesRes = await sucursalesApi.getSucursales();

    setSucur(sucursalesRes);
    console.log(sucursalesRes, "sucursales registradas");

    setLoading(false);
  };

  useEffect(() => {
    fetchSucursales();
  }, []);

  const submit = async () => {
    const {
      apellido,
      email,
      nombre,
      password1,
      password2,
      rol,
      sucursal,
      telefono,
    } = formField;

    if (!areAllFieldsFill()) {
      presenter("Todos los campos deben de estar llenos");
      return;
    }

    if (password1 !== password2) {
      presenter("Las contraseñas no coinciden");
      return;
    }

    if (password1.length !== 8) {
      presenter("Contraseña invalida");
      return;
    }

    setLoading(true);

    try {
      await usersApi.addUser({
        nombre: nombre,
        apellido: apellido,
        sucursal_id: sucursal,
        rol: rol,
        password: password1,
        email: email.toLowerCase(),
        status: 1,
        telefono: telefono,
      });
      setShowModal(false);
      await fetchData();
    } catch (e) {
      console.log(e);
    }
    setLoading(false);
  };

  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Nuevo usuario</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <label htmlFor="">Nombre</label>
              <input
                type="text"
                id="nombre"
                className="form-control"
                {...getLabelProps("nombre")}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Apellido</label>
              <input
                type="text"
                id="apellido"
                className="form-control"
                {...getLabelProps("apellido")}
              />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Rol</label>
              <select
                defaultValue="def"
                id="rol"
                className="form-control"
                {...getLabelProps("rol")}
              >
                <option value="">Seleccione un rol</option>
                <option value="1">Director</option>
                <option value="2">Gerente de Sucursal</option>
                <option value="3">Logistica</option>
                <option value="4">Chofer</option>
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="">Sucursal</label>
              <select
                id="sucursal"
                className="form-control"
                {...getLabelProps("sucursal")}
              >
                <option value="0" disabled>
                  Seleccione una sucursal
                </option>
                {sucur.map(({ id, nombre_sucursal }) => (
                  <option value={id}>{nombre_sucursal}</option>
                ))}
              </select>
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Contraseña</label>
              <input
                type="text"
                id="password1"
                className="form-control"
                {...getLabelProps("password1")}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Confirmar contraseña</label>
              <input
                type="text"
                id="password2"
                className="form-control"
                {...getLabelProps("password2")}
              />
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <label htmlFor="">Email</label>
              <input
                type="text"
                id="email"
                className="form-control"
                {...getLabelProps("email")}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Telefono</label>
              <input
                type="phone"
                id="telefono"
                className="form-control"
                {...getLabelProps("telefono")}
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton onClick={submit} color="bwater">
          Guardar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default ModalAddUser;
