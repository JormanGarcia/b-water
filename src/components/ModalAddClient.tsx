import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
  IonItem,
  IonCheckbox,
  IonLabel,
} from "@ionic/react";
import { FC, useEffect, useState } from "react";
import { useSucursalesApi } from "../api/useSucursalesApi";
import { useClientApi } from "../api/useClientApi";

import useFormFields from "../hooks/useFormFields";
import { sucursalesType } from "../models/sucuarsalesType.interface";
import { setLoading } from "../store/systemStateSlice";
import { useUserApi } from "../api/useUserApi";
import useSystemState from "../hooks/useSystemState";
import { useIonToast } from "@ionic/react";
import { useToast } from "../hooks/useToast";
import { GetTodayDate } from "../utils";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
  fetchClients?: () => void;
  forClients?: boolean;
}

const ModalAddClient = ({
  showModal,
  setShowModal,
  fetchClients,
  forClients,
}: WithModalProps) => {
  const [sucur, setSucur] = useState<sucursalesType[]>([]);
  const [userID, setUserID] = useState("");

  const sucursalesApi = useSucursalesApi();
  const clientApi = useClientApi();
  const usersApi = useUserApi();
  const { pushNotification } = useSystemState();
  const { presenter } = useToast();

  const [showClientForm, setShowClientForm] = useState<boolean>(false);

  const InitialClient = {
    estado: "Jalisco",
    ciudad: "Guadalajara",
    fraccionamiento: "",
    coto: "",
    casa: "",
    calle: "",
    avenida: "",
    tipo: "",
    sucursal: "",
    rfc: "",
    codigoPostal: "",
    email: "",
    estatus: "1",
    nombre_fam1: "",
    tel_fam1: "",
    nombre_fam2: "",
    tel_fam2: "",
    fechaPedido: "",
    isNewClient: forClients ? true : false,
    email_cliente: "",
  };

  const InitialUser = {
    nombre: "",
    apellido: "",
    rol: 5,
    sucursal: 0,
    password1: forClients ? "" : "bwater123",
    email: "",
    telefono: "",
  };

  const clientFormFields = useFormFields(InitialClient);

  const addUserForm = useFormFields(InitialUser);

  const onClose = async () => {
    setShowModal(false);
    setUserID("");
    setShowClientForm(false);
    clientFormFields.setFormFields(InitialClient);
    addUserForm.setFormFields(InitialUser);
  };

  const fetchSucursales = async () => {
    setLoading(true);

    const sucursalesRes = await sucursalesApi.getSucursales();

    setSucur(sucursalesRes);
    console.log(sucursalesRes, "sucursales registradas");

    setLoading(false);
  };

  useEffect(() => {
    fetchSucursales();
  }, []);

  const submitUser = async () => {
    const { apellido, email, nombre, password1, rol, sucursal, telefono } =
      addUserForm.formField;

    if (nombre === "" || apellido === "") {
      presenter("Nombre y/o apellido invalido");
      return;
    }

    if (telefono.length !== 10) {
      presenter("Telefono invalido");
      return;
    }

    if (sucursal === 0) {
      presenter("El usuario necesita estar afiliado a una sucursal");
      return;
    }

    setLoading(true);

    try {
      const newUser = await usersApi.addUser({
        nombre: nombre,
        apellido: apellido,
        sucursal_id: sucursal,
        rol: rol,
        password: password1,
        email: email.toLowerCase(),
        status: 1,
        telefono: telefono,
      });
      if (newUser!.user.data) {
        console.log(newUser!.user.data);
        setUserID(newUser!.user.data.object[0].id);
        setShowClientForm(true);
      }
    } catch (e) {
      console.log(e);
    }

    setLoading(false);
  };

  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => onClose()}
    >
      <IonHeader className="modal__header">Registro de Nuevo Cliente</IonHeader>

      {!showClientForm ? (
        <AddUserForm
          forClients={forClients || false}
          getLabelProps={addUserForm.getLabelProps}
          sucur={sucur}
        />
      ) : (
        <ClientForm
          getLabelProps={clientFormFields.getLabelProps}
          forClients={forClients || false}
          setCheckValue={(a: boolean) =>
            clientFormFields.setFormFields({
              ...clientFormFields.formField,
              isNewClient: a,
            })
          }
        />
      )}

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={async () => {
            if (userID) {
              await usersApi.deleteUser(+userID);
            }

            onClose();
          }}
        >
          Cancelar
        </IonButton>
        {showClientForm ? (
          <IonButton
            color="bwater"
            style={{ marginRight: "1rem" }}
            onClick={async () => {
              await clientApi.addClients({
                fraccionamiento: clientFormFields.formField.fraccionamiento,
                coto: clientFormFields.formField.coto,
                casa: clientFormFields.formField.casa,
                calle: clientFormFields.formField.calle,
                avenida: clientFormFields.formField.avenida,
                estado: clientFormFields.formField.estado,
                ciudad: clientFormFields.formField.ciudad,
                rfc: clientFormFields.formField.rfc,
                codigo_postal: clientFormFields.formField.codigoPostal,
                estatus_cliente: clientFormFields.formField.estatus,
                tipo_cliente: clientFormFields.formField.tipo,
                user_id: userID,
                nom_fam_1: clientFormFields.formField.nombre_fam1,
                nom_fam_2: clientFormFields.formField.nombre_fam2,
                tel_fam_1: clientFormFields.formField.tel_fam1,
                tel_fam_2: clientFormFields.formField.tel_fam2,
                fecha_pedido: clientFormFields.formField.fechaPedido,
                cliente_nuevo: clientFormFields.formField.isNewClient,
                email_cliente: clientFormFields.formField.email_cliente,
              });

              if (fetchClients) {
                fetchClients();
              }

              setShowModal(false);

              presenter("Usuario Creado Satisfactoriamente");
            }}
          >
            Agregar
          </IonButton>
        ) : (
          <IonButton
            color="bwater"
            style={{ marginRight: "1rem" }}
            onClick={submitUser}
          >
            Siguiente
          </IonButton>
        )}
      </IonFooter>
    </IonModal>
  );
};

const ClientForm: FC<{
  getLabelProps: any;
  setCheckValue: (a: boolean) => any;
  forClients: boolean;
}> = ({ getLabelProps, setCheckValue, forClients }) => {
  return (
    <IonContent>
      <IonItem
        style={{
          padding: 10,
        }}
      >
        <IonCheckbox
          disabled={forClients}
          checked={getLabelProps("isNewClient").value}
          style={{ marginRight: 5 }}
          onIonChange={(e) => setCheckValue(e.detail.checked)}
        />
        <IonLabel>Cliente Nuevo</IonLabel>
      </IonItem>
      <h5 className="modal__title">Dirección del cliente: </h5>

      <IonGrid>
        <IonRow className="form-row-separate">
          <IonCol>
            <input
              type="text"
              className="form-control"
              placeholder="Estado"
              {...getLabelProps("estado")}
              disabled
            />
          </IonCol>
          <IonCol>
            <select
              className="form-control"
              placeholder="Ciudad"
              {...getLabelProps("ciudad")}
              disabled
            >
              <option value="Guadalajara">Guadalajara</option>
            </select>
          </IonCol>
        </IonRow>
        <IonRow className="form-row-separate">
          <IonCol>
            <input
              type="text"
              className="form-control"
              placeholder="Fraccionamiento"
              {...getLabelProps("fraccionamiento")}
            />
          </IonCol>
          <IonCol>
            <input
              type="text"
              className="form-control"
              placeholder="Coto"
              {...getLabelProps("coto")}
            />
          </IonCol>
        </IonRow>
        <IonRow className="form-row-separate">
          <IonCol>
            <input
              type="text"
              className="form-control"
              placeholder="Casa"
              {...getLabelProps("casa")}
            />
          </IonCol>
          <IonCol>
            <input
              type="text"
              className="form-control"
              placeholder="Calle"
              {...getLabelProps("calle")}
            />
          </IonCol>
          <IonCol>
            <input
              type="text"
              className="form-control"
              placeholder="Avenida"
              {...getLabelProps("avenida")}
            />
          </IonCol>
        </IonRow>
      </IonGrid>
      <IonRow>
        <IonCol size="6">
          <label>Tipo de cliente</label>

          <select className="form-control" {...getLabelProps("tipo")}>
            <option value="" disabled>
              Seleccione Tipo de cliente
            </option>
            <option value="Residencial">Residencial</option>
            <option value="Punto de Venta">Punto de venta</option>
            <option value="Negocio">Negocio</option>
            <option value="Sucursal">Sucursal</option>
            <option value="Independiente">Independiente</option>
          </select>
        </IonCol>
      </IonRow>

      <p
        style={{
          padding: 10,
        }}
      >
        Registra hasta 2 familiares que residan contigo, como contactos
        alternativos y que regularmente tambien hacen pedidos (opcional).
      </p>

      <IonGrid>
        <IonRow>
          <IonCol size="4">
            <label>Telefono</label>

            <input
              type="text"
              className="form-control"
              placeholder="Telefono"
              {...getLabelProps("tel_fam1")}
            />
          </IonCol>
          <IonCol size="4">
            <label>Nombre y apellido</label>
            <input
              type="text"
              className="form-control"
              placeholder="Nombre y apellido"
              {...getLabelProps("nombre_fam1")}
            />
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol size="4">
            <input
              type="text"
              className="form-control"
              placeholder="Telefono"
              {...getLabelProps("tel_fam2")}
            />
          </IonCol>
          <IonCol size="4">
            <input
              type="text"
              className="form-control"
              placeholder="Nombre y apellido"
              {...getLabelProps("nombre_fam2")}
            />
          </IonCol>
        </IonRow>
      </IonGrid>

      <h5 className="modal__title">Datos de facturación (opcional): </h5>

      <IonGrid>
        <IonRow className="form-row-separate">
          <IonCol>
            <input
              type="text"
              className="form-control"
              placeholder="RFC"
              {...getLabelProps("rfc")}
            />
          </IonCol>
          <IonCol>
            <input
              type="text"
              className="form-control"
              placeholder="Código postal"
              {...getLabelProps("codigoPostal")}
            />
          </IonCol>
          <IonCol>
            <input
              id="email"
              placeholder="email"
              className="form-control"
              {...getLabelProps("email_cliente")}
            />
          </IonCol>
        </IonRow>
        <IonRow className="form-row-separate">
          <IonCol size="6">
            <label htmlFor="">Fecha de ultimo pedido</label>
            <input
              type="date"
              id="email"
              className="form-control"
              {...getLabelProps("fechaPedido")}
            />
          </IonCol>
        </IonRow>
      </IonGrid>
    </IonContent>
  );
};

const AddUserForm: FC<{
  getLabelProps: any;
  sucur: sucursalesType[];
  forClients: Boolean;
}> = ({ getLabelProps, sucur, forClients }) => {
  return (
    <IonContent>
      <IonGrid>
        <IonRow className="form-row-separate">
          <IonCol>
            <label htmlFor="">Telefono</label>
            <input
              type="phone"
              id="telefono"
              className="form-control"
              {...getLabelProps("telefono")}
            />
          </IonCol>
          <IonCol>
            <label htmlFor="">Contraseña</label>
            <input
              type="password"
              id="password1"
              className="form-control"
              disabled={!forClients}
              {...getLabelProps("password1")}
            />
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol>
            <label htmlFor="">Nombre</label>
            <input
              type="text"
              id="nombre"
              className="form-control"
              {...getLabelProps("nombre")}
            />
          </IonCol>
          <IonCol>
            <label htmlFor="">Apellido</label>
            <input
              type="text"
              id="apellido"
              className="form-control"
              {...getLabelProps("apellido")}
            />
          </IonCol>
        </IonRow>
        <IonRow className="form-row-separate">
          <IonCol>
            <label htmlFor="">Email</label>
            <input
              type="text"
              id="email"
              className="form-control"
              {...getLabelProps("email")}
            />
          </IonCol>
          <IonCol>
            <label htmlFor="">Sucursal</label>
            <select
              id="sucursal"
              className="form-control"
              {...getLabelProps("sucursal")}
            >
              <option value="0" disabled>
                Seleccione una sucursal
              </option>
              {sucur.map(({ id, nombre_sucursal }) => (
                <option value={id}>{nombre_sucursal}</option>
              ))}
            </select>
          </IonCol>
        </IonRow>
      </IonGrid>
    </IonContent>
  );
};

export default ModalAddClient;
