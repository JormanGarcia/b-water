const SucursalItem = ({ img, desc }: { img: any; desc: any }) => (
  <div className="sucursal-item">
    <img src={img} alt={desc} className="sucursal-item__img" />
    <p className="sucursal-item__desc">{desc}</p>
  </div>
);

export default SucursalItem;
