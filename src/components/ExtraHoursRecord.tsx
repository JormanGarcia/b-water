import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
} from "@ionic/react";
import { GetTodayDate } from "../utils";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

const ExtraHoursRecord = ({ showModal, setShowModal }: WithModalProps) => {
  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Registro de Horas Extras</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <label htmlFor="">Trabajador</label>
              <select className="form-control">
                <option>Seleccione Trabajador</option>
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha Laborada</label>
              <input
                type="date"
                className="form-control"
                placeholder="Fecha de ingreso"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de Registro</label>
              <input
                type="date"
                className="form-control"
                placeholder="Fecha de Registro"
                value={GetTodayDate()}
                disabled
              />
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label>Nombres</label>
              <input
                type="text"
                className="form-control"
                placeholder="Nombres"
              />
            </IonCol>
            <IonCol>
              <label>Apellidos</label>
              <input
                type="text"
                className="form-control"
                placeholder="Apellidos"
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label>Cargo</label>
              <input type="text" className="form-control" placeholder="Cargo" />
            </IonCol>
            <IonCol>
              <label>Sucursal</label>
              <select className="form-control">
                <option>Sucursal</option>
              </select>
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label>Numero de Horas Extras</label>
              <input
                type="number"
                className="form-control"
                placeholder="Horas Extras"
              />
            </IonCol>
            <IonCol>
              <label>Valor de la hora extra</label>
              <input
                type="number"
                className="form-control"
                placeholder="Valor de hora"
              />
            </IonCol>
            <IonCol>
              <label>Total de Pago Extra</label>
              <input
                type="text"
                className="form-control"
                placeholder="Pago Extra"
              />
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol size="6">
              <label>Observacion</label>
              <textarea className="form-control" placeholder="Observacion" />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton color="bwater" style={{ marginRight: "1rem" }}>
          Agregar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default ExtraHoursRecord;
