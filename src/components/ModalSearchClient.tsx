import {
  IonModal,
  IonHeader,
  IonContent,
  IonIcon,
  IonButton,
  IonFooter,
} from "@ionic/react";
import { searchOutline } from "ionicons/icons";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

const ModalSearchClient = ({ showModal, setShowModal }: WithModalProps) => (
  <IonModal
    isOpen={showModal}
    cssClass="modal"
    swipeToClose={true}
    onDidDismiss={() => setShowModal(false)}
  >
    <IonHeader className="modal__header">Búsqueda de clientes</IonHeader>
    <IonContent className="ion-padding-horizontal ion-padding-vertical">
      <div className="search">
        <button>
          <IonIcon md={searchOutline} />
        </button>
        <input
          type="text"
          style={{ width: "100%" }}
          className="search__input"
          placeholder="Buscar por ID, Nombre o Apellido"
        />
      </div>
      <table className="table">
        <thead>
          <tr className="ion-text-left">
            <th></th>
            <th>ID Cliente</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Télefono</th>
            <th>Dirección</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <input type="radio" name="client" />
            </td>
            <td>123</td>
            <td>Homero</td>
            <td>Moncayo</td>
            <td>1020304050</td>
            <td>Venezuela</td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="client" />
            </td>
            <td>123</td>
            <td>Homero</td>
            <td>Moncayo</td>
            <td>1020304050</td>
            <td>Venezuela</td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="client" />
            </td>
            <td>123</td>
            <td>Homero</td>
            <td>Moncayo</td>
            <td>1020304050</td>
            <td>Venezuela</td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="client" />
            </td>
            <td>123</td>
            <td>Homero</td>
            <td>Moncayo</td>
            <td>1020304050</td>
            <td>Venezuela</td>
          </tr>
        </tbody>
      </table>
    </IonContent>
    <IonFooter className="modal__footer">
      <IonButton
        color="bwater"
        style={{ marginRight: "1rem" }}
        onClick={() => {
          setShowModal(false);
        }}
      >
        Cancelar
      </IonButton>
      <IonButton color="bwater">Seleccionar</IonButton>
    </IonFooter>
  </IonModal>
);

export default ModalSearchClient;
