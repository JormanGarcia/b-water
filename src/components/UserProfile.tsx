import {
  IonIcon,
  useIonPopover,
  IonList,
  IonListHeader,
  IonItem,
} from "@ionic/react";
import { personCircleOutline } from "ionicons/icons";
import { memo } from "react";
import UseAuth from "../hooks/useAuth";
import useFirebaseAuth from "../hooks/useFirebaseAuth";
import { RolSelector } from "../utils";

const UserProfile = () => {
  const { user, logoutUser } = UseAuth();

  const PopoverList = ({ onHide }: { onHide: any }) => (
    <IonList className="pt-0">
      <IonListHeader class="list-header" color="bwater">
        <img
          src="https://www.cobdoglaps.sa.edu.au/wp-content/uploads/2017/11/placeholder-profile-sq.jpg"
          alt=""
          className="list-header__user-img"
        />
        {user?.nombre} {user?.apellido} <br />
        {RolSelector(user?.rol!)}
      </IonListHeader>
      {/* <IonItem button>Documentation</IonItem>
      <IonItem button>Showcase</IonItem>
      <IonItem button>GitHub Repo</IonItem> */}
      <IonItem
        lines="none"
        detail={false}
        button
        onClick={async () => {
          await logoutUser();
          onHide();
        }}
      >
        Cerrar Sesión
      </IonItem>
    </IonList>
  );

  const [present, dismiss] = useIonPopover(PopoverList, {
    onHide: () => dismiss(),
  });

  return (
    <button className="notifications">
      <IonIcon
        md={personCircleOutline}
        ios={personCircleOutline}
        onClick={(e) =>
          present({
            event: e.nativeEvent,
          })
        }
      ></IonIcon>
    </button>
  );
};

export default memo(UserProfile);
