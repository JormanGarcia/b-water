import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
  IonItem,
  IonLabel,
  IonCheckbox,
} from "@ionic/react";
import { useEffect, useState } from "react";
import { useSucursalesApi } from "../api/useSucursalesApi";
import { useClientApi } from "../api/useClientApi";

import useFormFields from "../hooks/useFormFields";
import { sucursalesType } from "../models/sucuarsalesType.interface";
import { setLoading } from "../store/systemStateSlice";
import { IClient } from "../models/client.interface";

interface WithModalProps {
  clientId: string | null;
  setClientId: (a: string | null) => void;
  fetchClients: () => void;
}

const ModalAddClient = ({
  setClientId,
  clientId,
  fetchClients,
}: WithModalProps) => {
  const [sucur, setSucur] = useState<sucursalesType[]>([]);
  const [edit, setEdit] = useState(false);

  const sucursalesApi = useSucursalesApi();
  const clientApi = useClientApi();

  const { formField, getLabelProps, setFormFields } = useFormFields({
    telefono_cliente: "",
    nombre_cliente: "",
    apellido_cliente: "",
    estado: "",
    ciudad: "",
    fraccionamiento: "",
    coto: "",
    casa: "",
    calle: "",
    avenida: "",
    tipo_cliente: "",
    rfc: "",
    codigo_postal: "",
    estatus_cliente: "1",
    id: 0,
    nom_fam_1: "",
    tel_fam_1: "",
    nom_fam_2: "",
    tel_fam_2: "",
    correo_cliente: "",
    cliente_nuevo: 0,
  });

  console.log(formField.cliente_nuevo);

  const fetchData = async (id: string) => {
    setLoading(true);

    const clientResponse = await clientApi.getClientById(id);
    const sucursalesResponse = await sucursalesApi.getSucursales();
    setFormFields(clientResponse);
    setSucur(sucursalesResponse);
    console.log(clientResponse);

    setLoading(false);
  };

  const setCheckValue = (a: number) => {
    console.log(a);
    setFormFields({
      ...formField,
      cliente_nuevo: a,
    });
  };

  useEffect(() => {
    console.log("fetching...");
    if (clientId !== null) {
      fetchData(clientId);
    }

    //   fetchSucursales();
  }, [clientId]);

  return (
    <IonModal
      isOpen={clientId !== null}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setClientId(null)}
    >
      <IonHeader className="modal__header">Detalles del cliente</IonHeader>
      <IonContent>
        {/*       <h5 className="modal__title">Datos del cliente: </h5>
        <IonGrid>
          <IonRow>
            <IonCol size="6">
              <label htmlFor="">Télefono</label>
              <input
                type="text"
                className="form-control"
                placeholder="Télefono"
                {...getLabelProps("telefono_cliente")}
                disabled={!edit}
              />
            </IonCol>
            <IonCol size="6">
              <label htmlFor="">Client Id</label>
              <input
                type="text"
                className="form-control"
                placeholder="Télefono"
                disabled
                {...getLabelProps("id")}
              />
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol>
              <label htmlFor="">Nombre</label>
              <input
                type="text"
                className="form-control"
                placeholder="Nombre"
                {...getLabelProps("nombre_cliente")}
                disabled={!edit}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Apellido</label>
              <input
                type="text"
                className="form-control"
                placeholder="Apellido"
                {...getLabelProps("apellido_cliente")}
                disabled={!edit}
              />
            </IonCol>
          </IonRow>
  </IonGrid> */}

        <IonItem
          style={{
            padding: 10,
          }}
        >
          <IonCheckbox
            disabled={!edit}
            onIonChange={(e) => setCheckValue(e.detail.checked ? 1 : 0)}
            checked={formField.cliente_nuevo === 1}
            style={{ marginRight: 5 }}
          />
          <IonLabel>Cliente Nuevo</IonLabel>
        </IonItem>

        <h5 className="modal__title">Dirección del cliente: </h5>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="Fraccionamiento"
                {...getLabelProps("fraccionamiento")}
                disabled={!edit}
              />
            </IonCol>
            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="Coto"
                {...getLabelProps("coto")}
                disabled={!edit}
              />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="Casa"
                {...getLabelProps("casa")}
                disabled={!edit}
              />
            </IonCol>
            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="Calle"
                {...getLabelProps("calle")}
                disabled={!edit}
              />
            </IonCol>
            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="Avenida"
                {...getLabelProps("avenida")}
                disabled={!edit}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="Estado"
                {...getLabelProps("estado")}
                disabled
              />
            </IonCol>
            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="Ciudad"
                {...getLabelProps("ciudad")}
                disabled
              />
            </IonCol>
          </IonRow>
        </IonGrid>
        <IonRow>
          <IonCol size="6">
            <label>Tipo de cliente</label>

            <select
              className="form-control"
              {...getLabelProps("tipo_cliente")}
              disabled={!edit}
            >
              <option value="Residencial">Residencial</option>
              <option value="Punto de Venta">Punto de venta</option>
              <option value="Negocio">Negocio</option>
            </select>
          </IonCol>
        </IonRow>
        <p
          style={{
            padding: 10,
          }}
        >
          Registra hasta 2 familiares que residan contigo, como contactos
          alternativos y que regularmente tambien hacen pedidos (opcional).
        </p>

        <IonRow>
          <IonCol size="4">
            <label>Telefono</label>

            <input
              disabled={!edit}
              type="text"
              className="form-control"
              placeholder="Telefono"
              {...getLabelProps("tel_fam_1")}
            />
          </IonCol>
          <IonCol size="4">
            <label>Nombre y apellido</label>
            <input
              disabled={!edit}
              type="text"
              className="form-control"
              placeholder="Nombre y apellido"
              {...getLabelProps("nom_fam_1")}
            />
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol size="4">
            <input
              disabled={!edit}
              type="text"
              className="form-control"
              placeholder="Telefono"
              {...getLabelProps("tel_fam_2")}
            />
          </IonCol>
          <IonCol size="4">
            <input
              disabled={!edit}
              type="text"
              className="form-control"
              placeholder="Nombre y apellido"
              {...getLabelProps("nom_fam_2")}
            />
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol size="6">
            <label>Sucursal</label>

            <select id="sucursal" className="form-control" disabled={!edit}>
              <option value="0" disabled>
                ! Seleccione una sucursal
              </option>
              {sucur.map(({ id, nombre_sucursal }) => (
                <option value={id}>{nombre_sucursal}</option>
              ))}
            </select>
          </IonCol>
        </IonRow>

        <h5 className="modal__title">Datos de facturación: </h5>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="RFC"
                {...getLabelProps("rfc")}
                disabled={!edit}
              />
            </IonCol>

            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="Código postal"
                {...getLabelProps("codigo_postal")}
                disabled={!edit}
              />
            </IonCol>

            <IonCol size="12">
              <input
                type="email"
                className="form-control"
                placeholder="Correo electrónico"
                disabled={!edit}
                {...getLabelProps("correo_cliente")}
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setClientId(null);
            setEdit(false);
          }}
        >
          Cancelar
        </IonButton>
        {!edit ? (
          <IonButton
            color="bwater"
            style={{ marginRight: "1rem" }}
            onClick={() => setEdit(true)}
          >
            Editar
          </IonButton>
        ) : (
          <IonButton
            color="bwater"
            style={{ marginRight: "1rem" }}
            onClick={async () => {
              await clientApi.updateClient(
                {
                  ...formField,
                },
                clientId!
              );
              await fetchClients();
              setClientId(null);
            }}
          >
            Guardar
          </IonButton>
        )}

        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={async () => {
            await clientApi.deleteClient(clientId!);
            await fetchClients();
            setClientId(null);
          }}
        >
          Eliminar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default ModalAddClient;
