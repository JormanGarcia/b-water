import { NavContext } from "@ionic/react";
import UseAuth from "../hooks/useAuth";
import { ReactChild, useContext, useEffect } from "react";

interface ComponentPropsTypes {
  children: any;
}

const AuthRedirection = ({ children }: ComponentPropsTypes) => {
  const { isAuthenticated, user } = UseAuth();
  const { navigate } = useContext(NavContext);

  useEffect(() => {
    if (!isAuthenticated) {
      navigate("/login");
    }
  }, [isAuthenticated]);

  return <>{children}</>;
};

export default AuthRedirection;
