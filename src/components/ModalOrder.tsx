import {
  IonContent,
  IonModal,
  IonHeader,
  IonFooter,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
} from "@ionic/react";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

const ModalOrder = ({ showModal, setShowModal }: WithModalProps) => {
  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Detalles del pedido</IonHeader>
      <IonContent className="ion-padding-horizontal">
        <h5 className="modal__title">Datos del cliente: </h5>
        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Cliente</label>
              <input
                type="text"
                className="form-control"
                placeholder="ID cliente"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Sucursal</label>
              <input
                type="text"
                className="form-control"
                placeholder="Sucursal"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Chofer</label>
              <input
                type="text"
                className="form-control"
                placeholder="Chofer"
              />
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol>
              <label htmlFor="">Nombre</label>
              <input
                type="text"
                className="form-control"
                placeholder="Nombre"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Apellido</label>
              <input
                type="text"
                className="form-control"
                placeholder="Apellido"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Télefono</label>
              <input
                type="text"
                className="form-control"
                placeholder="Télefono"
              />
            </IonCol>
          </IonRow>
        </IonGrid>

        <h5 className="modal__title">Dirección del cliente: </h5>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Fraccionamiento</label>
              <input
                type="text"
                className="form-control"
                placeholder="Fraccionamiento"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Coto</label>
              <input type="text" className="form-control" placeholder="Coto" />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Casa</label>
              <input type="text" className="form-control" placeholder="Casa" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Calle</label>
              <input type="text" className="form-control" placeholder="Calle" />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">RFC</label>
              <input type="text" className="form-control" placeholder="RFC" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Código postal</label>
              <input
                type="text"
                className="form-control"
                placeholder="Código postal"
              />
            </IonCol>
          </IonRow>
        </IonGrid>

        <table className="table">
          <thead>
            <tr>
              <th>Producto</th>
              <th>Cantidad</th>
              <th>P. Unit</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Garrafon nuevo</td>
              <td>1</td>
              <td>54</td>
              <td>77</td>
            </tr>
            <tr>
              <td>Garrafon nuevo</td>
              <td>1</td>
              <td>54</td>
              <td>77</td>
            </tr>
            <tr>
              <td>Garrafon nuevo</td>
              <td>1</td>
              <td>54</td>
              <td>77</td>
            </tr>
            <tr>
              <td>Garrafon nuevo</td>
              <td>1</td>
              <td>54</td>
              <td>77</td>
            </tr>
            <tr className="table-bottom">
              <td></td>
              <td></td>
              <td>Sub total</td>
              <td></td>
            </tr>
            <tr className="table-bottom">
              <td></td>
              <td></td>
              <td>Descuento</td>
              <td></td>
            </tr>
            <tr className="table-bottom">
              <td></td>
              <td></td>
              <td>Total</td>
              <td></td>
            </tr>
          </tbody>
        </table>

        <IonRow>
          <IonCol>
            <label htmlFor="">Método de pago</label>
            <input
              type="text"
              className="form-control"
              value="Efectivo"
              placeholder="Nombre"
            />
          </IonCol>
        </IonRow>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cerrar
        </IonButton>
        <IonButton color="bwater" style={{ marginRight: "1rem" }}>
          Editar
        </IonButton>
        <IonButton color="bwater" style={{ marginRight: "1rem" }}>
          Eliminar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default ModalOrder;
