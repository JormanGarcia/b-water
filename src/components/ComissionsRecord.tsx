import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
} from "@ionic/react";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

const ExtraHoursRecord = ({ showModal, setShowModal }: WithModalProps) => {
  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Registro de Comisiones</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <label htmlFor="">Trabajador</label>
              <select className="form-control">
                <option>Seleccione Trabajador</option>
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha Correspondiente</label>
              <input
                type="date"
                className="form-control"
                placeholder="Fecha de Factura"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de Registro</label>
              <input
                type="date"
                className="form-control"
                placeholder="Fecha de Registro"
              />
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label>Nombre</label>
              <input className="form-control" placeholder="Nombre" />
            </IonCol>
            <IonCol>
              <label>Apellidos</label>
              <input className="form-control" placeholder="Apellidos" />
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol>
              <label>Cargo</label>
              <input className="form-control" placeholder="Usuario Logueado" />
            </IonCol>
            <IonCol>
              <label>Sucursal</label>
              <input className="form-control" placeholder="Usuario Logueado" />
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <label>Monto</label>
              <input
                className="form-control"
                placeholder="monto"
                type="number"
              />
            </IonCol>
            <IonCol>
              <label>Asignado Por</label>
              <input className="form-control" placeholder="Usuario Logueado" />
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol size="6">
              <label>Concepto </label>
              <textarea
                placeholder="Concepto"
                className="form-control"
              ></textarea>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton color="bwater" style={{ marginRight: "1rem" }}>
          Agregar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default ExtraHoursRecord;
