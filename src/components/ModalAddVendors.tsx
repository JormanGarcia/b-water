import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonModal,
  IonHeader,
  IonFooter,
} from "@ionic/react";
import { useProveedoresApi } from "../api/useProveedoresApi";
import useFormFields from "../hooks/useFormFields";
import UseSystemState from "../hooks/useSystemState";
import { proveedoresType } from "../models/proveedoresTypes.interface";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => any;
  fetchVendors: () => void;
}

enum InputsNames {
  direccion = "direccion_proveedor",
  razonSocial = "razon_social",
  representante = "representante",
  rfc = "rfc",
  telefono = "telefono_proveedor",
}

const ModalAddVendor = ({
  showModal,
  setShowModal,
  fetchVendors,
}: WithModalProps) => {
  const { formField, areAllFieldsFill, getLabelProps } = useFormFields({
    direccion_proveedor: "",
    razon_social: "",
    representante: "",
    rfc: 0,
    telefono_proveedor: "",
  });

  const proveedoresApi = useProveedoresApi();

  const { setLoading, pushNotification } = UseSystemState();

  const submit = async (vendor: proveedoresType) => {
    if (!areAllFieldsFill) {
      pushNotification("Todos Los campos deben de estar llenos");
      return;
    }

    setLoading(true);
    await proveedoresApi.addProveedor(vendor);
    await fetchVendors();
    setShowModal(false);
    setLoading(false);
  };

  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Nuevo proveedor</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Telefono</label>
              <input
                name={InputsNames.telefono}
                type="text"
                className="form-control"
                {...getLabelProps("telefono_proveedor")}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">RFC</label>
              <input
                name={InputsNames.rfc}
                type="text"
                className="form-control"
                {...getLabelProps("rfc")}
              />
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <label htmlFor="">Razón social</label>
              <input
                name={InputsNames.razonSocial}
                type="text"
                className="form-control"
                {...getLabelProps("razon_social")}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Representante</label>
              <input
                name={InputsNames.representante}
                type="text"
                className="form-control"
                {...getLabelProps("representante")}
              />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Dirección</label>
              <input
                name={InputsNames.direccion}
                type="text"
                className="form-control"
                {...getLabelProps("direccion_proveedor")}
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton color="bwater" onClick={() => submit(formField)}>
          Guardar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default ModalAddVendor;
