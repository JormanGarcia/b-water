import {
  IonSlides,
  IonSlide,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardContent,
  IonModal,
  IonHeader,
  IonContent,
  IonFooter,
  IonIcon,
} from "@ionic/react";

import disableScroll from "disable-scroll";

import SucursalItem from "./SucursalItem";
import ModalPedidos from "./ModalPedidos";
import ModalAddClient from "./ModalAddClient";

import {
  addOutline,
  documentText,
  map,
  personAdd,
  reader,
} from "ionicons/icons";

import { useRef, useState } from "react";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

interface SucursalesProps {
  viewMap: boolean;
  setViewMap: (a: boolean) => any;
}

// Configuración del Slider
const slideOpts = {
  initialSlide: 0,
  speed: 400,
  slidesPerView: 3,
  spaceBetween: 10,
  mouseWheel: true,
};

const sucursales = [
  {
    img: "https://supermercados-paotrolado.com/wp-content/uploads/botellon-agua.jpg",
    desc: "Sucursal 1",
  },
  {
    img: "https://supermercados-paotrolado.com/wp-content/uploads/botellon-agua.jpg",
    desc: "Sucursal 2",
  },
  {
    img: "https://supermercados-paotrolado.com/wp-content/uploads/botellon-agua.jpg",
    desc: "Sucursal 3",
  },
  {
    img: "https://supermercados-paotrolado.com/wp-content/uploads/botellon-agua.jpg",
    desc: "Sucursal 4",
  },
  {
    img: "https://supermercados-paotrolado.com/wp-content/uploads/botellon-agua.jpg",
    desc: "Sucursal 5",
  },
  {
    img: "https://supermercados-paotrolado.com/wp-content/uploads/botellon-agua.jpg",
    desc: "Sucursal 6",
  },
  {
    img: "https://supermercados-paotrolado.com/wp-content/uploads/botellon-agua.jpg",
    desc: "Sucursal 7",
  },
];

const ModalFacturarPedido = ({ showModal, setShowModal }: WithModalProps) => {
  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Facturar pedido</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <label htmlFor="">Cliente: </label>
              <input
                type="text"
                className="form-control"
                placeholder="ID cliente"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Sucursal: </label>
              <input
                type="text"
                className="form-control"
                placeholder="Sucursal"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Chofer: </label>
              <input
                type="text"
                className="form-control"
                placeholder="Chofer"
              />
            </IonCol>
          </IonRow>
        </IonGrid>
        <h5 className="modal__title">Datos del cliente: </h5>
        <IonGrid>
          <IonRow>
            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="Nombre"
              />
            </IonCol>
            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="Apellido"
              />
            </IonCol>
            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="Télefono"
              />
            </IonCol>
          </IonRow>
        </IonGrid>

        <h5 className="modal__title">Dirección del cliente: </h5>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <input
                type="text"
                className="form-control"
                placeholder="Fraccionamiento"
              />
            </IonCol>
            <IonCol>
              <input type="text" className="form-control" placeholder="Coto" />
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <input type="text" className="form-control" placeholder="Casa" />
            </IonCol>
            <IonCol>
              <input type="text" className="form-control" placeholder="Calle" />
            </IonCol>
          </IonRow>
        </IonGrid>

        <h5 className="modal__title">Producto: </h5>

        <IonGrid>
          <IonRow>
            <IonCol>
              <select className="form-control" defaultValue="def">
                <option value="def" disabled>
                  Seleccione producto
                </option>
                <option value="op-1">Producto 1</option>
                <option value="op-2">Producto 2</option>
                <option value="op-3">Producto 3</option>
                <option value="op-4">Producto 4</option>
              </select>
            </IonCol>
            <IonCol>
              <input
                type="number"
                min="0"
                className="form-control"
                placeholder="Cantidad"
              />
            </IonCol>
            <IonCol>
              <input
                type="number"
                min="0"
                className="form-control"
                placeholder="Precio"
              />
            </IonCol>
          </IonRow>
          <IonRow className="footer-form">
            <IonCol size="4">
              <IonButton color="bwater" className="add-button">
                <IonIcon md={addOutline} ios={addOutline} />
              </IonButton>
            </IonCol>
            <IonCol>
              Total: <span>100</span>
            </IonCol>
          </IonRow>
        </IonGrid>

        <h5 className="modal__title">Método de pago: </h5>

        <IonGrid>
          <IonRow className="footer-form">
            <IonCol>
              <select className="form-control" defaultValue="def">
                <option value="def" disabled>
                  Seleccione el método de pago
                </option>
                <option value="op-1">Producto 1</option>
                <option value="op-2">Producto 2</option>
                <option value="op-3">Producto 3</option>
                <option value="op-4">Producto 4</option>
              </select>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton color="bwater">Facturar</IonButton>
      </IonFooter>
    </IonModal>
  );
};

const Sucursales = ({ setViewMap, viewMap }: SucursalesProps) => {
  const [showModalPedidos, setShowModalPedidos] = useState(false);
  const [showModalAddClient, setShowModalAddClient] = useState(false);
  const [showModalFPedido, setShowModalFPedido] = useState(false);

  const slideRef = useRef<HTMLIonSlidesElement>(null);

  return (
    <>
      <ModalPedidos
        showModal={showModalPedidos}
        setShowModal={setShowModalPedidos}
      />
      <ModalAddClient
        showModal={showModalAddClient}
        setShowModal={setShowModalAddClient}
      />
      <ModalFacturarPedido
        showModal={showModalFPedido}
        setShowModal={setShowModalFPedido}
      />

      <IonGrid className="sucursales__grid" style={{ marginBottom: 16 }}>
        <IonRow>
          <IonCol size="12" size-md="6" className="sucursales__col">
            <IonCard className="m-0 h-100">
              <IonCardContent className="h-100">
                <IonSlides
                  pager={false}
                  options={slideOpts}
                  class="sucursales__slider h-100"
                  onMouseEnter={() => disableScroll.on()}
                  onMouseLeave={() => disableScroll.off()}
                  onWheel={(event: any) => {
                    if (event.nativeEvent.wheelDelta > 0) {
                      slideRef.current?.slidePrev();
                    } else {
                      slideRef.current?.slideNext();
                    }
                  }}
                  ref={slideRef}
                >
                  {sucursales.map((sucursal, index) => (
                    <IonSlide key={index}>
                      <SucursalItem img={sucursal.img} desc={sucursal.desc} />
                    </IonSlide>
                  ))}
                </IonSlides>
              </IonCardContent>
            </IonCard>
          </IonCol>
          <IonCol size="12" size-md="6" className="sucursales__buttons">
            <IonRow style={{ height: "100%" }}>
              <IonCol size="6">
                <IonButton
                  style={{ height: "100%" }}
                  expand="block"
                  color="bwater"
                  onClick={() => {
                    setShowModalPedidos(true);
                  }}
                >
                  <div
                    className="ion-align-items-center"
                    style={{ display: "flex" }}
                  >
                    <IonIcon
                      md={documentText}
                      ios={documentText}
                      style={{ fontSize: 20, marginRight: 10 }}
                    />

                    <span style={{ marginTop: 1 }}>Nuevo Pedido</span>
                  </div>
                </IonButton>
              </IonCol>
              <IonCol size="6">
                <IonButton
                  style={{ height: "100%" }}
                  expand="block"
                  color="bwater"
                  onClick={() => {
                    setShowModalAddClient(true);
                  }}
                >
                  <div
                    className="ion-align-items-center"
                    style={{ display: "flex" }}
                  >
                    <IonIcon
                      md={personAdd}
                      ios={personAdd}
                      style={{ fontSize: 20, marginRight: 10 }}
                    />

                    <span style={{ marginTop: 1 }}>Nuevo cliente</span>
                  </div>
                </IonButton>
              </IonCol>
            </IonRow>

            <IonRow style={{ height: "100%" }}>
              <IonCol size="6">
                <IonButton
                  style={{ height: "100%" }}
                  expand="block"
                  color="bwater"
                  onClick={() => {
                    setShowModalFPedido(true);
                  }}
                >
                  <div
                    className="ion-align-items-center"
                    style={{ display: "flex" }}
                  >
                    <IonIcon
                      md={reader}
                      ios={reader}
                      style={{ fontSize: 20, marginRight: 10 }}
                    />

                    <span style={{ marginTop: 1 }}>Facturar Pedido</span>
                  </div>
                </IonButton>
              </IonCol>

              <IonCol size="6">
                <IonButton
                  style={{ height: "100%" }}
                  expand="block"
                  color="bwater"
                  onClick={() => {
                    viewMap ? setViewMap(false) : setViewMap(true);
                  }}
                  className="ion-justify-content-start"
                >
                  <div
                    className="ion-align-items-center"
                    style={{ display: "flex" }}
                  >
                    <IonIcon
                      md={map}
                      ios={map}
                      style={{ fontSize: 20, marginRight: 10 }}
                    />

                    <span style={{ marginTop: 1 }}>
                      {" "}
                      {viewMap ? "Ver lista" : "Ver mapa"}
                    </span>
                  </div>
                </IonButton>
              </IonCol>
            </IonRow>
          </IonCol>
        </IonRow>
      </IonGrid>
    </>
  );
};

export default Sucursales;
