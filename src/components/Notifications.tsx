import { IonCard, IonCardContent, IonButton } from "@ionic/react";

const Notifications = () => (
  <IonCard className="card-notifications">
    <IonCardContent>
      <h2 className="card-custom__title pl-0 bwater-color">Notificaciones</h2>
      <ul className="custom-list">
        <li className="custom-list__img">
          <img
            src="https://supermercados-paotrolado.com/wp-content/uploads/botellon-agua.jpg"
            alt=""
          />
          Pedido ID de fecha xx-xx-xxxx aun no ha sido entregado. Verificar con
          el Cliente.
        </li>
        <li className="custom-list__img">
          <img
            src="https://supermercados-paotrolado.com/wp-content/uploads/botellon-agua.jpg"
            alt=""
          />
          La cantidad de garrafones disponibles es baja. Comprar suministros.
        </li>
      </ul>
      <IonButton expand="block" color="bwater" routerLink="/page/notifications">
        Ver Todas
      </IonButton>
    </IonCardContent>
  </IonCard>
);

export default Notifications;
