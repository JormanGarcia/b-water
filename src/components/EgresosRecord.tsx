import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
} from "@ionic/react";
import { GetTodayDate } from "../utils";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

const ExtraHoursRecord = ({ showModal, setShowModal }: WithModalProps) => {
  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Registro de Egresos</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <label htmlFor="">Nro. de documento</label>
              <input
                type="number"
                className="form-control"
                placeholder="Fecha de ingreso"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de Factura</label>
              <input
                type="date"
                className="form-control"
                placeholder="Fecha de Factura"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de Registro</label>
              <input
                type="text"
                className="form-control"
                placeholder="Fecha de Registro"
                value={GetTodayDate()}
                disabled
              />
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label>Concepto</label>
              <input className="form-control" placeholder="Concepto" />
            </IonCol>
            <IonCol>
              <label>Proveedor</label>
              <select className="form-control">
                <option>Seleccione Proveedor</option>
              </select>
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label>Monto</label>
              <input
                type="number"
                className="form-control"
                placeholder="Nombres"
              />
            </IonCol>
            <IonCol>
              <label>Sucursal</label>
              <select className="form-control">
                <option>Seleccione Sucursal</option>
              </select>
            </IonCol>
            <IonCol>
              <label>Registrado Por</label>
              <input className="form-control" placeholder="Usuario Logueado" />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton color="bwater" style={{ marginRight: "1rem" }}>
          Agregar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default ExtraHoursRecord;
