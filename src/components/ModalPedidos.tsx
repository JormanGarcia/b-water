import { useState } from "react";
import ModalSearchClient from "./ModalSearchClient";
import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonIcon,
  IonButton,
  IonFooter,
} from "@ionic/react";
import { searchOutline, addOutline } from "ionicons/icons";

interface Venta {
  producto: string;
  cantidad: number;
  precio: number;
  precioUnitario: number;
}

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

const ModalPedidos = ({ showModal, setShowModal }: WithModalProps) => {
  const [showModalSearchClient, setShowModalSearchClient] = useState(false);
  const [ventas, setVentas] = useState<Venta[]>([
    { producto: "", cantidad: 0, precio: 0, precioUnitario: 0 },
  ]);
  return (
    <>
      <ModalSearchClient
        showModal={showModalSearchClient}
        setShowModal={setShowModalSearchClient}
      />
      <IonModal
        isOpen={showModal}
        cssClass="modal"
        swipeToClose={true}
        onDidDismiss={() => setShowModal(false)}
      >
        <IonHeader className="modal__header">Agregue nuevo pedido</IonHeader>
        <IonContent>
          <h5 className="modal__title">Cliente: </h5>
          <IonGrid>
            <IonRow>
              <IonCol>
                <label>Seleccione Cliente:</label>
                <select className="form-control" defaultValue="def">
                  <option value="def" disabled>
                    Seleccione cliente
                  </option>
                  <option value="op-1">Cliente 1</option>
                  <option value="op-2">Cliente 2</option>
                  <option value="op-3">Cliente 3</option>
                  <option value="op-4">Cliente 4</option>
                </select>
              </IonCol>
              <IonCol>
                <label>Tipo de Cliente:</label>
                <select className="form-control" defaultValue="def">
                  <option value="def" disabled>
                    Tipo de Cliente
                  </option>
                  <option value="op-1">Método 1</option>
                  <option value="op-2">Método 2</option>
                  <option value="op-3">Método 3</option>
                  <option value="op-4">Método 4</option>
                </select>
              </IonCol>
              <IonCol class="add-other">
                <button
                  className="action-icon"
                  onClick={() => {
                    setShowModalSearchClient(true);
                  }}
                >
                  <IonIcon md={searchOutline} ios={searchOutline} />
                </button>
                <IonButton color="bwater" className="add-button">
                  <IonIcon md={addOutline} ios={addOutline} />
                </IonButton>
              </IonCol>
            </IonRow>
          </IonGrid>

          <h5 className="modal__title">Datos del cliente: </h5>
          <IonGrid>
            <IonRow>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Nombre"
                />
              </IonCol>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Apellido"
                />
              </IonCol>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Télefono"
                />
              </IonCol>
            </IonRow>
          </IonGrid>

          <h5 className="modal__title">Dirección del cliente: </h5>

          <IonGrid>
            <IonRow>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Estado"
                />
              </IonCol>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Ciudad"
                />
              </IonCol>
            </IonRow>{" "}
            <IonRow>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Colonia"
                />
              </IonCol>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Avenida"
                />
              </IonCol>
            </IonRow>
            <IonRow className="form-row-separate">
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Fraccionamiento"
                />
              </IonCol>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Coto"
                />
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Casa"
                />
              </IonCol>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Calle"
                />
              </IonCol>
            </IonRow>
          </IonGrid>

          <IonGrid>
            <h5 className="modal__title">Tipo de Venta: </h5>

            {ventas.map((el) => (
              <>
                <IonRow>
                  <IonCol>
                    <select className="form-control" defaultValue="def">
                      <option value="def" disabled>
                        Tipo de Venta
                      </option>
                      <option value="op-1">Producto 1</option>
                      <option value="op-2">Producto 2</option>
                      <option value="op-3">Producto 3</option>
                      <option value="op-4">Producto 4</option>
                    </select>
                  </IonCol>
                  <IonCol>
                    <input
                      type="number"
                      min="0"
                      className="form-control"
                      placeholder="Cantidad"
                    />
                  </IonCol>
                  <IonCol>
                    <input
                      type="number"
                      min="0"
                      className="form-control"
                      placeholder="Precio Unitario"
                    />
                  </IonCol>
                  <IonCol>
                    <input
                      type="number"
                      min="0"
                      className="form-control"
                      placeholder="Total"
                    />
                  </IonCol>
                </IonRow>
              </>
            ))}

            <IonRow className="footer-form">
              <IonCol size="6">
                <IonButton
                  color="bwater"
                  className="add-button"
                  onClick={() =>
                    setVentas([
                      ...ventas,
                      {
                        producto: "",
                        cantidad: 0,
                        precio: 0,
                        precioUnitario: 0,
                      },
                    ])
                  }
                >
                  <IonIcon md={addOutline} ios={addOutline} />
                </IonButton>
              </IonCol>
              <IonCol>
                <label>Total:</label>
                <input
                  type="number"
                  min="0"
                  className="form-control"
                  placeholder="Total"
                  disabled
                />
              </IonCol>
            </IonRow>
          </IonGrid>

          <IonGrid>
            <IonRow className="form-row-separate">
              <IonCol>
                <label htmlFor="sucursal">Sucursal</label>
                <select
                  className="form-control"
                  id="sucursal"
                  defaultValue="def"
                >
                  <option value="def" disabled>
                    Seleccione sucursal
                  </option>
                  <option value="op-1">Producto 1</option>
                  <option value="op-2">Producto 2</option>
                  <option value="op-3">Producto 3</option>
                  <option value="op-4">Producto 4</option>
                </select>
              </IonCol>
              <IonCol>
                <label htmlFor="chofer">Chofer</label>
                <select className="form-control" id="chofer" defaultValue="def">
                  <option value="def" disabled>
                    Seleccione chofer
                  </option>
                  <option value="op-1">Producto 1</option>
                  <option value="op-2">Producto 2</option>
                  <option value="op-3">Producto 3</option>
                  <option value="op-4">Producto 4</option>
                </select>
              </IonCol>
            </IonRow>
          </IonGrid>

          <IonGrid>
            <IonRow>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Forma de pago"
                />
              </IonCol>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Usuario logueado"
                />
              </IonCol>
              <IonCol>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Observaciones"
                />
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>

        <IonFooter className="modal__footer">
          <IonButton
            color="bwater"
            style={{ marginRight: "1rem" }}
            onClick={() => {
              setShowModal(false);
            }}
          >
            Cancelar
          </IonButton>
          <IonButton color="bwater">Agregar</IonButton>
        </IonFooter>
      </IonModal>
    </>
  );
};

export default ModalPedidos;
