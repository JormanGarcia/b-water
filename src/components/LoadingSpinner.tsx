import { IonSpinner } from "@ionic/react";
import React from "react";
import UseSystemState from "../hooks/useSystemState";

const LoadingSpinner = () => {
  const { loading } = UseSystemState();

  const spinnerConatiner: React.CSSProperties = {
    display: "inline-flex",
    flexDirection: "column-reverse",
    position: "fixed",
    right: 20,
    bottom: 20,
    padding: 40,
    zIndex: 10000,
    width: 200,
    height: 100,
  };

  return (
    <>
      {loading && (
        <div style={spinnerConatiner}>
          <IonSpinner name="crescent" />
        </div>
      )}
    </>
  );
};

export default LoadingSpinner;
