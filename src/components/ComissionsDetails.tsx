import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
} from "@ionic/react";
import { useState } from "react";

interface WithModalProps {
  item: object | null;
  setItem: (a: object | null) => void;
}

const ComissionsDetails = ({ item, setItem }: WithModalProps) => {
  const [edit, setEdit] = useState(false);

  const closeModal = () => {
    setItem(null);
    setEdit(false);
  };

  return (
    <IonModal
      isOpen={Boolean(item !== null)}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => closeModal()}
    >
      <IonHeader className="modal__header">Detalles de Comision</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <label htmlFor="">Trabajador</label>
              <select disabled={!edit} className="form-control">
                <option>Seleccione Trabajador</option>
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha Correspondiente</label>
              <input
                disabled={!edit}
                type="date"
                className="form-control"
                placeholder="Fecha de Factura"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de Registro</label>
              <input
                disabled={!edit}
                type="date"
                className="form-control"
                placeholder="Fecha de Registro"
              />
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label>Monto</label>
              <input
                disabled={!edit}
                className="form-control"
                placeholder="Nombres"
              />
            </IonCol>
            <IonCol>
              <label>Apellidos</label>
              <input
                disabled={!edit}
                className="form-control"
                placeholder="Apellidos"
              />
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol>
              <label>Cargo</label>
              <input
                disabled={!edit}
                className="form-control"
                placeholder="Usuario Logueado"
              />
            </IonCol>
            <IonCol>
              <label>Sucursal</label>
              <input
                disabled={!edit}
                className="form-control"
                placeholder="Usuario Logueado"
              />
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <label>Monto</label>
              <input
                disabled={!edit}
                className="form-control"
                placeholder="monto"
                type="number"
              />
            </IonCol>
            <IonCol>
              <label>Asignado Por</label>
              <input
                disabled={!edit}
                className="form-control"
                placeholder="Usuario Logueado"
              />
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol size="6">
              <label>Concepto </label>
              <textarea
                disabled={!edit}
                placeholder="Concepto"
                className="form-control"
              ></textarea>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => closeModal()}
        >
          Cancelar
        </IonButton>
        {!edit ? (
          <IonButton
            color="bwater"
            style={{ marginRight: "1rem" }}
            onClick={() => setEdit(true)}
          >
            Editar
          </IonButton>
        ) : (
          <IonButton color="bwater" style={{ marginRight: "1rem" }}>
            Guardar
          </IonButton>
        )}
      </IonFooter>
    </IonModal>
  );
};

export default ComissionsDetails;
