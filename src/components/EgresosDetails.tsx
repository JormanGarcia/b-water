import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
} from "@ionic/react";
import { useState } from "react";

interface WithModalProps {
  item: object | null;
  setItem: (a: object | null) => void;
}

const EgresosDetails = ({ item, setItem }: WithModalProps) => {
  const [edit, setEdit] = useState(false);
  const closeModal = () => {
    setItem(null);
    setEdit(false);
  };
  return (
    <IonModal
      isOpen={Boolean(item !== null)}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => closeModal()}
    >
      <IonHeader className="modal__header">Registro de Egresos</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <label htmlFor="">Nro. de documento</label>
              <input
                type="number"
                className="form-control"
                placeholder="Fecha de ingreso"
                disabled={!edit}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de Factura</label>
              <input
                type="date"
                className="form-control"
                placeholder="Fecha de Factura"
                disabled={!edit}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de Registro</label>
              <input
                type="date"
                className="form-control"
                placeholder="Fecha de Registro"
                disabled={!edit}
              />
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label>Concepto</label>
              <input
                className="form-control"
                placeholder="Concepto"
                disabled={!edit}
              />
            </IonCol>
            <IonCol>
              <label>Proveedor</label>
              <select disabled={!edit} className="form-control">
                <option>Seleccione Proveedor</option>
              </select>
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label>Monto</label>
              <input
                disabled={!edit}
                type="number"
                className="form-control"
                placeholder="Nombres"
              />
            </IonCol>
            <IonCol>
              <label>Sucursal</label>
              <select disabled={!edit} className="form-control">
                <option>Seleccione Sucursal</option>
              </select>
            </IonCol>
            <IonCol>
              <label>Registrado Por</label>
              <input
                disabled={!edit}
                className="form-control"
                placeholder="Usuario Logueado"
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => closeModal()}
        >
          Cancelar
        </IonButton>
        {!edit ? (
          <IonButton
            color="bwater"
            style={{ marginRight: "1rem" }}
            onClick={() => setEdit(true)}
          >
            Editar
          </IonButton>
        ) : (
          <IonButton color="bwater" style={{ marginRight: "1rem" }}>
            Guardar
          </IonButton>
        )}
      </IonFooter>
    </IonModal>
  );
};

export default EgresosDetails;
