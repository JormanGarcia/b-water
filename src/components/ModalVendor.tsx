import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonModal,
  IonHeader,
  IonFooter,
} from "@ionic/react";
import { useEffect, useMemo, useState } from "react";
import { useProveedoresApi } from "../api/useProveedoresApi";
import useFormFields from "../hooks/useFormFields";
import UseSystemState from "../hooks/useSystemState";
import { proveedoresType } from "../models/proveedoresTypes.interface";

interface WithModalProps {
  setVendorId: (a: number | null) => any;
  fetchVendor: () => void;
  vendorId: number | null;
}

enum InputsNames {
  direccion = "direccion_proveedor",
  razonSocial = "razon_social",
  representante = "representante",
  rfc = "rfc",
  telefono = "telefono_proveedor",
  id = "id",
}

const ModalVendor = ({
  setVendorId,
  fetchVendor,
  vendorId,
}: WithModalProps) => {
  const initialState = useMemo<proveedoresType>(
    () => ({
      direccion_proveedor: "",
      razon_social: "",
      representante: "",
      rfc: 0,
      telefono_proveedor: "",
      id: 0,
    }),
    []
  );

  const { formField, getLabelProps, setFormFields } =
    useFormFields(initialState);

  const [isEdit, setIsEdit] = useState(false);

  const { setLoading } = UseSystemState();
  const proveedoresApi = useProveedoresApi();

  const deleteVendors = async () => {
    setLoading(true);
    await proveedoresApi.deleteProveedor(vendorId!);
    setVendorId(null);
    await fetchVendor();
    setLoading(false);
  };

  const updateVendors = async () => {
    setLoading(true);
    await proveedoresApi.updateProveedor(formField);
    setVendorId(null);
    await fetchVendor();
    setLoading(false);
  };

  useEffect(() => {
    if (vendorId === null) {
      setFormFields(initialState);
      setIsEdit(false);
      return;
    }

    (async () => {
      const VendorResponse = await proveedoresApi.getProveedorById(vendorId);
      const {
        direccion_proveedor,
        razon_social,
        representante,
        rfc,
        telefono_proveedor,
        id,
      } = VendorResponse;
      setLoading(true);
      setFormFields({
        direccion_proveedor,
        razon_social,
        representante,
        rfc,
        telefono_proveedor,
        id,
      });
      setLoading(false);
    })();
  }, [vendorId]);

  return (
    <IonModal
      isOpen={vendorId !== null}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setVendorId(null)}
    >
      <IonHeader className="modal__header">Detalles del proveedor</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Id</label>
              <input
                name={InputsNames.id}
                type="text"
                className="form-control"
                {...getLabelProps("id")}
                disabled
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Telefono</label>
              <input
                name={InputsNames.telefono}
                type="text"
                className="form-control"
                {...getLabelProps("telefono_proveedor")}
                disabled={!isEdit}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">RFC</label>
              <input
                name={InputsNames.rfc}
                type="text"
                className="form-control"
                {...getLabelProps("rfc")}
                disabled={!isEdit}
              />
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <label htmlFor="">Razón social</label>
              <input
                name={InputsNames.razonSocial}
                type="text"
                className="form-control"
                {...getLabelProps("razon_social")}
                disabled={!isEdit}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Representante</label>
              <input
                name={InputsNames.representante}
                type="text"
                className="form-control"
                {...getLabelProps("representante")}
                disabled={!isEdit}
              />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Dirección</label>
              <input
                name={InputsNames.direccion}
                type="text"
                className="form-control"
                {...getLabelProps("direccion_proveedor")}
                disabled={!isEdit}
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setVendorId(null);
          }}
        >
          Cerrar
        </IonButton>
        {!isEdit ? (
          <IonButton color="bwater" onClick={() => setIsEdit(true)}>
            Editar
          </IonButton>
        ) : (
          <IonButton color="bwater" onClick={() => updateVendors()}>
            Guardar
          </IonButton>
        )}
        <IonButton color="bwater" onClick={() => deleteVendors()}>
          Eliminar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default ModalVendor;
