import React from "react";
import UseSystemState from "../hooks/useSystemState";

const PopupNotification = () => {
  const { notifications } = UseSystemState();
  const listStyles: React.CSSProperties = {
    display: "inline-flex",
    flexDirection: "column-reverse",
    position: "fixed",
    left: 0,
    right: 0,
    bottom: 20,
    margin: "auto",
    padding: 20,
    zIndex: 100000,
    alignItems: "center",
  };
  return (
    <div style={listStyles}>
      {notifications.map(({ key, msg }) => {
        return <div key={key}>{msg}</div>;
      })}
    </div>
  );
};

export default PopupNotification;
