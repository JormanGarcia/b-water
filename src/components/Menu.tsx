import {
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonToolbar,
} from "@ionic/react";

import { useLocation } from "react-router-dom";

import whiteBwaterLogo from "../assets/img/white-logo.png";
import UseAuth from "../hooks/useAuth";
import getMenuLinks from "../utils/menuLinks";
import Header from "./Header";

const Menu = () => {
  const location = useLocation();
  const { user } = UseAuth();

  return (
    <>
      <IonMenu contentId="main" className="main-menu">
        <IonContent className="bg-gradient no-show-scroll">
          <IonList id="inbox-list" className="no-bg">
            <IonListHeader className="no-bg">
              <IonToolbar className="no-bg">
                <img
                  src={whiteBwaterLogo}
                  alt="Logo de BWater"
                  className="logo"
                />
              </IonToolbar>
            </IonListHeader>
            {getMenuLinks(user?.rol!)?.map((appPage: any, index: number) => {
              return (
                <IonMenuToggle key={index} autoHide={false}>
                  <IonItem
                    className={
                      location.pathname === appPage.url ||
                      location.pathname === appPage.url2 ||
                      location.pathname === appPage.url3 ||
                      location.pathname === appPage.url4 ||
                      location.pathname === appPage.url5
                        ? "selected"
                        : ""
                    }
                    routerLink={appPage.url}
                    routerDirection="none"
                    lines="none"
                    detail={false}
                  >
                    <IonIcon
                      slot="start"
                      md={appPage.icon}
                      ios={appPage.icon}
                    />
                    <IonLabel>{appPage.title}</IonLabel>
                  </IonItem>
                </IonMenuToggle>
              );
            })}
          </IonList>
        </IonContent>
      </IonMenu>
    </>
  );
};

export default Menu;
