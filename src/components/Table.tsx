import { useTable, useGlobalFilter } from "react-table";

interface TableProps {
  data: any[];
  columns: any;
  onRowClick: () => void;
}

const Table = ({ columns, data, onRowClick }: TableProps) => {
  const instance = useTable({ columns, data }, useGlobalFilter);

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    instance;

  const Header = () =>
    headerGroups.map((headerGroup) => (
      <tr {...headerGroup.getHeaderGroupProps()}>
        {headerGroup.headers.map((column) => (
          <th {...column.getHeaderProps}>{column.render("Header")}</th>
        ))}
      </tr>
    ));

  const Body = () =>
    rows.map((row) => {
      prepareRow(row);
      return (
        <tr {...row.getRowProps()}>
          {row.cells.map((cell) => (
            <td {...cell.getCellProps}>{cell.render("Cell")}</td>
          ))}
        </tr>
      );
    });

  return (
    <table className="table" {...getTableProps}>
      <thead>{Header()}</thead>
      <tbody {...getTableBodyProps()}>{Body()}</tbody>
    </table>
  );
};

export default Table;
