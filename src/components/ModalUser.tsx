import {
  IonModal,
  IonHeader,
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonFooter,
  IonButton,
} from "@ionic/react";
import React, { useState, useEffect, useMemo } from "react";
import useFormFields from "../hooks/useFormFields";
import UseSystemState from "../hooks/useSystemState";
import { sucursalesType } from "../models/sucuarsalesType.interface";
import { useUserApi } from "../api/useUserApi";
import { useSucursalesApi } from "../api/useSucursalesApi";

interface WithModalProps {
  user: number | null;
  setUser: (a: number | null) => void;
  fetchUsers: () => void;
}

const ModalUser = ({ setUser, user, fetchUsers }: WithModalProps) => {
  const initialFormValues = useMemo(
    () => ({
      id: 0,
      nombre: "",
      apellido: "",
      rol: 0,
      sucursal: 0,
      password: "",
      confirmPassword: "",
      email: "",
      telefono: "",
      uid: "",
      status: 0,
    }),
    []
  );

  const { formField, onChangeField, setFormFields, getLabelProps } =
    useFormFields(initialFormValues);

  const [isEdit, setIsEdit] = useState(true);

  const [sucursales, setSucursales] = useState<sucursalesType[]>([]);

  const { setLoading } = UseSystemState();

  const userApi = useUserApi();
  const sucursalesApi = useSucursalesApi();

  const fetchData = async () => {
    console.log("fetching...");
    const [userResponse, sucursalesResponse] = await Promise.all([
      userApi.getUsersById(user!),
      sucursalesApi.getSucursales(),
    ]);

    setSucursales(sucursalesResponse);

    setFormFields({
      ...formField,
      id: userResponse.id!,
      nombre: userResponse.nombre,
      apellido: userResponse.apellido,
      rol: userResponse.rol,
      sucursal: userResponse.sucursal_id,
      email: userResponse.email,
      uid: userResponse.uid!,
      status: userResponse.status,
      telefono: userResponse.telefono,
    });

    console.log(userResponse);
  };

  useEffect(() => {
    if (user === null) {
      setFormFields(initialFormValues);
      return;
    }

    (async () => {
      setLoading(true);
      await fetchData();
      setLoading(false);
    })();
  }, [user]);

  const deleteUser = async (id: number) => {
    setLoading(true);
    await userApi.deleteUser(id);
    await fetchUsers();
    setUser(null);
    setLoading(false);
  };

  const editUser = async (userData: typeof initialFormValues) => {
    const {
      apellido,
      email,
      id,
      nombre,
      password,
      rol,
      status,
      sucursal,
      telefono,
      uid,
    } = userData;
    setLoading(true);
    const updatedUser = await userApi.updateUser({
      id,
      nombre,
      apellido,
      email,
      sucursal_id: sucursal,
      rol,
      status,
      telefono,
      uid,
      password: password === "" ? null : password,
    });

    console.log(
      `Usuario ${updatedUser?.nombre} ${updatedUser?.apellido} ha sido editado`
    );
    setUser(null);
    fetchUsers();
    setLoading(false);
  };

  return (
    <IonModal
      isOpen={user !== null}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setUser(null)}
    >
      <IonHeader className="modal__header">Detalles del usuario</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">ID Usuarios</label>
              <input
                type="text"
                value={formField.id}
                className="form-control"
                placeholder="Generado automaticamente"
                disabled
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de registro</label>
              <input
                type="text"
                value="Fecha"
                className="form-control"
                placeholder="Generado automaticamente"
                disabled
              />
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <label htmlFor="">Nombre</label>

              <input
                type="text"
                className="form-control"
                disabled={isEdit}
                {...getLabelProps("nombre")}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Apellido</label>
              <input
                type="text"
                className="form-control"
                disabled={isEdit}
                {...getLabelProps("apellido")}
              />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Rol</label>
              <select
                defaultValue="def"
                className="form-control"
                disabled={isEdit}
                {...getLabelProps("rol")}
              >
                <option value="def" disabled>
                  Seleccione un rol
                </option>
                <option value="1">Director</option>
                <option value="2">Gerente de sucursal</option>
                <option value="3">Logistica</option>
                <option value="4">Chofer</option>
                <option value="5">Cliente</option>
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="">Sucursal</label>
              <select
                className="form-control"
                disabled={isEdit}
                {...getLabelProps("sucursal")}
              >
                {sucursales.map((el) => (
                  <option key={el.id} value={el.id}>
                    {el.nombre_sucursal}
                  </option>
                ))}
              </select>
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol>
              <label htmlFor="">Email</label>
              <input
                type="text"
                value={formField.email}
                className="form-control"
                disabled
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Telefono</label>
              <input
                type="text"
                value={formField.telefono}
                className="form-control"
                disabled={isEdit}
                onChange={onChangeField("telefono")}
              />
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol>
              <label htmlFor="">Contraseña</label>
              <input
                type="text"
                value={formField.password}
                className="form-control"
                disabled={isEdit}
                onChange={onChangeField("password")}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Confirmar contraseña</label>
              <input
                type="text"
                value={formField.confirmPassword}
                className="form-control"
                disabled={isEdit}
                onChange={onChangeField("confirmPassword")}
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setUser(null);
            setIsEdit(true);
          }}
        >
          Cerrar
        </IonButton>

        {isEdit ? (
          <IonButton
            color="bwater"
            onClick={() => setIsEdit(false)}
            style={{ marginRight: "1rem" }}
          >
            Editar
          </IonButton>
        ) : (
          <IonButton
            color="bwater"
            onClick={() => editUser(formField)}
            style={{ marginRight: "1rem" }}
          >
            Guardar
          </IonButton>
        )}

        <IonButton onClick={() => deleteUser(formField.id)} color="bwater">
          Eliminar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default ModalUser;
