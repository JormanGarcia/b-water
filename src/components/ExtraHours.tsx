import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonIcon,
} from "@ionic/react";
import ExtraHoursRecord from "../components/ExtraHoursRecord";
import ExtraHoursDetails from "../components/ExtraHoursDetails";
import ExtraHoursConfig from "../components/ExtraHoursConfig";
import { searchOutline } from "ionicons/icons";

import Header from "../components/Header";

import { useState } from "react";

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

const ExtraHours = () => {
  const [recordModal, setRecordModal] = useState(false);
  const [configModal, setConfigModal] = useState(false);
  const [modalDetails, setModalDetails] = useState<object | null>(null);

  return (
    <>
      <ExtraHoursRecord showModal={recordModal} setShowModal={setRecordModal} />
      <ExtraHoursConfig showModal={configModal} setShowModal={setConfigModal} />

      <ExtraHoursDetails item={modalDetails} setItem={setModalDetails} />

      <IonPage>
        <Header title="Personal" />
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                <IonButton color="bwater" routerLink="/page/personal">
                  Regresar a Personal
                </IonButton>
                <IonButton color="bwater" onClick={() => setRecordModal(true)}>
                  Registrar Horas Extras
                </IonButton>
                <IonButton
                  color="bwater"
                  onClick={() => {
                    setConfigModal(true);
                  }}
                >
                  Configuración de Horas Extras
                </IonButton>
                <IonCard className="m-0">
                  <IonCardHeader className="table-header">
                    <h2 className="table-title">Asignación de horas extras</h2>
                    <div style={{ display: "flex" }}>
                      <div className="search" style={{ marginRight: ".5rem" }}>
                        <button>
                          <IonIcon md={searchOutline} />
                        </button>
                        <input
                          type="text"
                          className="search__input"
                          placeholder="Buscar"
                        />
                      </div>
                    </div>
                  </IonCardHeader>
                  <IonCardContent>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nombres</th>
                          <th>Apellidos</th>
                          <th>Cargo</th>
                          <th>Sucursal</th>
                          <th>Fecha</th>
                          <th>Nro de Horas Extras</th>
                          <th>Pago Extra</th>
                        </tr>
                      </thead>
                      <tbody>
                        {numbers.map((item) => (
                          <tr
                            onClick={() => {
                              setModalDetails({});
                            }}
                          >
                            <td>1</td>
                            <td>14/02/21</td>
                            <td>1</td>
                            <td>1</td>
                            <td>100</td>
                            <td>100</td>
                            <td>100</td>
                            <td>100</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default ExtraHours;
