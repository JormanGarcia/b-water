import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonModal,
  IonHeader,
  IonFooter,
} from "@ionic/react";

import { ChangeEvent, useEffect, useState } from "react";
import { useSucursalesApi } from "../api/useSucursalesApi";
import { useUserApi } from "../api/useUserApi";
import { userType } from "../models/userType.interface";
import UseSystemState from "../hooks/useSystemState";
import useFormFields from "../hooks/useFormFields";

interface WithModalProps {
  setSucursalId: (a: number | null) => any;
  fetchSucursales: () => any;
  sucursalId: number | null;
}

enum LabelNames {
  id = "id",
  nombreGerente = "nombre_gerente",
  nombreSucursal = "nombre_sucursal",
  direccion = "direccion",
  latitud = "latitud",
  longitud = "longitud",
  telefonoGerente = "telefono_gerente",
  telefonoSucursal = "telefono_sucursal",
}

const ModalMatriz = ({
  setSucursalId,
  fetchSucursales,
  sucursalId,
}: WithModalProps) => {
  const [isEdit, setIsEdit] = useState(false);
  const [gerentes, setGerentes] = useState<userType[]>([]);

  const { formField, setFormFields, getLabelProps } = useFormFields({
    id: 0,
    nombre_gerente: "",
    nombre_sucursal: "",
    direccion: "",
    latitud: "",
    longitud: "",
    telefono_gerente: "",
    telefono_sucursal: "",
  });

  const sucursalesApi = useSucursalesApi();
  const usersApi = useUserApi();

  const { setLoading } = UseSystemState();

  const onChangeGerenteSelect = (e: ChangeEvent<HTMLSelectElement>) => {
    const gerenteSelected = gerentes.filter(
      (x) => `${x.nombre} ${x.apellido}` === e.target.value
    )[0];

    setFormFields({
      ...formField,
      nombre_gerente: `${gerenteSelected.nombre} ${gerenteSelected.apellido}`,
      telefono_gerente: gerenteSelected.telefono,
    });
    console.log(gerenteSelected);
  };

  const fetchData = async () => {
    const [sucursalResponse, gerenteResponse] = await Promise.all([
      sucursalesApi.getSucursalesById(sucursalId!),
      usersApi.getUsersByRole(2),
    ]);

    const {
      direccion,
      latitud,
      longitud,
      nombre_gerente,
      nombre_sucursal,
      telefono_gerente,
      telefono_sucursal,
      id,
    } = sucursalResponse;

    setFormFields({
      direccion,
      latitud,
      longitud,
      nombre_gerente,
      nombre_sucursal,
      telefono_gerente,
      telefono_sucursal,
      id: id!,
    });

    setGerentes(gerenteResponse);
  };

  const deleteSucursal = async () => {
    setLoading(true);
    await sucursalesApi.deleteSucursales(sucursalId!);
    setSucursalId(null);
    await fetchSucursales();
    setLoading(false);
  };

  const editSucursal = async () => {
    setLoading(true);
    await sucursalesApi.updateSucursal(formField);
    setSucursalId(null);
    await fetchSucursales();
    setLoading(false);
  };

  useEffect(() => {
    if (sucursalId === null) {
      return;
    }

    setLoading(true);
    fetchData();
    setLoading(false);
  }, [sucursalId]);

  return (
    <IonModal
      isOpen={sucursalId !== null}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setSucursalId(null)}
    >
      <IonHeader className="modal__header">Nueva matriz</IonHeader>
      <IonContent className="ion-padding-horizontal ion-padding-vertical">
        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Id</label>
              <input
                name={LabelNames.id}
                type="text"
                className="form-control"
                {...getLabelProps("id")}
                disabled
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Nombre</label>
              <input
                name={LabelNames.nombreSucursal}
                type="text"
                className="form-control"
                {...getLabelProps("nombre_sucursal")}
                disabled={!isEdit}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Dirección</label>
              <input
                name={LabelNames.direccion}
                type="text"
                className="form-control"
                {...getLabelProps("direccion")}
                disabled={!isEdit}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Teléfono de sucursal</label>
              <input
                name={LabelNames.telefonoSucursal}
                type="text"
                className="form-control"
                {...getLabelProps("telefono_sucursal")}
                disabled={!isEdit}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Latitud</label>
              <input
                name={LabelNames.latitud}
                type="text"
                className="form-control"
                {...getLabelProps("latitud")}
                disabled={!isEdit}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Longitud</label>
              <input
                name={LabelNames.longitud}
                type="text"
                className="form-control"
                {...getLabelProps("id")}
                disabled={!isEdit}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Gerente</label>
              <select
                value={formField.nombre_gerente}
                className="form-control"
                onChange={onChangeGerenteSelect}
                disabled={!isEdit}
              >
                <option value="" disabled>
                  Seleccione gerente
                </option>
                {gerentes.map(({ id, nombre, apellido }) => (
                  <option key={id} value={`${nombre} ${apellido}`}>
                    {nombre} {apellido}
                  </option>
                ))}
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="">Tlf Gerente</label>
              <input
                name={LabelNames.telefonoGerente}
                type="number"
                className="form-control"
                value={formField.telefono_gerente}
                disabled
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setSucursalId(null);
          }}
        >
          Cancelar
        </IonButton>
        {!isEdit ? (
          <IonButton color="bwater" onClick={() => setIsEdit(true)}>
            Editar
          </IonButton>
        ) : (
          <IonButton color="bwater" onClick={() => editSucursal()}>
            Guardar
          </IonButton>
        )}
        <IonButton color="bwater" onClick={() => deleteSucursal()}>
          Eliminar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

export default ModalMatriz;
