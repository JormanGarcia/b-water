const getHeaderTitle = (url: string) => {
  if (url === "/page/dashboard") return "Dashboard";
  if (url === "/page/notifications") return "Notificaciones";
  if (url === "/page/orders") return "Pedidos";

  return "";
};

export default getHeaderTitle;
