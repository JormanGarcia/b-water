export const GetTodayDate = () => {
  const todayDate = new Date();
  let dd: string = todayDate.getDate().toString();
  let mm: string = (todayDate.getMonth() + 1).toString();
  const yyyy = todayDate.getFullYear().toString();

  if (+dd < 10) {
    dd = "0" + dd;
  }

  if (+mm < 10) {
    mm = "0" + mm;
  }

  return `${yyyy}-${mm}-${dd}`;
};

export const RolSelector = (rol: number) => {
  if (rol === 1) return "Director";
  if (rol === 2) return "Gerente de Sucursal";
  if (rol === 3) return "Logistica";
  if (rol === 4) return "Chofer";
  if (rol === 5) return "Cliente";
};

export const StatusSelector = (status: number) => {
  if (status === 1) return "Activo";
  if (status === 2) return "Inactivo";
};
