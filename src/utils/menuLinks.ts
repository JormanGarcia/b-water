import {
  idCardOutline,
  appsOutline,
  receiptOutline,
  navigateCircleOutline,
  fileTrayOutline,
  storefrontOutline,
  personOutline,
  settingsOutline,
  cartOutline,
  peopleOutline,
  waterOutline,
  rocketOutline,
  mapOutline,
} from "ionicons/icons";

const getMenuLinks = (rol: number) => {
  if (rol === 1)
    return [
      {
        title: "Dashboard",
        url: "/page/dashboard",
        url2: "/page/notifications",
        icon: appsOutline,
      },
      {
        title: "Pedidos",
        url: "/page/orders",
        icon: navigateCircleOutline,
      },
      {
        title: "Ventas",
        url: "/page/billing",
        icon: receiptOutline,
      },
      {
        title: "Inventario",
        url: "/page/inventory",
        url2: "/page/history-inventory",
        url3: "/page/vehicle-page",
        url4: "/page/assignment-history",
        url5: "/page/garrafones",
        icon: fileTrayOutline,
      },
      {
        title: "Sucursales",
        url: "/page/matrices",
        icon: storefrontOutline,
      },
      {
        title: "Alta de insumos y productos",
        url: "/page/product-registration",
        icon: cartOutline,
      },
      {
        title: "Egresos varios",
        url: "/page/egresos",
        icon: cartOutline,
      },
      {
        title: "Clientes",
        url: "/page/customers",
        icon: peopleOutline,
      },
      {
        title: "Productos",
        url: "/page/products",
        icon: waterOutline,
      },
      {
        title: "Proveedores",
        url: "/page/vendors",
        icon: rocketOutline,
      },
      {
        title: "Usuarios",
        url: "/page/users",
        icon: personOutline,
      },
      {
        title: "Personal",
        url: "/page/personal",
        url2: "/page/extra-hours",
        url3: "/page/comisiones",
        icon: idCardOutline,
      },
      {
        title: "Ver en mapa",
        url: "/page/map",
        icon: mapOutline,
      },
      {
        title: "Configuración",
        url: "/page/configuracion",
        icon: settingsOutline,
      },
    ];

  if (rol === 2)
    return [
      {
        title: "Dashboard",
        url: "/page/dashboard",
        url2: "/page/notifications",
        icon: appsOutline,
      },
      {
        title: "Pedidos",
        url: "/page/orders",
        icon: navigateCircleOutline,
      },
      {
        title: "Ventas",
        url: "/page/billing",
        icon: receiptOutline,
      },
      {
        title: "Inventario",
        url: "/page/inventory",
        url2: "/page/history-inventory",
        url3: "/page/vehicle-page",
        url4: "/page/assignment-history",
        url5: "/page/garrafones",
        icon: fileTrayOutline,
      },
      {
        title: "Alta de insumos y productos",
        url: "/page/product-registration",
        icon: cartOutline,
      },
      {
        title: "Egresos varios",
        url: "/page/egresos",
        icon: cartOutline,
      },
      {
        title: "Clientes",
        url: "/page/customers",
        icon: peopleOutline,
      },

      {
        title: "Personal",
        url: "/page/personal",
        url2: "/page/extra-hours",
        url3: "/page/comisiones",
        icon: idCardOutline,
      },
      {
        title: "Ver en mapa",
        url: "/page/map",
        icon: mapOutline,
      },
    ];

  if (rol === 3)
    return [
      {
        title: "Logistica",
        // url: "/page/dashboard",
        // url2: "/page/notifications",
        icon: appsOutline,
      },
    ];

  if (rol === 4)
    return [
      {
        title: "Dashboard",
        url: "/page/dashboard",
        url2: "/page/notifications",
        icon: appsOutline,
      },
      {
        title: "Pedidos",
        url: "/page/orders",
        icon: navigateCircleOutline,
      },
      {
        title: "Ventas",
        url: "/page/billing",
        icon: receiptOutline,
      },
      {
        title: "Inventario",
        url: "/page/inventory",
        url2: "/page/history-inventory",
        url3: "/page/vehicle-page",
        url4: "/page/assignment-history",
        url5: "/page/garrafones",
        icon: fileTrayOutline,
      },
      {
        title: "Sucursales",
        url: "/page/matrices",
        icon: storefrontOutline,
      },
      {
        title: "Alta de insumos y productos",
        url: "/page/product-registration",
        icon: cartOutline,
      },
      {
        title: "Egresos varios",
        url: "/page/egresos",
        icon: cartOutline,
      },
      {
        title: "Clientes",
        url: "/page/customers",
        icon: peopleOutline,
      },
      {
        title: "Productos",
        url: "/page/products",
        icon: waterOutline,
      },
      {
        title: "Proveedores",
        url: "/page/vendors",
        icon: rocketOutline,
      },
      {
        title: "Usuarios",
        url: "/page/users",
        icon: personOutline,
      },
      {
        title: "Personal",
        url: "/page/personal",
        url2: "/page/extra-hours",
        url3: "/page/comisiones",
        icon: idCardOutline,
      },
      {
        title: "Ver en mapa",
        url: "/page/map",
        icon: mapOutline,
      },
      {
        title: "Configuración",
        url: "/page/configuracion",
        icon: settingsOutline,
      },
    ];

  if (rol === 5)
    return [
      {
        title: "Dashboard",
        url: "/page/dashboard",
        url2: "/page/notifications",
        icon: appsOutline,
      },
      {
        title: "Pedidos",
        url: "/page/orders",
        icon: navigateCircleOutline,
      },
      {
        title: "Ventas",
        url: "/page/billing",
        icon: receiptOutline,
      },
      {
        title: "Inventario",
        url: "/page/inventory",
        url2: "/page/history-inventory",
        url3: "/page/vehicle-page",
        url4: "/page/assignment-history",
        url5: "/page/garrafones",
        icon: fileTrayOutline,
      },
      {
        title: "Sucursales",
        url: "/page/matrices",
        icon: storefrontOutline,
      },
      {
        title: "Alta de insumos y productos",
        url: "/page/product-registration",
        icon: cartOutline,
      },
      {
        title: "Egresos varios",
        url: "/page/egresos",
        icon: cartOutline,
      },
      {
        title: "Clientes",
        url: "/page/customers",
        icon: peopleOutline,
      },
      {
        title: "Productos",
        url: "/page/products",
        icon: waterOutline,
      },
      {
        title: "Proveedores",
        url: "/page/vendors",
        icon: rocketOutline,
      },
      {
        title: "Usuarios",
        url: "/page/users",
        icon: personOutline,
      },
      {
        title: "Personal",
        url: "/page/personal",
        url2: "/page/extra-hours",
        url3: "/page/comisiones",
        icon: idCardOutline,
      },
      {
        title: "Ver en mapa",
        url: "/page/map",
        icon: mapOutline,
      },
      {
        title: "Configuración",
        url: "/page/configuracion",
        icon: settingsOutline,
      },
    ];
};

export default getMenuLinks;
