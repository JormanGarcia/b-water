import UseAuth from "../hooks/useAuth";
import { useAppDispatch } from "../store/";
import { setToken } from "../store/authenticationSlice";

/*
const TOKEN_KEY = 'AuthToken';

export class TokenStorage {
    
    
    static saveToken(token: string): Promise<any> {
        const dispatch = useDispatch(setToken())
        return new Promise(Resolve => {
            window.localStorage.removeItem(TOKEN_KEY);
            window.localStorage.setItem(TOKEN_KEY, token);
            Resolve();
        })

    }

    static getToken(): string {
        return localStorage.getItem(TOKEN_KEY);
    }
}
*/

export const UseTokenStorage = () => {
  const dispatch = useAppDispatch();
  const { token } = UseAuth();

  const saveToken = async (Token: string) => {
    dispatch(setToken(Token));
    Promise.resolve();
  };

  const getToken = () => {
    return token;
  };

  return {
    saveToken,
    getToken,
  };
};
