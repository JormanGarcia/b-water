import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonCard,
  IonCardContent,
  IonBadge,
  IonButton,
} from "@ionic/react";

import { useState } from "react";

import Sucursales from "../components/Sucursales";
import OrdersTable from "../components/OrdersTable";
import Notifications from "../components/Notifications";
import VehicleAssignment from "../components/VehicleAssignment";
import UseAuth from "../hooks/useAuth";
import Header from "../components/Header";

const Dashboard = () => {
  const [viewMap, setViewMap] = useState(false);
  const [vehicleAssignment, setVehicleAssignment] = useState(false);
  const { user } = UseAuth();

  return (
    <IonPage>
      <VehicleAssignment
        showModal={vehicleAssignment}
        setShowModal={setVehicleAssignment}
      />
      <Header title="Dashboard" />
      <IonContent fullscreen className="bg-light-bwater">
        <IonGrid>
          <IonRow>
            <IonCol size="12" size-lg="9">
              <Sucursales setViewMap={setViewMap} viewMap={viewMap} />
              {viewMap ? (
                <iframe
                  title="Map"
                  src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d6300515.913136642!2d-105.782067!3d39.550051!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2sve!4v1620150614538!5m2!1ses!2sve"
                  style={{
                    width: "100%",
                    height: "700px",
                    border: "3px solid #001871",
                    marginTop: "1.5rem",
                    boxShadow:
                      "0px 0px 30px rgba(0,0,0,.15), 0px 5px 10px rgba(0,0,0,0.05)",
                  }}
                ></iframe>
              ) : (
                <>
                  <OrdersTable />
                </>
              )}
            </IonCol>
            <IonCol size="12" size-lg="3">
              <Notifications />
              {viewMap ? (
                <IonCard className="card-notifications">
                  <IonCardContent>
                    <h2 className="card-custom__title pl-0 bwater-color">
                      Pedidos actuales
                    </h2>
                    <table className="table">
                      <thead>
                        <tr className="ion-text-left">
                          <th>ID pedido</th>
                          <th>ID cliente</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>N-12</td>
                          <td>N-12345ABC</td>
                        </tr>
                        <tr>
                          <td>N-12</td>
                          <td>N-12345ABC</td>
                        </tr>
                        <tr>
                          <td>N-12</td>
                          <td>N-12345ABC</td>
                        </tr>
                        <tr>
                          <td>N-12</td>
                          <td>N-12345ABC</td>
                        </tr>
                        <tr>
                          <td>N-12</td>
                          <td>N-12345ABC</td>
                        </tr>
                        <tr>
                          <td>N-12</td>
                          <td>N-12345ABC</td>
                        </tr>
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              ) : (
                <>
                  <IonCard className="card-notifications">
                    <IonCardContent>
                      <h2 className="card-custom__title pl-0 bwater-color">
                        Resumen por Estatus
                      </h2>
                      <table className="table">
                        <thead>
                          <tr>
                            <th> Estatus del envío</th>
                            <th>Cantidad</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <IonBadge color="light">En proceso</IonBadge>
                            </td>
                            <td>89</td>
                          </tr>
                          <tr>
                            <td>
                              <IonBadge color="success">Entregado</IonBadge>
                            </td>
                            <td>87</td>
                          </tr>
                          <tr>
                            <td>
                              <IonBadge color="danger">Regresado</IonBadge>
                            </td>
                            <td>88</td>
                          </tr>
                          <tr>
                            <td>
                              <IonBadge color="secondary">Rezagados</IonBadge>
                            </td>
                            <td>45</td>
                          </tr>
                        </tbody>
                      </table>
                    </IonCardContent>
                  </IonCard>
                  <IonCard className="card-notifications">
                    <IonCardContent>
                      <h2 className="card-custom__title pl-0 bwater-color">
                        Resumen por pago
                      </h2>
                      <table className="table">
                        <thead>
                          <tr>
                            <th> Estatus de pago </th>
                            <th>Cantidad</th>
                            <th>Monto</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <IonBadge color="light">En proceso</IonBadge>
                            </td>
                            <td>89</td>
                            <td>44</td>
                          </tr>
                          <tr>
                            <td>
                              <IonBadge color="success">Entregado</IonBadge>
                            </td>
                            <td>56</td>
                            <td>45</td>
                          </tr>
                          <tr>
                            <td>
                              <IonBadge color="danger">Regresado</IonBadge>
                            </td>
                            <td>87</td>
                            <td>44</td>
                          </tr>

                          <tr>
                            <td>
                              <IonBadge color="secondary">
                                En verificacion
                              </IonBadge>
                            </td>
                            <td>88</td>
                            <td>44</td>
                          </tr>
                        </tbody>
                      </table>
                    </IonCardContent>
                  </IonCard>

                  <IonCard className="card-notifications">
                    <IonCardContent>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "space-between",
                        }}
                      >
                        <h2
                          className="card-custom__title pl-0 bwater-color"
                          style={{ marginRight: 5 }}
                        >
                          Vehiculos
                        </h2>
                        <select
                          style={{ width: "auto", flexShrink: 1 }}
                          className="form-control"
                          value=""
                        >
                          <option value="" disabled>
                            Sucursales
                          </option>
                        </select>
                      </div>
                      <table className="table">
                        <thead>
                          <tr>
                            <th>Chofer</th>
                            <th>Vehiculo</th>
                            <th>Tipo</th>
                          </tr>
                        </thead>
                        <tbody>
                          {[1, 2, 3, 4].map(() => (
                            <tr>
                              <td>Carlos</td>
                              <td>Ford Pickup</td>
                              <td>Camioneta</td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                      {user?.rol === 1 && (
                        <IonButton
                          expand="block"
                          color="bwater"
                          onClick={() => setVehicleAssignment(true)}
                        >
                          Asignar Vehiculos - Hoy
                        </IonButton>
                      )}
                    </IonCardContent>
                  </IonCard>

                  <IonCard className="card-notifications">
                    <IonCardContent>
                      <h2 className="card-custom__title pl-0 bwater-color">
                        Resumen por tipo de pago - Hoy
                      </h2>
                      <table className="table">
                        <thead>
                          <tr>
                            <th>Tipo de Pago</th>
                            <th>Monto</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Efectivo</td>
                            <td>89</td>
                          </tr>
                          <tr>
                            <td>Transferencia</td>
                            <td>56</td>
                          </tr>
                          <tr>
                            <td>Tarjeta</td>
                            <td>87</td>
                          </tr>

                          <tr>
                            <td>En Verificacion</td>
                            <td>88</td>
                          </tr>
                        </tbody>
                      </table>
                    </IonCardContent>
                  </IonCard>
                </>
              )}
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Dashboard;
