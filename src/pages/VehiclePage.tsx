import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonIcon,
  IonModal,
  IonHeader,
  IonFooter,
} from "@ionic/react";
import { searchOutline } from "ionicons/icons";

import Header from "../components/Header";

import { useState } from "react";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

const ModalAddVehicle = ({ showModal, setShowModal }: WithModalProps) => {
  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Agregar vehículo</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">ID</label>
              <input
                type="text"
                className="form-control"
                placeholder="Generado automaticamente"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de registro</label>
              <input
                type="text"
                className="form-control"
                placeholder="Generado automaticamente"
              />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Matrícula</label>
              <input type="text" className="form-control" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Marca</label>
              <input type="text" className="form-control" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Modelo</label>
              <input type="text" className="form-control" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Año</label>
              <input type="text" className="form-control" />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Tipo</label>
              <select className="form-control" defaultValue="def">
                <option value="def" disabled>
                  Seleccione Tipo
                </option>
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="">Estatus</label>
              <select className="form-control" defaultValue="def">
                <option value="def" disabled>
                  Seleccione Estatus
                </option>
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="">Sucursal asignada</label>
              <select className="form-control" defaultValue="def">
                <option value="def" disabled>
                  Seleccione Sucursal
                </option>
              </select>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton color="bwater">Guardar</IonButton>
      </IonFooter>
    </IonModal>
  );
};

const ModalVehicle = ({ showModal, setShowModal }: WithModalProps) => {
  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Detalles del vehículo</IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">ID</label>
              <input
                type="text"
                className="form-control"
                value="valor"
                placeholder="ID"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="">Fecha de registro</label>
              <input
                type="text"
                className="form-control"
                value="valor"
                placeholder="Generado automaticamente"
              />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Matrícula</label>
              <input type="text" value="valor" className="form-control" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Marca</label>
              <input type="text" value="valor" className="form-control" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Modelo</label>
              <input type="text" value="valor" className="form-control" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Año</label>
              <input type="text" value="valor" className="form-control" />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Tipo</label>
              <select className="form-control" defaultValue="def">
                <option value="def" disabled>
                  Seleccione Tipo
                </option>
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="">Estatus</label>
              <select className="form-control" defaultValue="def">
                <option value="def" disabled>
                  Seleccione Estatus
                </option>
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="">Sucursal asignada</label>
              <select className="form-control" defaultValue="def">
                <option value="def" disabled>
                  Seleccione Sucursal
                </option>
              </select>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cerrar
        </IonButton>
        <IonButton color="bwater" style={{ marginRight: "1rem" }}>
          Editar
        </IonButton>
        <IonButton color="bwater">Eliminar</IonButton>
      </IonFooter>
    </IonModal>
  );
};

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

const VehiclePage = () => {
  const [showModalAddVehicle, setShowModalAddVehicle] = useState(false);
  const [showModalVehicle, setShowModalVehicle] = useState(false);

  const handleTableClick = (e: any) => {
    if (e.target.tagName === "TD") {
      setShowModalVehicle(true);
    }
  };

  return (
    <>
      <ModalAddVehicle
        showModal={showModalAddVehicle}
        setShowModal={setShowModalAddVehicle}
      />
      <ModalVehicle
        showModal={showModalVehicle}
        setShowModal={setShowModalVehicle}
      />
      <IonPage>
        <Header title="Vehículos" />
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                <IonButton color="bwater" routerLink="/page/inventory">
                  Regresar al inventario
                </IonButton>
                <IonButton
                  color="bwater"
                  onClick={() => {
                    setShowModalAddVehicle(true);
                  }}
                >
                  Agregar vehículo
                </IonButton>
                <IonButton color="bwater" routerLink="/page/assignment-history">
                  Historial de Asignacion
                </IonButton>
                <IonCard className="m-0">
                  <IonCardHeader className="table-header">
                    <h2 className="table-title">Listado de vehículos</h2>
                    <div style={{ display: "flex" }}>
                      <div className="search" style={{ marginRight: ".5rem" }}>
                        <button>
                          <IonIcon md={searchOutline} />
                        </button>
                        <input
                          type="text"
                          className="search__input"
                          placeholder="Buscar"
                        />
                      </div>
                    </div>
                  </IonCardHeader>
                  <IonCardContent>
                    <table className="table table-with-info">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>ID Vehículo</th>
                          <th>Tipo Vehículo</th>
                          <th>Conductor Asignado</th>
                          <th>Sucursal</th>
                        </tr>
                      </thead>
                      <tbody onClick={handleTableClick}>
                        {numbers.map((item) => (
                          <tr>
                            <td>1</td>
                            <td>14/02/21</td>
                            <td>1</td>
                            <td>1</td>
                            <td>100</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default VehiclePage;
