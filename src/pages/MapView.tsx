import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
} from "@ionic/react";

import Header from "../components/Header";

const ProductsPage = () => {
  return (
    <IonPage>
      <Header title="Vistas en Mapa" />
      <IonContent fullscreen className="bg-light-bwater">
        <IonGrid>
          <IonRow>
            <IonCol size="12">
              <div style={{ marginTop: "1rem" }}>
                <IonButton color="bwater">Ver clientes</IonButton>
                <IonButton color="bwater">Ver sucursales</IonButton>
                <IonButton color="bwater">Ver pedidos</IonButton>
                <IonButton color="bwater">Ver choferes</IonButton>
              </div>
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d6300515.913136642!2d-105.782067!3d39.550051!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2sve!4v1620150614538!5m2!1ses!2sve"
                title="Maps"
                style={{
                  width: "100%",
                  height: "700px",
                  border: "3px solid #001871",
                  marginTop: "1.5rem",
                  boxShadow:
                    "0px 0px 20px rgba(0,0,0,.15), 0px 5px 10px rgba(0,0,0,0.1)",
                }}
              ></iframe>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default ProductsPage;
