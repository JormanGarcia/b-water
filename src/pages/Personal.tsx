import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonIcon,
} from "@ionic/react";
import AddPersonal from "../components/AddPersonal";
import { searchOutline } from "ionicons/icons";

import Header from "../components/Header";

import { useState } from "react";
import PersonalDetails from "../components/PersonalDetails";

interface Personal {
  ID: number;
  fechaIngreso: string;
  fechaRegistro: string;
  nombre: string;
  apellido: string;
  cargo: string;
  sucursal: string;
  salario: number;
  telefono: number;
  correo: string;
  direccion: string;
}

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
const TEMPORALPERSONAL: Personal = {
  ID: 123,
  fechaIngreso: "22/5/21",
  fechaRegistro: "22/5/21",
  nombre: "Jorman",
  apellido: "Garcia",
  cargo: "Lorem Ipsum",
  sucursal: "Lorem Ipsum",
  salario: 500,
  telefono: 4246420487,
  correo: "Jorman@email.com",
  direccion: "Alfredo Sadel",
};

const PersonalPage = () => {
  const [AddPersonalModal, setAddPersonalModal] = useState(false);
  const [personalDetails, setPersonalDetails] = useState<Personal | null>(null);

  return (
    <>
      <AddPersonal
        showModal={AddPersonalModal}
        setShowModal={setAddPersonalModal}
      />

      <PersonalDetails
        personal={personalDetails}
        setPersonal={setPersonalDetails}
      />

      <IonPage>
        <Header title="Personal" />
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                <IonButton
                  color="bwater"
                  onClick={() => {
                    setAddPersonalModal(true);
                  }}
                >
                  Agregar Personal
                </IonButton>
                <IonButton color="bwater" routerLink="/page/extra-hours">
                  Gestión de Horas Extras
                </IonButton>
                <IonButton color="bwater" routerLink="/page/comisiones">
                  Gestión de Comisiones
                </IonButton>
                <IonCard className="m-0">
                  <IonCardHeader className="table-header">
                    <h2 className="table-title">Listado de Personal</h2>
                    <div style={{ display: "flex" }}>
                      <div className="search" style={{ marginRight: ".5rem" }}>
                        <button>
                          <IonIcon md={searchOutline} />
                        </button>
                        <input
                          type="text"
                          className="search__input"
                          placeholder="Buscar"
                        />
                      </div>
                    </div>
                  </IonCardHeader>
                  <IonCardContent>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nombres</th>
                          <th>Apellidos</th>
                          <th>Cargo</th>
                          <th>Sucursal</th>
                          <th>Fecha De Ingreso</th>
                          <th>Salario</th>
                        </tr>
                      </thead>
                      <tbody>
                        {numbers.map((item) => (
                          <tr
                            onClick={() => {
                              setPersonalDetails(TEMPORALPERSONAL);
                            }}
                          >
                            <td>1</td>
                            <td>14/02/21</td>
                            <td>1</td>
                            <td>1</td>
                            <td>100</td>
                            <td>100</td>
                            <td>100</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default PersonalPage;
