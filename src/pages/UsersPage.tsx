import "firebase/auth";
import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
} from "@ionic/react";
import Header from "../components/Header";
import { useEffect, useMemo, useState } from "react";
import ModalAddUser from "../components/ModalAddUser";
import ModalUser from "../components/ModalUser";
import { useUserApi } from "../api/useUserApi";

import { userType } from "../models/userType.interface";

import { RolSelector, StatusSelector } from "../utils";
import UseSystemState from "../hooks/useSystemState";
import DataTable from "../components/DataTable";

const UsersPage = () => {
  const [showModalAddUser, setShowModalAddUser] = useState(false);
  const [usuarios, setUsuarios] = useState<userType[]>([]);
  const [userForModal, setUserForModal] = useState<number | null>(null);
  const userApi = useUserApi();

  const { setLoading } = UseSystemState();

  const fetchUsers = async () => {
    const users = await userApi.getUsers();
    setUsuarios(
      users.map((user) => {
        return {
          ...user,
          rolConverted: RolSelector(user?.rol!),
          statusConverted: StatusSelector(user?.status!),
        };
      })
    );
  };

  const TableColumns = useMemo(
    () => [
      {
        Header: "ID Usuario",
        accessor: "id",
      },
      {
        Header: "Nombre",
        accessor: "nombre",
      },
      {
        Header: "Apellido",
        accessor: "apellido",
      },
      {
        Header: "Email",
        accessor: "email",
      },
      {
        Header: "Rol",
        accessor: "rolConverted",
      },
      {
        Header: "Sucursal",
        accessor: "nombre_sucursal",
      },
      {
        Header: "Estatus",
        accessor: "statusConverted",
      },
    ],
    []
  );

  useEffect(() => {
    (async () => {
      setLoading(true);
      await fetchUsers();
      setLoading(false);
    })();
  }, []);

  return (
    <>
      <ModalAddUser
        showModal={showModalAddUser}
        setShowModal={setShowModalAddUser}
        fetchData={fetchUsers}
      />
      <ModalUser
        fetchUsers={fetchUsers}
        user={userForModal}
        setUser={setUserForModal}
      />
      <IonPage>
        <Header title="Usuarios" />
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                <IonButton
                  color="bwater"
                  onClick={() => {
                    setShowModalAddUser(true);
                  }}
                >
                  Nuevo usuario
                </IonButton>
                <DataTable
                  data={usuarios}
                  columns={TableColumns}
                  title="Listado de usuarios"
                  onRowClick={(x) => setUserForModal(x.id)}
                />
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default UsersPage;
