import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
} from "@ionic/react";

import { useEffect, useMemo, useState } from "react";

import Header from "../components/Header";
import { sucursalesType } from "../models/sucuarsalesType.interface";
import { useSucursalesApi } from "../api/useSucursalesApi";

import ModalNewMatriz from "../components/ModalNewMatriz";
import ModalMatriz from "../components/ModalMatriz";
import UseSystemState from "../hooks/useSystemState";
import DataTable from "../components/DataTable";

const MatricesPage = () => {
  const [showModalNM, setShowModalNM] = useState(false);
  const [sucursalId, setSucursalId] = useState<number | null>(null);
  const [sucursales, setSucursales] = useState<sucursalesType[]>([]);

  const { setLoading } = UseSystemState();
  const sucursalesApi = useSucursalesApi();

  const colums = useMemo<{ Header: string; accessor: keyof sucursalesType }[]>(
    () => [
      {
        Header: "ID Matriz",
        accessor: "id",
      },
      {
        Header: "Nombre",
        accessor: "nombre_sucursal",
      },
      {
        Header: "Direccion",
        accessor: "direccion",
      },
      {
        Header: "Telefono",
        accessor: "telefono_sucursal",
      },
      {
        Header: "Gerente",
        accessor: "nombre_gerente",
      },
      {
        Header: "Telefono Gerente",
        accessor: "telefono_gerente",
      },
    ],
    []
  );

  const fetchData = async () => {
    const response = await sucursalesApi.getSucursales();
    setSucursales(response);
  };
  useEffect(() => {
    setLoading(true);
    (async () => {
      await fetchData();
    })();
    setLoading(false);
  }, []);

  return (
    <>
      <ModalNewMatriz
        showModal={showModalNM}
        setShowModal={setShowModalNM}
        fetchSucursales={fetchData}
      />
      <ModalMatriz
        fetchSucursales={fetchData}
        setSucursalId={setSucursalId}
        sucursalId={sucursalId}
      />
      <IonPage>
        <Header title="Sucursales" />
        <IonContent className="bg-light-bwater">
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                <IonButton
                  color="bwater"
                  onClick={() => {
                    setShowModalNM(true);
                  }}
                >
                  Nueva sucursal
                </IonButton>
                <DataTable
                  columns={colums}
                  data={sucursales}
                  title="Listado de Sucursales"
                  onRowClick={(x) => {
                    setSucursalId(x.id);
                  }}
                />
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default MatricesPage;
