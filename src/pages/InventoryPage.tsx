import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonIcon,
  IonModal,
  IonHeader,
  IonFooter,
} from "@ionic/react";
import { searchOutline } from "ionicons/icons";

import Header from "../components/Header";

import { useState } from "react";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => void;
}

const ModalInventoryOptions = ({ showModal, setShowModal }: WithModalProps) => (
  <IonModal
    isOpen={showModal}
    cssClass="modal"
    swipeToClose={true}
    onDidDismiss={() => setShowModal(false)}
  >
    <IonHeader className="modal__header">Operaciones de inventario</IonHeader>
    <IonContent className="ion-padding-horizontal ion-padding-vertical">
      <IonGrid className="grid-forms">
        <IonRow>
          <IonCol>
            <label htmlFor="">Producto:</label>
            <select className="form-control" defaultValue="def">
              <option value="def" disabled>
                Seleccione producto
              </option>
              <option value="op-1">Producto 1</option>
              <option value="op-2">Producto 2</option>
              <option value="op-3">Producto 3</option>
              <option value="op-4">Producto 4</option>
            </select>
          </IonCol>
          <IonCol>
            <label htmlFor="">Sucursal:</label>
            <select className="form-control" defaultValue="def">
              <option value="def" disabled>
                Seleccione sucursal
              </option>
              <option value="op-1">Sucursal 1</option>
              <option value="op-2">Sucursal 2</option>
              <option value="op-3">Sucursal 3</option>
              <option value="op-4">Sucursal 4</option>
            </select>
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol>
            <label htmlFor="">Nombre:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
          <IonCol>
            <label htmlFor="">Descripción:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
          <IonCol>
            <label htmlFor="">Ubicación:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
          <IonCol>
            <label htmlFor="">Costo:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
          <IonCol>
            <label htmlFor="">Precio venta:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol>
            <label htmlFor="">Existencia actual:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
          <IonCol>
            <label htmlFor="">Operación:</label>
            <div className="operation-wrapper">
              <input type="text" id="" className="form-control" />
              <button color="bwater">+</button>
              <button color="bwater">-</button>
            </div>
          </IonCol>
          <IonCol>
            <label htmlFor="">Nueva existencia:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
          <IonCol>
            <label htmlFor="">Fecha operación:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol>
            <label htmlFor="">Motivos de la operación:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
          <IonCol>
            <label htmlFor="">Solicitado por:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
        </IonRow>
      </IonGrid>
    </IonContent>
    <IonFooter className="modal__footer">
      <IonButton
        color="bwater"
        style={{ marginRight: "1rem" }}
        onClick={() => {
          setShowModal(false);
        }}
      >
        Cancelar
      </IonButton>
      <IonButton color="bwater">Seleccionar</IonButton>
    </IonFooter>
  </IonModal>
);

const InventoryPage = () => {
  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <ModalInventoryOptions
        showModal={showModal}
        setShowModal={setShowModal}
      />
      <IonPage>
        <Header title="Inventario de productos e insumos" />
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                <IonButton
                  color="bwater"
                  onClick={() => {
                    setShowModal(true);
                  }}
                >
                  Operaciones de inventario
                </IonButton>
                <IonButton color="bwater" routerLink="/page/history-inventory">
                  Historial operaciones de inventario
                </IonButton>
                <IonButton color="bwater" routerLink="/page/vehicle-page">
                  Vehículos
                </IonButton>
                <IonButton color="bwater" routerLink="/page/garrafones">
                  Garrafones de Canje
                </IonButton>
                <IonCard className="m-0">
                  <IonCardHeader className="table-header">
                    <h2 className="table-title">
                      Inventario de productos e insumos
                    </h2>
                    <div style={{ display: "flex" }}>
                      <div className="search" style={{ marginRight: ".5rem" }}>
                        <button>
                          <IonIcon md={searchOutline} />
                        </button>
                        <input
                          type="text"
                          className="search__input"
                          placeholder="Buscar"
                        />
                      </div>
                      <div className="table-header__form">
                        <select defaultValue="def" className="form-control">
                          <option value="def" disabled>
                            Sucursal
                          </option>
                          <option value="s1">Sucursal 1</option>
                          <option value="s2">Sucursal 2</option>
                        </select>
                        <select defaultValue="def" className="form-control">
                          <option value="def" disabled>
                            Producto
                          </option>
                          <option value="s1">Producto 1</option>
                          <option value="s2">Producto 2</option>
                        </select>
                      </div>
                    </div>
                  </IonCardHeader>
                  <IonCardContent>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>ID producto</th>
                          <th>Nombre</th>
                          <th>Descripción</th>
                          <th>U. medida</th>
                          <th>Ubicación</th>
                          <th>Costo (mxn)</th>
                          <th>Precio de venta</th>
                          <th>Existencia</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                        </tr>
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default InventoryPage;
