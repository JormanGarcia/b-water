import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonIcon,
} from "@ionic/react";
import { searchOutline } from "ionicons/icons";

import Header from "../components/Header";

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

const AssignmentHistory = () => {
  return (
    <>
      <IonPage>
        <Header title="Vehículos" />
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                <IonButton color="bwater" routerLink="/page/inventory">
                  Regresar al inventario
                </IonButton>

                <IonButton color="bwater" routerLink="/page/vehicle-page">
                  Regresar a Vehículos
                </IonButton>
                <IonCard className="m-0">
                  <IonCardHeader className="table-header">
                    <h2 className="table-title">Historial de Asignación</h2>
                    <div style={{ display: "flex" }}>
                      <div className="search" style={{ marginRight: ".5rem" }}>
                        <button>
                          <IonIcon md={searchOutline} />
                        </button>
                        <input
                          type="text"
                          className="search__input"
                          placeholder="Buscar"
                        />
                      </div>
                    </div>
                  </IonCardHeader>
                  <IonCardContent>
                    <table className="table table-with-info">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>ID Vehiculo</th>
                          <th>Tipo Vehículo</th>
                          <th>Conductor Asignado</th>
                          <th>Sucursal</th>
                        </tr>
                      </thead>
                      <tbody>
                        {numbers.map((item) => (
                          <tr>
                            <td>1</td>
                            <td>14/02/21</td>
                            <td>1</td>
                            <td>1</td>
                            <td>100</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default AssignmentHistory;
