import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonIcon,
  IonModal,
  IonHeader,
  IonFooter,
} from "@ionic/react";
import { searchOutline, addOutline } from "ionicons/icons";

import Header from "../components/Header";

import { useState } from "react";

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => any;
}

const ModalAltaProducto = ({ showModal, setShowModal }: WithModalProps) => (
  <IonModal
    isOpen={showModal}
    cssClass="modal"
    swipeToClose={true}
    onDidDismiss={() => setShowModal(false)}
  >
    <IonHeader className="modal__header">Alta de productos e insumos</IonHeader>
    <IonContent className="ion-padding-horizontal ion-padding-vertical">
      <IonGrid className="grid-forms">
        <IonRow>
          <IonCol>
            <label htmlFor="">Nro. factura:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
          <IonCol>
            <label htmlFor="">Fecha de la factura:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
          <IonCol>
            <label htmlFor="">Fecha del registro:</label>
            <input type="text" id="" className="form-control" />
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol>
            <label htmlFor="">Proveedor:</label>
            <select className="form-control" defaultValue="def">
              <option value="def" disabled>
                Seleccione proveedor
              </option>
            </select>
          </IonCol>
          <IonCol>
            <label htmlFor="">Descripción:</label>
            <input type="text" className="form-control" />
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol>
            <label htmlFor="">Sucursal asignada:</label>
            <select className="form-control" defaultValue="def">
              <option value="def" disabled>
                Seleccione sucursal
              </option>
            </select>
          </IonCol>
        </IonRow>
      </IonGrid>

      <IonGrid>
        <IonRow>
          <IonCol>
            <select className="form-control" defaultValue="def">
              <option value="def" disabled>
                Seleccione producto
              </option>
              <option value="op-1">Producto 1</option>
              <option value="op-2">Producto 2</option>
              <option value="op-3">Producto 3</option>
              <option value="op-4">Producto 4</option>
            </select>
          </IonCol>
          <IonCol>
            <input
              type="number"
              min="0"
              className="form-control"
              placeholder="Costo / Empa."
            />
          </IonCol>
          <IonCol>
            <input
              type="number"
              min="0"
              className="form-control"
              placeholder="Cantidad / Empa."
            />
          </IonCol>
          <IonCol>
            <input
              type="number"
              min="0"
              className="form-control"
              placeholder="Total"
            />
          </IonCol>
        </IonRow>
        <IonRow className="footer-form">
          <IonCol size="4">
            <IonButton color="bwater" className="add-button">
              <IonIcon md={addOutline} ios={addOutline} />
            </IonButton>
          </IonCol>
          <IonCol>
            Total: <span>100</span>
          </IonCol>
        </IonRow>
      </IonGrid>
    </IonContent>
    <IonFooter className="modal__footer">
      <IonButton
        color="bwater"
        style={{ marginRight: "1rem" }}
        onClick={() => {
          setShowModal(false);
        }}
      >
        Cancelar
      </IonButton>
      <IonButton color="bwater">Guardar</IonButton>
    </IonFooter>
  </IonModal>
);

const ModalProducto = ({ showModal, setShowModal }: WithModalProps) => (
  <IonModal
    isOpen={showModal}
    cssClass="modal"
    swipeToClose={true}
    onDidDismiss={() => setShowModal(false)}
  >
    <IonHeader className="modal__header">Alta de productos e insumos</IonHeader>
    <IonContent className="ion-padding-horizontal ion-padding-vertical">
      <IonGrid className="grid-forms">
        <IonRow>
          <IonCol>
            <label htmlFor="">Nro. factura:</label>
            <input type="text" value="valor" id="" className="form-control" />
          </IonCol>
          <IonCol>
            <label htmlFor="">Fecha de la factura:</label>
            <input type="text" value="valor" id="" className="form-control" />
          </IonCol>
          <IonCol>
            <label htmlFor="">Fecha del registro:</label>
            <input type="text" value="valor" id="" className="form-control" />
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol>
            <label htmlFor="">Proveedor:</label>
            <select className="form-control" defaultValue="def">
              <option value="def" disabled>
                Seleccione proveedor
              </option>
            </select>
          </IonCol>
          <IonCol>
            <label htmlFor="">Descripción:</label>
            <input type="text" value="valor" className="form-control" />
          </IonCol>
        </IonRow>

        <IonRow>
          <IonCol>
            <label htmlFor="">Sucursal asignada:</label>
            <select className="form-control" defaultValue="def">
              <option value="def" disabled>
                Seleccione sucursal
              </option>
            </select>
          </IonCol>
        </IonRow>

        <table className="table">
          <thead>
            <tr>
              <th>Producto o insumos</th>
              <th>Costo / Empa.</th>
              <th>Cantidad Emp.</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Garrafon nuevo</td>
              <td>1</td>
              <td>54</td>
              <td>77</td>
            </tr>
            <tr>
              <td>Garrafon nuevo</td>
              <td>1</td>
              <td>54</td>
              <td>77</td>
            </tr>
            <tr>
              <td>Garrafon nuevo</td>
              <td>1</td>
              <td>54</td>
              <td>77</td>
            </tr>
            <tr>
              <td>Garrafon nuevo</td>
              <td>1</td>
              <td>54</td>
              <td>77</td>
            </tr>
            <tr className="table-bottom">
              <td></td>
              <td></td>
              <td>Total</td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </IonGrid>
    </IonContent>
    <IonFooter className="modal__footer">
      <IonButton
        color="bwater"
        style={{ marginRight: "1rem" }}
        onClick={() => {
          setShowModal(false);
        }}
      >
        Cancelar
      </IonButton>
      <IonButton color="bwater">Guardar</IonButton>
    </IonFooter>
  </IonModal>
);

const ProductsRegistrationPage = () => {
  const [showModal, setShowModal] = useState(false);
  const [showModalP, setShowModalP] = useState(false);

  return (
    <>
      <ModalAltaProducto showModal={showModal} setShowModal={setShowModal} />
      <ModalProducto showModal={showModalP} setShowModal={setShowModalP} />
      <IonPage>
        <Header title="Alta de insumos y productos" />
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                <IonButton
                  color="bwater"
                  onClick={() => {
                    setShowModal(true);
                  }}
                >
                  Dar de alta productos e insumos
                </IonButton>
                <IonCard className="m-0">
                  <IonCardHeader className="table-header">
                    <h2 className="table-title">Alta de productos e insumos</h2>
                    <div style={{ display: "flex" }}>
                      <div className="search" style={{ marginRight: ".5rem" }}>
                        <button>
                          <IonIcon md={searchOutline} />
                        </button>
                        <input
                          type="text"
                          className="search__input"
                          placeholder="Buscar"
                        />
                      </div>
                      <div className="table-header__form">
                        <select defaultValue="def" className="form-control">
                          <option value="def" disabled>
                            Proveedor
                          </option>
                          <option value="s1">Proveedor 1</option>
                          <option value="s2">Proveedor 2</option>
                        </select>
                        <select defaultValue="def" className="form-control">
                          <option value="def" disabled>
                            Sucursal
                          </option>
                          <option value="s1">Sucursal 1</option>
                          <option value="s2">Sucursal 2</option>
                        </select>
                      </div>
                    </div>
                  </IonCardHeader>
                  <IonCardContent>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Nro. Factura</th>
                          <th>Preoveedor</th>
                          <th>Descripción</th>
                          <th>Sucursal asignada</th>
                          <th>Monto de factura</th>
                        </tr>
                      </thead>
                      <tbody>
                        {numbers.map((number) => (
                          <tr
                            onClick={() => {
                              setShowModalP(true);
                            }}
                          >
                            <td>1</td>
                            <td>14/02/21</td>
                            <td>1</td>
                            <td>1</td>
                            <td>100</td>
                            <td>100</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default ProductsRegistrationPage;
