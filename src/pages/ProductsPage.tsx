import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonCard,
  IonCardHeader,
  IonIcon,
  IonCardContent,
  IonButton,
  IonModal,
  IonHeader,
  IonFooter,
} from "@ionic/react";

import { useEffect, useState } from "react";
import { searchOutline, addOutline } from "ionicons/icons";

import Header from "../components/Header";
import useFormFields from "../hooks/useFormFields";
import { useProductApi } from "../api/useProductApi";
import { useMedidasApi } from "../api/useMedidasApi";
import { useProveedoresApi } from "../api/useProveedoresApi";
import { proveedoresType } from "../models/proveedoresTypes.interface";

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => any;
  fetchData: () => any;
}

const ModalNewProduct = ({
  showModal,
  setShowModal,
  fetchData,
}: WithModalProps) => {
  const [productType, setProductType] = useState<"individual" | "compuesto">(
    "individual"
  );
  const [medidas, setMedidas] = useState<
    {
      id: string;
      nombre_unidad: string;
    }[]
  >([]);
  const [proveedores, setProveedores] = useState<proveedoresType[]>([]);
  const { addIndividualProduct } = useProductApi();
  const { getMedidas } = useMedidasApi();
  const { getProveedores } = useProveedoresApi();

  const { getLabelProps, formField } = useFormFields({
    clave_sat: "",
    nombre: "",
    descripcion: "",
    cantidad_paquete: "",
    costo_paquete: 0,
    costo_unitario: 0,
    precio_venta: 0,
    unidad_medida_id: 0,
    proveedor_id: 0,
  });

  const handleChange = (e: any) => {
    console.log(e.target.id);
    setProductType(e.target.id);
  };

  useEffect(() => {
    (async () => {
      const response = await getMedidas();
      const _response = await getProveedores();
      setMedidas(response);
      setProveedores(_response);
      console.log(response);
    })();
  }, []);

  const onSubmit = async () => {
    if (productType === "individual") {
      await addIndividualProduct({
        clave_sat: formField.clave_sat,
        nombre_producto_individual: formField.nombre,
        descripcion_producto_individual: formField.descripcion,
        cantidad_paquete: formField.cantidad_paquete,
        costo_paquete: formField.costo_paquete,
        costo_unitario: formField.costo_unitario,
        precio_venta: formField.precio_venta,
        unidad_medida_id: formField.unidad_medida_id,
        proveedor_id: formField.proveedor_id,
      });
    }

    fetchData();
    setShowModal(false);
  };

  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Agregar nuevo producto</IonHeader>
      <IonContent className="ion-padding-horizontal ion-padding-vertical">
        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="sat-producto">Clave cátalogo SAT</label>
              <input
                type="text"
                className="form-control"
                id="sat-producto"
                {...getLabelProps("clave_sat")}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="nombre-producto">Nombre</label>
              <input
                type="text"
                className="form-control"
                id="nombre-producto"
                {...getLabelProps("nombre")}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="desc-producto">Descripción</label>
              <input
                type="text"
                className="form-control"
                id="desc-producto"
                {...getLabelProps("descripcion")}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="u-medida">Unidad de medida</label>
              <select
                id="u-medida"
                className="form-control"
                {...getLabelProps("unidad_medida_id")}
              >
                <option value="0" disabled>
                  Seleccione unidad
                </option>

                {medidas.map((medida) => (
                  <option value={medida.id}>{medida.nombre_unidad}</option>
                ))}
              </select>
            </IonCol>
            <IonCol>
              <label htmlFor="proveedor">Proveedor principal</label>
              <select
                id="proveedor"
                className="form-control"
                {...getLabelProps("proveedor_id")}
              >
                <option value="" disabled>
                  Seleccione proveedor
                </option>

                {proveedores.map((item) => (
                  <option value={item.id}>{item.representante}</option>
                ))}
              </select>
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate center-cols">
            <IonCol>
              <h5>Tipo de producto:</h5>
            </IonCol>
            <IonCol>
              <input
                type="radio"
                id="individual"
                defaultChecked={true}
                name="tipo"
                onInput={handleChange}
              />
              <label htmlFor="individual">Individual</label>
            </IonCol>
            <IonCol>
              <input
                type="radio"
                id="compuesto"
                name="tipo"
                onInput={handleChange}
              />
              <label htmlFor="compuesto">Compuesto</label>
            </IonCol>
          </IonRow>

          {productType === "individual" ? (
            <>
              <IonRow className="form-row-separate">
                <IonCol>
                  <label htmlFor="cantidad">Cantidad por paquete</label>
                  <input
                    type="number"
                    className="form-control"
                    id="cantidad"
                    {...getLabelProps("cantidad_paquete")}
                  />
                </IonCol>
                <IonCol>
                  <label htmlFor="sat-producto">Costo por paquete</label>
                  <input
                    type="number"
                    className="form-control"
                    id="sat-producto"
                    {...getLabelProps("costo_paquete")}
                  />
                </IonCol>
                <IonCol>
                  <label htmlFor="sat-producto">Costo unitario</label>
                  <input
                    type="number"
                    className="form-control"
                    id="sat-producto"
                    {...getLabelProps("costo_unitario")}
                  />
                </IonCol>
              </IonRow>
            </>
          ) : (
            <>
              <IonRow className="form-row-separate">
                <IonCol size="12">
                  <label htmlFor="">Producto</label>
                </IonCol>
                <IonCol>
                  <select className="form-control" defaultValue="def">
                    <option value="def" disabled>
                      Seleccione producto
                    </option>
                    <option value="op-1">Producto 1</option>
                    <option value="op-2">Producto 2</option>
                    <option value="op-3">Producto 3</option>
                    <option value="op-4">Producto 4</option>
                  </select>
                </IonCol>
                <IonCol className="add-other">
                  <IonButton color="bwater">
                    <IonIcon
                      md={addOutline}
                      ios={addOutline}
                      className="add-other-btn"
                    />
                  </IonButton>
                </IonCol>
              </IonRow>
            </>
          )}
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="sat-producto">Precio Venta</label>
              <input
                type="number"
                className="form-control"
                id="sat-producto"
                {...getLabelProps("precio_venta")}
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cancelar
        </IonButton>
        <IonButton color="bwater" onClick={() => onSubmit()}>
          Agregar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

const ModalProduct = ({ showModal, setShowModal }: WithModalProps) => {
  const [productType, setProductType] = useState("individual");
  const [editable, setEditable] = useState(false);
  const [product, setProduct] = useState<any[]>([]);

  const handleChange = (e: any) => {
    console.log(e.target.id);
    setProductType(e.target.id);
  };

  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Detalle del producto</IonHeader>
      <IonContent className="ion-padding-horizontal ion-padding-vertical">
        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="id-producto">ID producto</label>
              <input
                type="text"
                value="N-12345"
                className="form-control"
                id="id-producto"
                disabled={!editable}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="sat-producto">Clave cátalogo SAT</label>
              <input
                type="text"
                value="N-12345"
                className="form-control"
                id="sat-producto"
                disabled={!editable}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="nombre-producto">Nombre</label>
              <input
                type="text"
                value="Botellón"
                className="form-control"
                id="nombre-producto"
                disabled={!editable}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="desc-producto">Descripción</label>
              <input
                type="text"
                value="Botellón de 1 litro"
                className="form-control"
                id="desc-producto"
                disabled={!editable}
              />
            </IonCol>
          </IonRow>

          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="u-medida">Unidad de medida</label>
              <input
                type="text"
                value="Alguna unidad"
                className="form-control"
                id="desc-producto"
                disabled={!editable}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="proveedor">Proveedor principal</label>
              <input
                type="text"
                value="Algún proveedor"
                className="form-control"
                id="desc-producto"
                disabled={!editable}
              />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate center-cols">
            <IonCol>
              <h5>Tipo de producto:</h5>
            </IonCol>
            <IonCol>
              <input
                type="radio"
                id="individual"
                defaultChecked={true}
                name="tipo"
                onInput={handleChange}
                disabled={!editable}
              />
              <label htmlFor="individual">Individual</label>
            </IonCol>
            <IonCol>
              <input
                type="radio"
                id="compuesto"
                name="tipo"
                onInput={handleChange}
                disabled={!editable}
              />
              <label htmlFor="compuesto">Compuesto</label>
            </IonCol>
          </IonRow>

          {productType === "individual" ? (
            <>
              <IonRow className="form-row-separate">
                <IonCol>
                  <label htmlFor="cantidad">Cantidad por paquete</label>
                  <input
                    type="number"
                    value="200"
                    className="form-control"
                    id="cantidad"
                    disabled={!editable}
                  />
                </IonCol>
                <IonCol>
                  <label htmlFor="sat-producto">Costo por paquete</label>
                  <input
                    type="number"
                    value="200"
                    className="form-control"
                    id="sat-producto"
                    disabled={!editable}
                  />
                </IonCol>
                <IonCol>
                  <label htmlFor="sat-producto">Costo unitario</label>
                  <input
                    type="number"
                    value="200"
                    className="form-control"
                    id="sat-producto"
                    disabled={!editable}
                  />
                </IonCol>
              </IonRow>
            </>
          ) : (
            <>
              <IonRow className="form-row-separate">
                <IonCol size="12">
                  <label htmlFor="">Producto</label>
                </IonCol>
                <input
                  type="text"
                  value="Producto #"
                  className="mb form-control"
                  id="sat-producto"
                  disabled={!editable}
                />
                <input
                  type="text"
                  value="Producto #"
                  className="mb form-control"
                  id="sat-producto"
                  disabled={!editable}
                />
                <input
                  type="text"
                  value="Producto #"
                  className="mb form-control"
                  id="sat-producto"
                  disabled={!editable}
                />
                <input
                  type="text"
                  value="Producto #"
                  className="mb form-control"
                  id="sat-producto"
                  disabled={!editable}
                />
              </IonRow>
            </>
          )}
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="cantidad">% Impuestos</label>
              <input
                type="number"
                value="100"
                className="form-control"
                disabled={!editable}
                id="cantidad"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="sat-producto">% Ganancia</label>
              <input
                type="number"
                value="100"
                className="form-control"
                id="sat-producto"
                disabled={!editable}
              />
            </IonCol>
            <IonCol>
              <label htmlFor="sat-producto">% Com. Por Venta</label>
              <input
                type="number"
                value="100"
                className="form-control"
                disabled={!editable}
                id="sat-producto"
              />
            </IonCol>
            <IonCol>
              <label htmlFor="sat-producto">Precio Venta</label>
              <input
                type="number"
                value="100"
                className="form-control"
                id="sat-producto"
                disabled={!editable}
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Cerrar
        </IonButton>

        {editable ? (
          <IonButton
            color="bwater"
            style={{ marginRight: "1rem" }}
            onClick={() => setEditable(true)}
          >
            Guardar
          </IonButton>
        ) : (
          <IonButton
            color="bwater"
            style={{ marginRight: "1rem" }}
            onClick={() => setEditable(true)}
          >
            Editar
          </IonButton>
        )}
        <IonButton color="bwater" style={{ marginRight: "1rem" }}>
          Eliminar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

const ProductsPage = () => {
  const [showModalNP, setShowModalNP] = useState(false);
  const [showModalP, setShowModalP] = useState(false);
  const [products, setProducts] = useState<any[]>([]);
  const { getIndividualProducts } = useProductApi();

  const fetch = async () => {
    const response = await getIndividualProducts();
    setProducts(response);
    console.log(response);
  };

  useEffect(() => {
    fetch();
  }, []);

  return (
    <>
      <ModalNewProduct
        fetchData={fetch}
        showModal={showModalNP}
        setShowModal={setShowModalNP}
      />
      <ModalProduct showModal={showModalP} setShowModal={setShowModalP} />
      <IonPage>
        <Header title="Perfiles de productos - Suministros" />
        <IonContent fullscreen className="bg-light-bwater">
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                <IonButton
                  color="bwater"
                  onClick={() => {
                    setShowModalNP(true);
                  }}
                >
                  Nuevo producto
                </IonButton>
                <IonButton color="bwater">Eliminar producto</IonButton>
                <IonCard className="m-0">
                  <IonCardHeader className="table-header">
                    <h2 className="table-title">
                      Relación de Productos – Suministros
                    </h2>
                    <div className="search">
                      <button>
                        <IonIcon md={searchOutline} />
                      </button>
                      <input
                        type="text"
                        className="search__input"
                        placeholder="Buscar"
                      />
                    </div>
                  </IonCardHeader>
                  <IonCardContent>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>ID Producto</th>
                          <th>Nombre</th>
                          <th>Descripción</th>
                          <th>U. Medida</th>
                          <th>Clave Prod. SAT</th>
                          <th>Proveedor</th>
                          <th>Precio de venta</th>
                        </tr>
                      </thead>
                      <tbody>
                        {products.map((product) => (
                          <tr
                            onClick={() => {
                              setShowModalP(true);
                            }}
                          >
                            <td>{product.id}</td>
                            <td>
                              {product.nombre_producto_individual ||
                                "Not found"}
                            </td>
                            <td>
                              {product.descripcion_producto_individual ||
                                "Not found"}
                            </td>
                            <td>{product.unidad_medida_id || "Not found"}</td>
                            <td>{product.clave_sat}</td>
                            <td>{product.proveedor_id || "Not found"}</td>
                            <td>{product.precio_venta}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default ProductsPage;
