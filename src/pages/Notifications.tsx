import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonButton,
} from "@ionic/react";

import Header from "../components/Header";

const Notifications = () => {
  // Estado para ver el mapa - Esto lo pasamos al componente hijo

  return (
    <IonPage>
      <Header title="Notificaciones" />
      <IonContent fullscreen className="bg-light-bwater">
        <IonGrid>
          <IonRow>
            <IonCol size="12">
              <IonButton color="bwater" routerLink="/page/dashboard">
                Regresar al Dashboard
              </IonButton>

              <IonCard className="m-0">
                <IonCardHeader className="table-header">
                  <h2
                    className="table-title"
                    style={{ textAlign: "center", width: "100%" }}
                  >
                    Notificaciones
                  </h2>
                </IonCardHeader>
                <IonCardContent>
                  <table className="table">
                    <tbody>
                      {[1, 2, 3, 4, 5].map((el) => (
                        <tr
                          style={{
                            width: "100%",
                          }}
                        >
                          <td style={{ textAlign: "center" }}>
                            {" "}
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Ex et omnis rerum neque eligendi facere!
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Notifications;
