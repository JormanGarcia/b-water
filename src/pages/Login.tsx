import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonCard,
  IonButton,
} from "@ionic/react";
import bwaterLogo from "../assets/img/logo.png";
import "firebase/auth";
import { useEffect, useState } from "react";
import UseAuth from "../hooks/useAuth";
import { useNavContext } from "../hooks/useNavContext";
import useFormFields from "../hooks/useFormFields";
import ModalAddClient from "../components/ModalAddClient";

const Login = () => {
  const {
    formField: { email, password },
    getLabelProps,
  } = useFormFields({
    email: "",
    password: "",
  });
  const { isAuthenticated, loginUser } = UseAuth();
  const { navigate } = useNavContext();

  const [modalAddClient, setModalAddClient] = useState(false);

  useEffect(() => {
    if (isAuthenticated) {
      navigate("/page/dashboard");
    }
  }, [isAuthenticated]);

  return (
    <IonPage className="login-page" id="main">
      <ModalAddClient
        setShowModal={setModalAddClient}
        showModal={modalAddClient}
        forClients
      />
      <IonContent className="bg-gradient">
        <IonGrid>
          <IonRow>
            <IonCol size="10" size-md="4">
              <IonCard className="login-card">
                <img src={bwaterLogo} alt="Logo BWater" />
                <form>
                  <input
                    type="text"
                    placeholder="Usuario"
                    className="form-control"
                    {...getLabelProps("email")}
                  />
                  <input
                    type="password"
                    placeholder="Contraseña"
                    className="form-control"
                    {...getLabelProps("password")}
                  />
                  <IonButton
                    color="bwater"
                    onClick={() => loginUser({ email, password })}
                  >
                    Iniciar sesión
                  </IonButton>

                  <IonButton
                    color="bwater"
                    onClick={() => setModalAddClient(true)}
                  >
                    Registrar Usuario
                  </IonButton>
                </form>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Login;
