import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
} from "@ionic/react";
import ModalAddVendor from "../components/ModalAddVendors";
import ModalVendor from "../components/ModalVendor";

import Header from "../components/Header";

import { useEffect, useMemo, useState } from "react";
import UseSystemState from "../hooks/useSystemState";
import { useProveedoresApi } from "../api/useProveedoresApi";
import { proveedoresType } from "../models/proveedoresTypes.interface";
import DataTable from "../components/DataTable";

const VendorsPage = () => {
  const [showModalAddVendor, setShowModalAddVendor] = useState(false);
  const [showModalVendor, setShowModalVendor] = useState(false);
  const [proveedores, setProveedores] = useState<proveedoresType[]>([]);
  const [vendorId, setVendorId] = useState<number | null>(null);
  const { setLoading } = UseSystemState();

  const proveedoresApi = useProveedoresApi();

  const fetchData = async () => {
    const proveedoresResponse = await proveedoresApi.getProveedores();
    setProveedores(proveedoresResponse);
  };

  const columns = useMemo<
    { Header: string; accessor: keyof proveedoresType }[]
  >(
    () => [
      {
        Header: "Id Proveedor",
        accessor: "id",
      },
      {
        Header: "Razon Social",
        accessor: "razon_social",
      },
      {
        Header: "RFC",
        accessor: "rfc",
      },
      {
        Header: "Representante",
        accessor: "representante",
      },
      {
        Header: "Direccion",
        accessor: "direccion_proveedor",
      },
      {
        Header: "Telefono",
        accessor: "telefono_proveedor",
      },
    ],

    []
  );

  useEffect(() => {
    (async () => {
      setLoading(true);
      await fetchData();
      setLoading(false);
    })();
  }, []);

  return (
    <>
      <ModalAddVendor
        showModal={showModalAddVendor}
        setShowModal={setShowModalAddVendor}
        fetchVendors={fetchData}
      />
      <ModalVendor
        setVendorId={setVendorId}
        fetchVendor={fetchData}
        vendorId={vendorId}
      />
      <IonPage>
        <Header title="Proveedores" />
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                <IonButton
                  color="bwater"
                  onClick={() => {
                    setShowModalAddVendor(true);
                  }}
                >
                  Nuevo proveedor
                </IonButton>
                <DataTable
                  title="Listado de Proveedores"
                  data={proveedores}
                  columns={columns}
                  onRowClick={(x) => setVendorId(x.id)}
                />
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default VendorsPage;
