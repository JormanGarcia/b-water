import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonBadge,
  IonModal,
  IonHeader,
  IonFooter,
} from "@ionic/react";

import Header from "../components/Header";

import { useState } from "react";

interface WithModalProps {
  showModal: boolean;
  setShowModal: (a: boolean) => any;
}

const ModalBillingDetail = ({ showModal, setShowModal }: WithModalProps) => {
  return (
    <IonModal
      isOpen={showModal}
      cssClass="modal"
      swipeToClose={true}
      onDidDismiss={() => setShowModal(false)}
    >
      <IonHeader className="modal__header">Detalles de la factura</IonHeader>
      <IonContent className="ion-padding-horizontal">
        <h5 className="modal__title">Datos del cliente: </h5>
        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <label htmlFor="">Cliente</label>
              <input type="text" className="form-control" value="N-123" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Sucursal</label>
              <input type="text" className="form-control" value="Sucursal" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Chofer</label>
              <input
                type="text"
                className="form-control"
                value="Carlos Javier"
              />
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol>
              <label htmlFor="">Nombre</label>
              <input type="text" className="form-control" value="Moises" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Apellido</label>
              <input type="text" className="form-control" value="Caceres" />
            </IonCol>
            <IonCol>
              <label htmlFor="">Télefono</label>
              <input type="text" className="form-control" value="18009200" />
            </IonCol>
          </IonRow>
        </IonGrid>

        <h5 className="modal__title">Dirección del cliente: </h5>

        <IonGrid>
          <IonRow className="form-row-separate">
            <IonCol>
              <input
                type="text"
                value="Fraccionamiento"
                className="form-control"
                placeholder="Fraccionamiento"
              />
            </IonCol>
            <IonCol>
              <input
                type="text"
                value="Coto"
                className="form-control"
                placeholder="Coto"
              />
            </IonCol>
          </IonRow>
          <IonRow className="form-row-separate">
            <IonCol>
              <input
                type="text"
                value="Casa"
                className="form-control"
                placeholder="Casa"
              />
            </IonCol>
            <IonCol>
              <input
                type="text"
                value="Calle"
                className="form-control"
                placeholder="Calle"
              />
            </IonCol>
          </IonRow>
        </IonGrid>

        <h5 className="modal__title">Datos de facturación: </h5>

        <table className="table">
          <thead>
            <tr>
              <th>Producto</th>
              <th>Cantidad</th>
              <th>P. Unit</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Garrafon nuevo</td>
              <td>1</td>
              <td>54</td>
              <td>77</td>
            </tr>
            <tr>
              <td>Garrafon nuevo</td>
              <td>1</td>
              <td>54</td>
              <td>77</td>
            </tr>
            <tr>
              <td>Garrafon nuevo</td>
              <td>1</td>
              <td>54</td>
              <td>77</td>
            </tr>
            <tr>
              <td>Garrafon nuevo</td>
              <td>1</td>
              <td>54</td>
              <td>77</td>
            </tr>
            <tr className="table-bottom">
              <td></td>
              <td></td>
              <td>Sub total</td>
              <td></td>
            </tr>
            <tr className="table-bottom">
              <td></td>
              <td></td>
              <td>Descuento</td>
              <td></td>
            </tr>
            <tr className="table-bottom">
              <td></td>
              <td></td>
              <td>Total</td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </IonContent>

      <IonFooter className="modal__footer">
        <IonButton
          color="bwater"
          style={{ marginRight: "1rem" }}
          onClick={() => {
            setShowModal(false);
          }}
        >
          Aceptar
        </IonButton>
      </IonFooter>
    </IonModal>
  );
};

const Billing = () => {
  const [showModalBD, setShowModalBD] = useState(false);

  return (
    <>
      <ModalBillingDetail
        showModal={showModalBD}
        setShowModal={setShowModalBD}
      />
      <IonPage>
        <Header title="Facturación" />
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="12" size-lg="8">
                <IonButton color="bwater">Devolución</IonButton>
                <IonButton color="bwater">Anular fac</IonButton>
                <IonButton color="bwater">Filtros</IonButton>
                <IonButton color="bwater">Configuración SAT</IonButton>

                <IonCard className="m-0">
                  <IonCardHeader className="table-header">
                    <h2 className="table-title">Relación de facturación</h2>
                    <div className="table-header__form">
                      <select defaultValue="def" className="form-control">
                        <option value="def" disabled>
                          Sucursal
                        </option>
                        <option value="s1">Sucursal 1</option>
                        <option value="s2">Sucursal 2</option>
                      </select>
                      <select defaultValue="def" className="form-control">
                        <option value="def" disabled>
                          Cliente
                        </option>
                        <option value="s1">Cliente 1</option>
                        <option value="s2">Cliente 2</option>
                      </select>
                      <select defaultValue="def" className="form-control">
                        <option value="def" disabled>
                          Periodo
                        </option>
                        <option value="s1">Periodo 1</option>
                        <option value="s2">Periodo 2</option>
                      </select>
                      <select defaultValue="def" className="form-control">
                        <option value="def" disabled>
                          Conductor
                        </option>
                        <option value="s1">Conductor 1</option>
                        <option value="s2">Conductor 2</option>
                      </select>
                    </div>
                  </IonCardHeader>
                  <IonCardContent>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>ID factura</th>
                          <th>Fecha</th>
                          <th>ID Pedido</th>
                          <th>ID Cliente</th>
                          <th>Monto (mxn)</th>
                          <th>Estatus de facturación</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr
                          onClick={() => {
                            setShowModalBD(true);
                          }}
                        >
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>
                            <IonBadge color="success">Facturado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>
                            <IonBadge color="success">Facturado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>
                            <IonBadge color="success">Facturado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>
                            <IonBadge color="success">Facturado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>
                            <IonBadge color="success">Facturado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>
                            <IonBadge color="warning">Devolución</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>
                            <IonBadge color="warning">Devolución</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>
                            <IonBadge color="warning">Devolución</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>
                            <IonBadge color="warning">Devolución</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>
                            <IonBadge color="warning">Devolución</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>
                            <IonBadge color="warning">Devolución</IonBadge>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
              <IonCol size="12" size-lg="4">
                <IonCard className="card-notifications">
                  <IonCardContent>
                    <h2 className="card-custom__title pl-0 bwater-color">
                      Pedidos por facturar: ##N
                    </h2>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>ID pedido</th>
                          <th>Estatus</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>N-12345ABC</td>
                          <td>
                            <IonBadge color="danger">Por facturar</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>N-12345ABC</td>
                          <td>
                            <IonBadge color="danger">Por facturar</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>N-12345ABC</td>
                          <td>
                            <IonBadge color="danger">Por facturar</IonBadge>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>

                <IonCard className="card-notifications">
                  <IonCardContent>
                    <h2 className="card-custom__title pl-0 bwater-color">
                      Devoluciones: ##N
                    </h2>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>ID pedido</th>
                          <th>Estatus</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>N-12345ABC</td>
                          <td>
                            <IonBadge color="warning">Devolución</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>N-12345ABC</td>
                          <td>
                            <IonBadge color="warning">Devolución</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>N-12345ABC</td>
                          <td>
                            <IonBadge color="warning">Devolución</IonBadge>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>

                <IonCard className="card-notifications">
                  <IonCardContent>
                    <h2 className="card-custom__title pl-0 bwater-color">
                      Vólumen de facturación: ##N
                    </h2>
                    <IonCardHeader className="table-header">
                      <h2 className="table-title">Periodo</h2>
                      <div className="table-header__form">
                        <select defaultValue="def" className="form-control">
                          <option value="def">Hoy</option>
                          <option value="s1">Periodo 1</option>
                          <option value="s2">Periodo 2</option>
                        </select>
                      </div>
                    </IonCardHeader>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>Concepto</th>
                          <th>Monto</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <IonBadge color="success">Facturado</IonBadge>
                          </td>
                          <td>N-12345ABC</td>
                        </tr>
                        <tr>
                          <td>
                            <IonBadge color="success">Facturado</IonBadge>
                          </td>
                          <td>N-12345ABC</td>
                        </tr>
                        <tr>
                          <td>
                            <IonBadge color="success">Facturado</IonBadge>
                          </td>
                          <td>N-12345ABC</td>
                        </tr>
                        <tr>
                          <td>
                            <IonBadge color="success">Facturado</IonBadge>
                          </td>
                          <td>N-12345ABC</td>
                        </tr>
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default Billing;
