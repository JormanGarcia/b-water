import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonIcon,
} from "@ionic/react";
import { searchOutline } from "ionicons/icons";

import Header from "../components/Header";
import ModalAddClient from "../components/ModalAddClient";
import ModalClientDetails from "../components/ModalClientDetails";

import { useEffect, useState } from "react";
import { useClientApi } from "../api/useClientApi";
import { StatusSelector } from "../utils";

const CustomersPage = () => {
  const [showModalAddClient, setShowModalAddClient] = useState(false);
  const [detailsModal, setDetailsModal] = useState<null | string>(null);
  const [clients, setClients] = useState([]);
  const [TipoFilter, setTipoFilter] = useState("");

  const FilterClients = () => {
    if (!TipoFilter) {
      return clients;
    }

    return clients.filter((x) => x!.tipo_cliente === TipoFilter);
  };

  const { getClients } = useClientApi();
  const fetchData = async () => {
    const _clients = await getClients();
    setClients(_clients);
    console.log(_clients);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <ModalAddClient
        showModal={showModalAddClient}
        setShowModal={setShowModalAddClient}
        fetchClients={fetchData}
      />

      <ModalClientDetails
        clientId={detailsModal}
        setClientId={setDetailsModal}
        fetchClients={fetchData}
      />

      <IonPage>
        <Header title="Clientes" />
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                <IonButton
                  color="bwater"
                  onClick={() => {
                    setShowModalAddClient(true);
                  }}
                >
                  Nuevo cliente
                </IonButton>
                <IonCard className="m-0">
                  <IonCardHeader className="table-header">
                    <h2 className="table-title">Base de datos de clientes</h2>
                    <div style={{ display: "flex" }}>
                      <div className="search" style={{ marginRight: ".5rem" }}>
                        <button>
                          <IonIcon md={searchOutline} />
                        </button>
                        <input
                          type="text"
                          className="search__input"
                          placeholder="Buscar"
                        />
                      </div>
                      <div className="table-header__form">
                        <select defaultValue="def" className="form-control">
                          <option value="" disabled>
                            Sucursal
                          </option>
                          <option value="s1">Sucursal 1</option>
                          <option value="s2">Sucursal 2</option>
                        </select>
                        <select
                          value={TipoFilter}
                          onChange={(x) => setTipoFilter(x.target.value)}
                          className="form-control"
                        >
                          <option value="">Tipo</option>
                          <option value="Residencial">Residencial</option>
                          <option value="Negocio">Negocio</option>
                          <option value="Punto de Venta">Punto de Venta</option>
                          <option value="Sucursal">Sucursal</option>
                          <option value="Independiente">Independiente</option>
                        </select>
                      </div>
                    </div>
                  </IonCardHeader>
                  <IonCardContent>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>ID cliente</th>
                          <th>Nombre</th>
                          <th>Apellido</th>
                          <th>Dirección</th>
                          <th>Télefono</th>
                          <th>Tipo</th>
                          <th>Sucursal afiliada</th>
                          <th>Estatus</th>
                        </tr>
                      </thead>
                      <tbody>
                        {FilterClients().map((client: any) => (
                          <tr
                            onClick={() => {
                              setDetailsModal(client.id);
                            }}
                          >
                            <td>{client.telefono.slice(-4)}</td>
                            <td>{client.nombre}</td>
                            <td>{client.apellido}</td>
                            <td>{client.fraccionamiento}</td>

                            <td>{client.telefono}</td>
                            <td>{client.tipo_cliente}</td>
                            <td>{client.nombre_sucursal || ""}</td>
                            <td>{StatusSelector(+client.estatus_cliente)}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default CustomersPage;
