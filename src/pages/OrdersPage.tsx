import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonCard,
  IonCardHeader,
  IonIcon,
  IonCardContent,
  IonBadge,
  IonButton,
} from "@ionic/react";

import { useState } from "react";
import { searchOutline } from "ionicons/icons";

import Header from "../components/Header";
import ModalOrder from "../components/ModalOrder";

const OrdersPage = () => {
  // Modal para mostrar pedidos
  const [showModal, setShowModal] = useState(false);

  // Manera correcta de mostrar modales, con delegación de eventos
  const handleTableClick = (e: any) => {
    if (e.target.tagName === "TD") {
      setShowModal(true);
    }
  };

  return (
    <>
      <ModalOrder showModal={showModal} setShowModal={setShowModal} />
      <IonPage>
        <Header title="Pedidos" />
        <IonContent fullscreen className="bg-light-bwater">
          <IonGrid>
            <IonRow>
              <IonCol size="12" size-lg="8">
                <IonCard className="m-0">
                  <IonCardHeader className="table-header">
                    <h2 className="table-title">Lista de últimos pedidos</h2>
                    <div className="search">
                      <button>
                        <IonIcon md={searchOutline} />
                      </button>
                      <input
                        type="text"
                        className="search__input"
                        placeholder="Buscar"
                      />
                      <IonButton color="bwater">+ Filtros</IonButton>
                    </div>
                  </IonCardHeader>
                  <IonCardContent>
                    <table className="table table-with-info">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>ID Pedido</th>
                          <th>ID Cliente</th>
                          <th>Tipo Venta</th>
                          <th>Sucursal</th>
                          <th>Conductor</th>
                          <th>Monto (mnx)</th>
                          <th>Estatus</th>
                        </tr>
                      </thead>
                      <tbody onClick={handleTableClick}>
                        <tr>
                          <td>14/02/21</td>
                          <td>N-123</td>
                          <td>N-123</td>
                          <td>Refill</td>
                          <td>N-123</td>
                          <td>N-123</td>
                          <td>123</td>
                          <td>
                            <IonBadge color="success">Entregado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>14/02/21</td>
                          <td>N-123</td>
                          <td>N-123</td>
                          <td>Garrafon Nuevo</td>
                          <td>N-123</td>
                          <td>N-123</td>
                          <td>123</td>
                          <td>
                            <IonBadge color="success">Entregado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>14/02/21</td>
                          <td>N-123</td>
                          <td>N-123</td>
                          <td>Canje</td>
                          <td>N-123</td>
                          <td>N-123</td>
                          <td>123</td>
                          <td>
                            <IonBadge color="success">Entregado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>14/02/21</td>
                          <td>N-123</td>
                          <td>N-123</td>
                          <td>Canje</td>

                          <td>N-123</td>
                          <td>N-123</td>
                          <td>123</td>
                          <td>
                            <IonBadge color="success">Entregado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>14/02/21</td>
                          <td>N-123</td>
                          <td>N-123</td>
                          <td>Canje</td>

                          <td>N-123</td>
                          <td>N-123</td>
                          <td>123</td>
                          <td>
                            <IonBadge color="success">Entregado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>14/02/21</td>
                          <td>N-123</td>
                          <td>N-123</td>
                          <td>Canje</td>

                          <td>N-123</td>
                          <td>N-123</td>
                          <td>123</td>
                          <td>
                            <IonBadge color="success">Entregado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>14/02/21</td>
                          <td>N-123</td>
                          <td>N-123</td>
                          <td>Canje</td>

                          <td>N-123</td>
                          <td>N-123</td>
                          <td>123</td>
                          <td>
                            <IonBadge color="success">Entregado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>14/02/21</td>
                          <td>N-123</td>
                          <td>N-123</td>
                          <td>Canje</td>

                          <td>N-123</td>
                          <td>N-123</td>
                          <td>123</td>
                          <td>
                            <IonBadge color="success">Entregado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>14/02/21</td>
                          <td>N-123</td>
                          <td>N-123</td>
                          <td>Canje</td>

                          <td>N-123</td>
                          <td>N-123</td>
                          <td>123</td>
                          <td>
                            <IonBadge color="success">Entregado</IonBadge>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
              <IonCol size="12" size-lg="4">
                <IonCard className="card-notifications">
                  <IonCardContent>
                    <h2 className="card-custom__title pl-0 bwater-color">
                      Pedidos por facturar: ##N
                    </h2>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>ID del pedido</th>
                          <th>Estatus</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>N-12345ABC</td>
                          <td>
                            <IonBadge color="light">En proceso</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>N-12345ABC</td>
                          <td>
                            <IonBadge color="success">Entregado</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>N-12345ABC</td>
                          <td>
                            <IonBadge color="danger">Por facturar</IonBadge>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>

                <IonCard className="card-notifications">
                  <IonCardContent>
                    <h2 className="card-custom__title pl-0 bwater-color">
                      Devoluciones: ##N
                    </h2>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>ID del pedido</th>
                          <th>Estatus</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>N-12345ABC</td>
                          <td>
                            <IonBadge color="warning">Devolución</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>N-12345ABC</td>
                          <td>
                            <IonBadge color="warning">Devolución</IonBadge>
                          </td>
                        </tr>
                        <tr>
                          <td>N-12345ABC</td>
                          <td>
                            <IonBadge color="warning">Devolución</IonBadge>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>

                <IonCard className="card-notifications">
                  <IonCardContent>
                    <h2 className="card-custom__title pl-0 bwater-color">
                      Vólumen de pedidos: ##N
                    </h2>
                    <IonCardHeader className="table-header">
                      <h2 className="table-title">Periodo</h2>
                      <div className="table-header__form">
                        <select defaultValue="def" className="form-control">
                          <option value="def">Hoy</option>
                          <option value="s1">Periodo 1</option>
                          <option value="s2">Periodo 2</option>
                        </select>
                      </div>
                    </IonCardHeader>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>Concepto</th>
                          <th>Monto</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <IonBadge color="success">Entregado</IonBadge>
                          </td>
                          <td>N-12345ABC</td>
                        </tr>
                        <tr>
                          <td>
                            <IonBadge color="light">En proceso</IonBadge>
                          </td>
                          <td>N-12345ABC</td>
                        </tr>
                        <tr>
                          <td>
                            <IonBadge color="success">Entregado</IonBadge>
                          </td>
                          <td>N-12345ABC</td>
                        </tr>
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default OrdersPage;
