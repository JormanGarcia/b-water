import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardContent,
} from "@ionic/react";
import { useState } from "react";
import GarrafonesSalida from "../components/GarrafonesSalida";

import Header from "../components/Header";

const Garrafones = () => {
  const [garrafones, setGarrafones] = useState(false);

  return (
    <IonPage>
      <GarrafonesSalida showModal={garrafones} setShowModal={setGarrafones} />
      <Header title="Garrafones de Canje" />
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol size="12">
              <IonButton color="bwater" routerLink="/page/inventory">
                Regresar al inventario
              </IonButton>
              <IonButton color="bwater" onClick={() => setGarrafones(true)}>
                Salida de Garrafones
              </IonButton>
              <IonCard className="m-0">
                <IonCardHeader className="table-header">
                  <h2 className="table-title">Lista de Garrafones de Canje</h2>
                  <div style={{ display: "flex" }}>
                    <div className="search" style={{ marginRight: ".5rem" }}>
                      <h4>
                        Total de Garrafones de Canje:{" "}
                        <span style={{ color: "var(--ion-color-bwater)" }}>
                          5
                        </span>
                      </h4>
                    </div>
                  </div>
                </IonCardHeader>
                <IonCardContent>
                  <table className="table">
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>ID producto</th>
                        <th>Nombre</th>
                        <th>Sucursal</th>
                        <th>Existencia inicial</th>
                        <th>Existencia final</th>
                        <th>Operación</th>
                        <th>Motivo</th>
                      </tr>
                    </thead>
                    <tbody>
                      {[1, 2, 3, 4, 5].map((el) => (
                        <tr>
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                          <td>100</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Garrafones;
