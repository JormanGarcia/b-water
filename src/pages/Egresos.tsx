import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonButton,
  IonIcon,
} from "@ionic/react";

import { searchOutline } from "ionicons/icons";
import { useState } from "react";

import Header from "../components/Header";
import EgresosRecord from "../components/EgresosRecord";
import EgresosDetails from "../components/EgresosDetails";

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

const Egresos = () => {
  const [egresosRecord, setEgresosRecord] = useState(false);
  const [egresosDetails, setEgresosDetails] = useState<object | null>(null);

  return (
    <IonPage>
      <EgresosRecord
        showModal={egresosRecord}
        setShowModal={setEgresosRecord}
      ></EgresosRecord>
      <EgresosDetails
        item={egresosDetails}
        setItem={setEgresosDetails}
      ></EgresosDetails>
      <Header title="Egresos Varios" />
      <IonContent fullscreen className="bg-light-bwater">
        <IonGrid>
          <IonRow>
            <IonCol size="12">
              <IonButton color="bwater" onClick={() => setEgresosRecord(true)}>
                Registrar Egresos
              </IonButton>

              <IonCard className="m-0">
                <IonCardHeader className="table-header">
                  <h2 className="table-title">Notificaciones</h2>
                  <div style={{ display: "flex" }}>
                    <div className="search" style={{ marginRight: ".5rem" }}>
                      <button>
                        <IonIcon md={searchOutline} />
                      </button>
                      <input
                        type="text"
                        className="search__input"
                        placeholder="Buscar"
                      />
                    </div>
                    <div className="table-header__form">
                      <select defaultValue="def" className="form-control">
                        <option value="def" disabled>
                          Sucursal
                        </option>
                        <option value="s1">Sucursal 1</option>
                        <option value="s2">Sucursal 2</option>
                      </select>
                      <select defaultValue="def" className="form-control">
                        <option value="def" disabled>
                          Periodo
                        </option>
                        <option value="s1">Producto 1</option>
                        <option value="s2">Producto 2</option>
                      </select>
                    </div>
                  </div>
                </IonCardHeader>
                <IonCardContent>
                  <table className="table">
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>Nro. Factura</th>
                        <th>Proveedor</th>
                        <th>Descripcion</th>
                        <th>Sucursal </th>
                        <th>Monto</th>
                      </tr>
                    </thead>
                    <tbody>
                      {numbers.map((item) => (
                        <tr
                          onClick={() => {
                            setEgresosDetails({});
                          }}
                        >
                          <td>1</td>
                          <td>14/02/21</td>
                          <td>1</td>
                          <td>1</td>
                          <td>100</td>
                          <td>100</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Egresos;
