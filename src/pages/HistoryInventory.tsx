import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonIcon,
} from "@ionic/react";
import { searchOutline } from "ionicons/icons";

import Header from "../components/Header";

const HistoryInventory = () => (
  <IonPage>
    <Header title="Historial de Operaciones de Inventario." />
    <IonContent>
      <IonGrid>
        <IonRow>
          <IonCol size="12">
            <IonButton color="bwater" routerLink="/page/inventory">
              Regresar al inventario
            </IonButton>
            <IonCard className="m-0">
              <IonCardHeader className="table-header">
                <h2 className="table-title">
                  Historial de Operaciones de Inventario.
                </h2>
                <div style={{ display: "flex" }}>
                  <div className="search" style={{ marginRight: ".5rem" }}>
                    <button>
                      <IonIcon md={searchOutline} />
                    </button>
                    <input
                      type="text"
                      className="search__input"
                      placeholder="Buscar"
                    />
                  </div>
                  <div className="table-header__form">
                    <select defaultValue="def" className="form-control">
                      <option value="def" disabled>
                        Sucursal
                      </option>
                      <option value="s1">Sucursal 1</option>
                      <option value="s2">Sucursal 2</option>
                    </select>
                    <select defaultValue="def" className="form-control">
                      <option value="def" disabled>
                        Producto
                      </option>
                      <option value="s1">Producto 1</option>
                      <option value="s2">Producto 2</option>
                    </select>
                  </div>
                </div>
              </IonCardHeader>
              <IonCardContent>
                <table className="table">
                  <thead>
                    <tr>
                      <th>Fecha</th>
                      <th>ID producto</th>
                      <th>Nombre</th>
                      <th>Sucursal</th>
                      <th>Existencia inicial</th>
                      <th>Existencia final</th>
                      <th>Operación</th>
                      <th>Motivo</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>14/02/21</td>
                      <td>1</td>
                      <td>1</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>14/02/21</td>
                      <td>1</td>
                      <td>1</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>14/02/21</td>
                      <td>1</td>
                      <td>1</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>14/02/21</td>
                      <td>1</td>
                      <td>1</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>14/02/21</td>
                      <td>1</td>
                      <td>1</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>14/02/21</td>
                      <td>1</td>
                      <td>1</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>14/02/21</td>
                      <td>1</td>
                      <td>1</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>14/02/21</td>
                      <td>1</td>
                      <td>1</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>14/02/21</td>
                      <td>1</td>
                      <td>1</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>14/02/21</td>
                      <td>1</td>
                      <td>1</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                      <td>100</td>
                    </tr>
                  </tbody>
                </table>
              </IonCardContent>
            </IonCard>
          </IonCol>
        </IonRow>
      </IonGrid>
    </IonContent>
  </IonPage>
);

export default HistoryInventory;
