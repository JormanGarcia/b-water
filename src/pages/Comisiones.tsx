import {
  IonContent,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonIcon,
} from "@ionic/react";
import CommissionsRecord from "../components/ComissionsRecord";
import { searchOutline } from "ionicons/icons";

import Header from "../components/Header";

import { useState } from "react";
import ComissionsDetails from "../components/ComissionsDetails";

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

const Comisiones = () => {
  const [commissionsRecord, setCommissionsRecord] = useState(false);
  const [comissionsDetails, setComissionsDetails] =
    useState<Object | null>(null);

  return (
    <>
      <CommissionsRecord
        showModal={commissionsRecord}
        setShowModal={setCommissionsRecord}
      />

      <ComissionsDetails
        item={comissionsDetails}
        setItem={setComissionsDetails}
      />

      <IonPage>
        <Header title="Personal" />
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="12">
                <IonButton color="bwater" routerLink="/page/personal">
                  Regresar a Personal
                </IonButton>
                <IonButton
                  color="bwater"
                  onClick={() => {
                    setCommissionsRecord(true);
                  }}
                >
                  Registrar Comisiones
                </IonButton>
                <IonCard className="m-0">
                  <IonCardHeader className="table-header">
                    <h2 className="table-title">Asignación de Comisiones</h2>
                    <div style={{ display: "flex" }}>
                      <div className="search" style={{ marginRight: ".5rem" }}>
                        <button>
                          <IonIcon md={searchOutline} />
                        </button>
                        <input
                          type="text"
                          className="search__input"
                          placeholder="Buscar"
                        />
                      </div>
                    </div>
                  </IonCardHeader>
                  <IonCardContent>
                    <table className="table">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nombres</th>
                          <th>Apellidos</th>
                          <th>Cargo</th>
                          <th>Sucursal</th>
                          <th>Fecha Correspondiente</th>
                          <th>Monto</th>
                        </tr>
                      </thead>
                      <tbody>
                        {numbers.map((item) => (
                          <tr
                            onClick={() => {
                              setComissionsDetails({});
                            }}
                          >
                            <td>1</td>
                            <td>14/02/21</td>
                            <td>1</td>
                            <td>1</td>
                            <td>100</td>
                            <td>100</td>
                            <td>100</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </IonCardContent>
                </IonCard>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </IonPage>
    </>
  );
};

export default Comisiones;
