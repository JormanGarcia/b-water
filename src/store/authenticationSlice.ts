import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { userByIdType } from "../models/userByIdType.interface";

interface userStateType {
  id: string;
  nombre: string;
  apellido: string;
  rol: number;
  sucursal_id: number;
}

export const authenticationSlice = createSlice({
  name: "authentication",
  initialState: {
    user: null as userStateType | null,
    authToken: null as string | null,
  },
  reducers: {
    login: (state, action: PayloadAction<userStateType>) => {
      state.user = action.payload;
    },
    logout: (state) => {
      state.user = null;
      state.authToken = null;
    },
    setToken: (state, action: PayloadAction<string>) => {
      state.authToken = action.payload;
    },
  },
});

export const { login, logout, setToken } = authenticationSlice.actions;

export default authenticationSlice.reducer;
