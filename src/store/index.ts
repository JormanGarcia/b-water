import {
  configureStore,
  getDefaultMiddleware,
  combineReducers,
} from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import authenticationReducer from "./authenticationSlice";
import systemStateReducer from "./systemStateSlice";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import {
  FLUSH,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  REHYDRATE,
} from "redux-persist/es/constants";

const reducers = combineReducers({
  auth: authenticationReducer,
  systemState: systemStateReducer,
});

const persistedReducer = persistReducer(
  { key: "root", version: 1, storage },
  reducers
);

const store = configureStore({
  reducer: persistedReducer,
  devTools: true,
  middleware: getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
  }),
});

type dispatchType = typeof store.dispatch;
export type StateType = ReturnType<typeof store.getState>;

export const useAppDispatch = () => useDispatch<dispatchType>();
export const persistor = persistStore(store);
export default store;
