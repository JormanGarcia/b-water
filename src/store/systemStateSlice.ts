import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface notification {
  msg: string;
  key: number;
}

export const systemStateSlice = createSlice({
  name: "systemState",
  initialState: {
    notifications: [] as notification[],
    loading: false,
  },
  reducers: {
    setLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload;
    },
    pushNotification: (state, action: PayloadAction<notification>) => {
      state.notifications.push(action.payload);
    },
    unsetNotification: (state, action: PayloadAction<number>) => {
      state.notifications = state.notifications.filter(
        (x) => x.key !== action.payload
      );
    },
  },
});

export const { setLoading, pushNotification, unsetNotification } =
  systemStateSlice.actions;

export default systemStateSlice.reducer;
