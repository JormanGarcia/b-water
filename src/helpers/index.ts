// Estableciendo color para los badge en las tablas
export function getBadgeColor(text: string) {
  if (text === "Entregado") return "success";
  if (text === "Facturado") return "success";
  if (text === "En proceso") return "ligth";
  if (text === "Regresado") return "danger";
  if (text === "No facturado") return "danger";
}
