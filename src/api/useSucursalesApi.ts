import { useInstance } from "./useInstance";
import { sucursalesType } from "../models/sucuarsalesType.interface";

export const useSucursalesApi = () => {
  const instance = useInstance();

  const getSucursales = async () => {
    const CallResponse = await instance.get<sucursalesType[]>("/sucursales");
    return CallResponse.data;
  };

  const getSucursalesById = async (id: number) => {
    const CallResponse = await instance.get(`/sucursales/${id}`);
    const sucursal: sucursalesType = CallResponse.data.object[0];
    return sucursal;
  };

  const addSucursales = async (sucursal: sucursalesType) => {
    const ApiResponse = await instance.post("/addsucursal", sucursal);
    return ApiResponse;
  };

  const deleteSucursales = async (id: number) => {
    const ApiResponse = await instance.delete(`/deletesucursal/${id}`);
    return ApiResponse;
  };

  const updateSucursal = async (sucursal: sucursalesType) => {
    const ApiResponse = await instance.put(
      `/updatesucursal/${sucursal.id}`,
      sucursal
    );
    return ApiResponse;
  };

  return {
    getSucursales,
    getSucursalesById,
    addSucursales,
    deleteSucursales,
    updateSucursal,
  };
};
