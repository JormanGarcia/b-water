import { proveedoresType } from "../models/proveedoresTypes.interface";
import { useInstance } from "./useInstance";

export const useProductApi = () => {
  const instance = useInstance();

  return {
    getIndividualProducts: async () => {
      const response = await instance.get("/productosindividuales");
      return response.data;
    },

    getIndividualProductById: async (id: string) => {
      const response = await instance.get("/productoindividual/" + id);
      return response.data;
    },

    addIndividualProduct: async (newProduct: any) => {
      const response = await instance.post(
        "/addproductoindividual",
        newProduct
      );
      return response.data;
    },

    updateIndividualProduct: async (id: string, Product: any) => {
      const response = await instance.put(
        "/updateproductoindividual/" + id,
        Product
      );
      return response.data;
    },

    deleteIndividualProduct: async (id: string) => {
      const response = await instance.delete("/deleteproductoindividual/" + id);
      return response.data;
    },
  };
};
