import { proveedoresType } from "../models/proveedoresTypes.interface";
import { useInstance } from "./useInstance";

export const useMedidasApi = () => {
  const instance = useInstance();

  return {
    getMedidas: async () => {
      const response = await instance.get("/unidadesmedidas");
      return response.data as {
        id: string;
        nombre_unidad: string;
      }[];
    },
  };
};
