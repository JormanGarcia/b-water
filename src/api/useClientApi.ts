import { IClient } from "../models/client.interface";
import { useInstance } from "./useInstance";

export const useClientApi = () => {
  const instance = useInstance();

  return {
    getClients: async () => {
      const CallResponse = await instance.get("/clientesUsers");
      const clients = CallResponse.data;
      return clients;
    },

    getClientById: async (id: string) => {
      const CallResponse = await instance.get("/clientes/" + id);
      const client = CallResponse.data.object[0];
      return client;
    },

    addClients: async (newClient: any) => {
      const CallResponse = await instance.post("/addclientes", newClient);
      const clients = CallResponse.data;
      return clients;
    },

    updateClient: async (editClient: IClient, id: string) => {
      const CallResponse = await instance.put(
        "/updatecliente/" + id,
        editClient
      );
      const clients = CallResponse.data;
      return clients;
    },

    deleteClient: async (id: string) => {
      const CallResponse = await instance.delete("/deletecliente/" + id);
      const clients = CallResponse.data;
      return clients;
    },
  };
};
