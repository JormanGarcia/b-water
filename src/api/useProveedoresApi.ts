import { proveedoresType } from "../models/proveedoresTypes.interface";
import { useInstance } from "./useInstance";

export const useProveedoresApi = () => {
  const instance = useInstance();

  const getProveedores = async () => {
    const ApiResponse = await instance.get("/proveedores");
    const proveedores: proveedoresType[] = ApiResponse.data;
    return proveedores;
  };

  const getProveedorById = async (id: number) => {
    const ApiResponse = await instance.get(`/proveedores/${id}`);
    const proveedor: proveedoresType = ApiResponse.data.object[0];
    return proveedor;
  };

  const addProveedor = async (proveedor: proveedoresType) => {
    const ApiResponse = await instance.post(`/addproveedor`, proveedor);
    return ApiResponse;
  };

  const deleteProveedor = async (id: number) => {
    const ApiResponse = await instance.delete(`/deleteproveedor/${id}`);
    return ApiResponse;
  };

  const updateProveedor = async (proveedor: proveedoresType) => {
    const ApiResponse = await instance.put(
      `/updateproveedor/${proveedor?.id}`,
      proveedor
    );
    return ApiResponse;
  };

  return {
    getProveedores,
    getProveedorById,
    addProveedor,
    deleteProveedor,
    updateProveedor,
  };
};
