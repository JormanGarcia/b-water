import axios from "axios";
import { API } from "../utils/constant";

const instance = axios.create({
  baseURL: API,
});

const loginUser = async (user: { email: string; password: string }) => {
  console.log(user);
  const loginResponse = await instance.post("/loginEmail", user);
  console.log(loginResponse);
  return loginResponse;
};

const loginUserByPhone = async (user: { email: string; password: string }) => {
  console.log(user);
  const loginResponse = await instance.post("/loginTelefono", {
    telefono: user.email,
    password: user.password,
  });
  console.log(loginResponse);
  return loginResponse;
};

const logoutUser = async (token: string) => {
  const loginResponse = await instance.get("/logout", {
    headers: {
      Authorization: token,
    },
  });
  console.log(loginResponse);
  return loginResponse;
};

const authApi = {
  loginUser,
  hello: () => {
    console.log("hello");
  },
  logoutUser,
  loginUserByPhone,
};

export default authApi;
