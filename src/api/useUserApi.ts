import { useInstance } from "./useInstance";

import { userType } from "../models/userType.interface";
import { userByIdType } from "../models/userByIdType.interface";

export const useUserApi = () => {
  const instance = useInstance();

  const getUsers = async () => {
    const CallResponse = await instance.get("/usuariosSucursal");
    const users: userType[] = CallResponse.data;
    console.log(users);
    return users;
  };

  const getUsersByRole = async (rol: number) => {
    const CallResponse = await instance.get("/usuarios");
    const users: userType[] = CallResponse.data.filter(
      (x: userType) => x.rol === rol
    );
    return users;
  };

  const getUsersById = async (id: number) => {
    const CallResponse = await instance.get(`/usuario/${id}`);

    return CallResponse.data.object[0] as userByIdType;
  };

  const addUser = async (newUser: any) => {
    console.log("agregando usuario....");
    try {
      const user = await instance.post("/addUsuarios", newUser);
      return { ok: true, msg: "Usuario Agregado", user };
    } catch (e) {
      Promise.reject(e);
    }
  };

  const deleteUser = async (id: number) => {
    try {
      await instance.delete(`/deleteUsuario/${id}`);
      Promise.resolve({
        ok: true,
        msg: "Usuario Eliminado Satisfactoriamente",
      });
    } catch (e) {
      Promise.reject(e);
    }
  };

  const updateUser = async (user: any) => {
    try {
      await instance.put(`/updateUsuario/${user.id}`, user);
      return user;
    } catch (e) {
      Promise.reject(e);
    }
  };

  return {
    getUsers,
    getUsersById,
    getUsersByRole,
    addUser,
    deleteUser,
    updateUser,
  };
};
