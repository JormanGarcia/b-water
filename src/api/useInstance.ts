import axios from "axios";
import useAuth from "../hooks/useAuth";

import { API } from "../utils/constant";

export const useInstance = () => {
  const { token } = useAuth();

  const instance = axios.create({
    baseURL: API,
    headers: {
      "content-type": "application/json",
      Authorization: token,
    },
  });

  return instance;
};
